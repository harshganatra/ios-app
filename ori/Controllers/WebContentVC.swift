//
//  WebContentVC.swift
//  ori
//
//  Created by Harsh Ganatra on 20/02/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import WebKit

class WebContentVC: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var wkWebView: WKWebView!
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.makeToastActivity(.center)
        
        wkWebView.navigationDelegate = self
        
        let urlReq = URLRequest(url: URL(string: url)!)
        wkWebView.load(urlReq)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if webView.isLoading {
            return
        }
        self.view.hideToastActivity()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
