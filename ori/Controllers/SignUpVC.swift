//
//  SignUpVC.swift
//  ori
//
//  Created by Harsh Ganatra on 01/11/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import UIKit
import Toast_Swift

class SignUpVC: UIViewController {
    
    var phone:String!
    var toPage = ""

    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var emailAddressField: UITextField!
    let phoneDelegate = NumericFieldDelegate()
    let textDelegate = TextFieldDelegate()
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneField.text = phone!
        self.phoneField.delegate = phoneDelegate
        let numberToolbar: UIToolbar = UIToolbar()
        numberToolbar.barStyle = UIBarStyle.default
        let button = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closeKeyboard))
        button.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        numberToolbar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button
        ]
        numberToolbar.sizeToFit()
        self.phoneField.inputAccessoryView = numberToolbar //do it for every relevant textfield if there are more than one
        
        firstNameField.delegate = textDelegate
        firstNameField.tag = 0
        lastNameField.delegate = textDelegate
        lastNameField.tag = 1
        emailAddressField.delegate = textDelegate
        emailAddressField.tag=2
        
    }
    
    @objc func closeKeyboard () {
        self.phoneField.resignFirstResponder()
    }
    
    @IBAction func signUpButton(_ sender: Any) {
//        self.navigationController!.popToRootViewController(animated: true)
        if Validation.isValidPhoneNumber(textField: phoneField, required: true) &&
            Validation.isValidEmail(textField: emailAddressField, required: true) &&
            Validation.hasText(textField: firstNameField, required: true) &&
            Validation.hasText(textField: lastNameField, required: true) {
            
            
            self.performSegue(withIdentifier: "signupOTP", sender: self)
        } else {
            OriUtil.showToast(view: self.view, msg: "Enter valid details")
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "signupOTP") {
            let otpVC = segue.destination as! OtpVC
            otpVC.phone = phoneField.text!
            otpVC.firstName = firstNameField.text!
            otpVC.lastName = lastNameField.text!
            otpVC.emailID = emailAddressField.text!
            otpVC.gender = ""
            otpVC.dob = ""
            otpVC.toPage = self.toPage
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.unsubscribeToKeyboardNotifications()
    }
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(_ :)),
                                               name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: .UIKeyboardWillHide, object: nil)
    }
    func unsubscribeToKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyBoardWillShow(_ notification:Notification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = getKeyboardHeight(notification)
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(_ notification:Notification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }
}

