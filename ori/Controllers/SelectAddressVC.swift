//
//  SelectAddressVC.swift
//  ori
//
//  Created by Harsh Ganatra on 31/01/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON

class SelectAddressVC: UIViewController, UITableViewDataSource, UITableViewDelegate, ReturnProtocol {

    var myAddressList:[Address] = []
    var accountPage = false
    var selectedPosition:Int = -1
    var selectedAddress = Address()
    weak var delegate: ReturnProtocol?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nullAddressLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 16, right: 0)
        tableView.register(UINib(nibName:"AddressCardLayout",bundle:nil), forCellReuseIdentifier: "addressCell")
        
        self.getAddresses()
        
        if accountPage {
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    @IBAction func addAddress(_ sender: Any) {
        self.performSegue(withIdentifier: "addAddress", sender: nil)
    }
    
    @IBAction func doneButton(_ sender: Any) {
        OriUtil.oriLog("done pressed, selected position ",self.selectedPosition)

        if selectedPosition != -1 && self.delegate != nil {
            self.delegate?.getReturnValue(value: ArrayModel(type: Values.address_key, data: myAddressList[selectedPosition]))
            self.navigationController?.popViewController(animated: true)
        } else {
            OriUtil.showToast(view: self.view, msg: "Please select an address to continue")
        }
    }
    
    func getAddresses() {
        self.view.makeToastActivity(.center)

        //call API
        OriNetworking.makeRequest(requestType: .get, url: ApiUrls.getAddressByUserIdUrl, parameters: nil, auth: .send_auth, view: self.view) {(status, error, response) in
            if status {
                if response["status"].bool! {
                    let data = response["data"][0]["address"]
                    self.myAddressList = []
                    for (_,subJson):(String, JSON) in data {
                        var address = Address()
                        address._id = subJson["_id"].stringValue
                        address.addressLine1 = subJson["addressLine1"].stringValue
                        address.addressLine2 = subJson["addressLine2"].stringValue
                        address.city = subJson["city"].stringValue
                        if subJson["nickname"].exists() {
                            address.nickName = subJson["nickname"].stringValue
                        }
                        if subJson["state"].exists() {
                            address.state = subJson["state"].stringValue
                        }
                        if subJson["landmark"].exists() {
                            address.landmark = subJson["landmark"].stringValue
                        }
                        address.zipcode = subJson["zipcode"].stringValue
                        if subJson["isDefault"].exists() {
                            address.isDefault = subJson["isDefault"].boolValue
                        }
                        if address.matches(self.selectedAddress) {
                            self.selectedPosition  = self.myAddressList.count
                        }
                        self.myAddressList.append(address)
                    } // for loop ends here
                    
                    if self.myAddressList.count > 0 {
                        self.nullAddressLabel.isHidden = true
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                        if self.selectedPosition != -1 {
                            let indexPath:IndexPath = IndexPath(row: self.selectedPosition, section: 0)
                            self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: true)
                        }
                    } else {
                        self.nullAddressLabel.isHidden = false
                        self.tableView.isHidden = true
                    }
                } else {
                    self.nullAddressLabel.isHidden = false
                    self.tableView.isHidden = true
                }
            }
            self.view.hideToastActivity()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let address = myAddressList[indexPath.item]
        let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for:indexPath) as! AddressCardCell
        cell.addressLine1.text = address.addressLine1
        cell.addressLine2.text = address.addressLine2
        cell.addCity.text = address.city+" - "+address.zipcode
        if address.nickName != "" {
            cell.addressNickname.isHidden = false
            cell.addressNickname.text = address.nickName
        } else {
            cell.addressNickname.isHidden = true
        }
        if address.landmark != "" {
            cell.addLandmark.isHidden = false
            cell.addLandmark.text = address.landmark
        } else {
            cell.addLandmark.isHidden = true
        }
        
        if accountPage {
            cell.addDeleteImage.isHidden = false
            cell.addSwitch.isHidden = true
        } else {
            cell.addDeleteImage.isHidden = true
            cell.addSwitch.isHidden = false
            cell.addSwitch.isUserInteractionEnabled = false
            if indexPath.item == selectedPosition {
                if cell.addSwitch.isOn == false {
                    cell.addSwitch.setOn(true, animated: true)
                }
            } else {
                if cell.addSwitch.isOn == true {
                    cell.addSwitch.setOn(false, animated: true)
                }
            }
            
        }

        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myAddressList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if accountPage {
            let msg = "Are you sure you want to delete this address?"
            let alertView = UIAlertController(title:"", message: msg, preferredStyle: UIAlertControllerStyle.alert)
            alertView.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (alertAction) -> Void in
                let address = self.myAddressList[indexPath.item]
                self.deleteAddress(id: address._id)
            }))
            alertView.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alertView.presentInOwnWindow()
        } else {
            self.selectedPosition = indexPath.item
            self.tableView.reloadData()
        }
    }
    
    func deleteAddress(id:String) {
        self.view.makeToastActivity(.center)
        
        //call API
        OriNetworking.makeRequest(requestType: .delete, url: ApiUrls.deleteAddressByIdUrl+id, view: self.view) {(status, error, response) in
            if status {
                if response["status"].bool! {
                    let localAddress = OriUtil.getLocalAddress()
                    if localAddress._id == id {
                        OriUtil.deleteLocalAddress()
                    }
                    self.getAddresses()
                } else {
                    OriUtil.showErrorToast(view: self.view)
                }
            }
            self.view.hideToastActivity()
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "addAddress" {
            let vc = segue.destination as! AddAddressVC
            vc.delegate = self
        }
    }
 
    func getReturnValue(value: ArrayModel) {
        if value.type == Values.address_key {
            self.selectedAddress = value.data as! Address
            self.getAddresses()
            if selectedPosition != -1 && self.delegate != nil { //update new address in previous window
                self.delegate?.getReturnValue(value: ArrayModel(type: Values.address_key, data: myAddressList[selectedPosition]))
            }
        }
    }

}

public class AddressCardCell: UITableViewCell {
    @IBOutlet weak var addressNickname: UILabel!
    @IBOutlet weak var addressLine1: UILabel!
    @IBOutlet weak var addressLine2: UILabel!
    @IBOutlet weak var addCity: UILabel!
    @IBOutlet weak var addLandmark: UILabel!
    @IBOutlet weak var addSwitch: UISwitch!
    @IBOutlet weak var addDeleteImage: UIImageView!
    
}






