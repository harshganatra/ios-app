//
//  UserAccountVC.swift
//  ori
//
//  Created by Harsh Ganatra on 19/02/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit

class UserAccountVC: UIViewController {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var mobile: UILabel!
    @IBOutlet weak var emailAddress: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var dateOfBirth: UILabel!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let user = LoginHelper.getCurrentUser().1
        userName.text = user.name
        mobile.text = "+91 "+user.phoneNumber
        emailAddress.text = user.email
        gender.text = user.gender
        dateOfBirth.text = OriUtil.getDateStringFromISO(user.ISOdateOfBirth)
        self.editButton.isEnabled = false
        self.editButton.isEnabled = true
    }
    
    @IBAction func viewAddresses(_ sender: Any) {
        let sb = UIStoryboard(name: "AddressStoryboard", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "selectAddress") as! SelectAddressVC
        vc.accountPage = true
        self.show(vc, sender: self)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
