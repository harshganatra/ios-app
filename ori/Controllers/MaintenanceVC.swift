//
//  MaintenanceVC.swift
//  ori
//
//  Created by Harsh Ganatra on 10/01/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON

class MaintenanceVC: UIViewController, ReturnProtocol {
    
    var serviceType = ""
    var amcNumber = ""
    var myDevice:MyDevice = MyDevice()
    weak var delegate:ReturnProtocol?
    var myCard = MyCard()
    var timeSlot = ""
    let datePicker = UIDatePicker()
    var selectedAddress = Address()

    @IBOutlet weak var nullAddressText: UITextField!
    @IBOutlet weak var addressNickname: UILabel!
    @IBOutlet weak var addressLine1: UILabel!
    @IBOutlet weak var addressLine2: UILabel!
    @IBOutlet weak var addCity: UILabel!
    @IBOutlet weak var addLandmark: UILabel!
    @IBOutlet weak var addLoader: UIActivityIndicatorView!
    @IBOutlet weak var mornButton: UIButton!
    @IBOutlet weak var aftButton: UIButton!
    @IBOutlet weak var eveButton: UIButton!
    @IBOutlet weak var confirmButton: UIBarButtonItem!
    
    @IBOutlet weak var datePickerText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIBarButtonItem(title: "Don't Know", style: UIBarButtonItemStyle.done, target: self, action: #selector(clearPicker))
        button.tintColor = #colorLiteral(red: 0.4401171207, green: 0.4401280284, blue: 0.4401221871, alpha: 1)
        let numberToolbar: UIToolbar = UIToolbar()
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items=[
            button,
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closePicker))
        ]
        numberToolbar.sizeToFit()
        self.datePickerText.inputAccessoryView = numberToolbar //do it for every relevant textfield if there are more than one
        datePicker.setDate(Date(), animated: false)
        datePicker.timeZone = TimeZone.init(abbreviation: "IST")
        datePicker.minimumDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        //setting max date to one month from tomorrow
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 1, to: nextMonth!)
        //        datePicker.backgroundColor = #colorLiteral(red: 0.9844431281, green: 0.9844661355, blue: 0.9844536185, alpha: 1)
        datePicker.datePickerMode = UIDatePickerMode.date
        datePickerText.inputView = datePicker
        
        self.nullAddressText.isUserInteractionEnabled = false
        let localAddress = OriUtil.getLocalAddress()
        if localAddress._id != "" {
            self.setAddressHolder(localAddress)
        } else {
            self.getAddresses()
        }
    }    

    func getAddresses() {
        self.addLoader.startAnimating()
        
        //call API
        OriNetworking.makeRequest(requestType: .get, url: ApiUrls.getAddressByUserIdUrl, parameters: nil, auth: .send_auth, view: self.view) {(status, error, response) in
            if status {
                if response["status"].bool! {
                    let data = response["data"][0]["address"]
                    for (_,subJson):(String, JSON) in data {
                        var address = Address()
                        address._id = subJson["_id"].stringValue
                        address.addressLine1 = subJson["addressLine1"].stringValue
                        address.addressLine2 = subJson["addressLine2"].stringValue
                        address.city = subJson["city"].stringValue
                        if subJson["nickname"].exists() {
                            address.nickName = subJson["nickname"].stringValue
                        }
                        if subJson["state"].exists() {
                            address.state = subJson["state"].stringValue
                        }
                        if subJson["landmark"].exists() {
                            address.landmark = subJson["landmark"].stringValue
                        }
                        address.zipcode = subJson["zipcode"].stringValue
                        if subJson["isDefault"].exists() {
                            address.isDefault = subJson["isDefault"].boolValue
                        }
                        self.setAddressHolder(address)
                        break
                    } // for loop ends here
                    

                } else {
                    self.nullAddressText.placeholder = "Add a new address"
                }
            }
            self.addLoader.stopAnimating()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.addDetails()
        if self.isMovingFromParentViewController {
            if delegate != nil { //pass mydevice back to AddDeviceVC so that details are saved and can be loaded back
                delegate?.getReturnValue(value: ArrayModel(type: "fromMaint", data: myCard as Any))
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if myCard.schedule != "" {
//            OriUtil.oriLog("mycard schedule, ",myCard.schedule)
            let date = OriUtil.dateFromISO(myCard.schedule)
            datePicker.setDate(date, animated: false)
            datePickerText.text = OriUtil.dateToString(date: datePicker.date)
        }
    }
    
    func addDetails() {
        if datePickerText.text != "" {
            myCard.schedule = OriUtil.getISO8601StringForDate(self.datePicker.date)
        }
    }
    
    @objc func clearPicker () {
        self.datePickerText.resignFirstResponder()
        self.datePickerText.text = ""
    }
    
    @objc func closePicker () {
        self.datePickerText.resignFirstResponder()
        self.datePickerText.text = OriUtil.dateToString(date: self.datePicker.date)
        //        OriUtil.oriLog("date got is ", self.datePicker.date.debugDescription)
    }
    
    @IBAction func forenoon(_ sender: Any) {
        if timeSlot  == "morning" {
            OriUtil.setButton(selected: false, button: self.mornButton)
            self.timeSlot = ""
        } else {
            OriUtil.setButton(selected: true, button: self.mornButton)
            OriUtil.setButton(selected: false, button: self.aftButton)
            OriUtil.setButton(selected: false, button: self.eveButton)
            self.timeSlot = "morning"
        }
    }
    
    @IBAction func afternoon(_ sender: Any) {
        if timeSlot  == "afternoon" {
            OriUtil.setButton(selected: false, button: self.aftButton)
            self.timeSlot = ""
        } else {
            OriUtil.setButton(selected: false, button: self.mornButton)
            OriUtil.setButton(selected: true, button: self.aftButton)
            OriUtil.setButton(selected: false, button: self.eveButton)
            self.timeSlot = "afternoon"
        }
    }
    
    @IBAction func evening(_ sender: Any) {
        if timeSlot  == "evening" {
            OriUtil.setButton(selected: false, button: self.eveButton)
            self.timeSlot = ""
        } else {
            OriUtil.setButton(selected: false, button: self.mornButton)
            OriUtil.setButton(selected: false, button: self.aftButton)
            OriUtil.setButton(selected: true, button: self.eveButton)
            self.timeSlot = "evening"
        }
    }
    
    @IBAction func pickAddress(_ sender: Any) {
        let sb = UIStoryboard(name: "AddressStoryboard", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "selectAddress") as! SelectAddressVC
        vc.selectedAddress = self.selectedAddress
        vc.delegate = self
        self.show(vc, sender: self)
    }
    
    func getReturnValue(value: ArrayModel) {
        if value.type == Values.address_key {
            let address = value.data as! Address
            self.setAddressHolder(address)
        }
    }
    
    func setAddressHolder(_ address:Address) {
        self.nullAddressText.placeholder = ""
        self.selectedAddress = address
        OriUtil.saveLocalAddress(address)
        
        if address.nickName == "" {
            self.addressNickname.isHidden = true
        } else {
            self.addressNickname.isHidden = false
            self.addressNickname.text = address.nickName
        }
        self.addressLine1.text = address.addressLine1
        self.addressLine2.text = address.addressLine2
        self.addCity.text = address.city+" - "+address.zipcode
        
        if address.landmark == "" {
            self.addLandmark.isHidden = true
        } else {
            self.addLandmark.isHidden = false
            self.addLandmark.text = "Landmark: "+address.landmark
        }
        
    }
    
    @IBAction func confirmButton(_ sender: Any) {
        if selectedAddress._id == "" {
            OriUtil.showToast(view: self.view, msg: "Please select an address")
        } else if datePickerText.text == "" {
            OriUtil.showToast(view: self.view, msg: "Please select date")
        } else if self.timeSlot == "" {
            OriUtil.showToast(view: self.view, msg: "Please select a time slot")
        } else {
            self.view.makeToastActivity(.center)
            self.confirmButton.isEnabled = false
            let myDevice = self.myDevice
            var json:[String:Any]  = [
                "userId": LoginHelper.getCurrentUser().1.userID,
                "brandId": myDevice.brandId,
                "categorySlug": myDevice.categorySlug,
                "type": "uws",
                "createdAt": OriUtil.getISO8601StringForCurrentDate(),
                "source": "iOS",
            ]
            
            let bookingDetails:[String:Any]  = [
                "serviceAddressId": selectedAddress._id
            ]
            let uwsTask:[String:Any]  = [
                "warrantyInfo": myCard.warrantyInfo,
                "problem": myCard.problem,
                "amcNumber": amcNumber,
                "service": serviceType,
                "customerPhone": LoginHelper.getCurrentUser().1.phoneNumber,
                "deviceId": myDevice._id
            ]
            let uwsSchedule:[String:Any]  = [
                "date": OriUtil.getISO8601StringForDate(datePicker.date),
                "time": timeSlot
            ]
            let uwsAddress:[String:Any]  = [
                "addressLine1": selectedAddress.addressLine1,
                "addressLine2": selectedAddress.addressLine2,
                "landmark": selectedAddress.landmark,
                "zipcode": selectedAddress.zipcode,
                "city": selectedAddress.city,
                "state": selectedAddress.state
            ]
            json["bookingDetails"] = bookingDetails
            json["uwsTask"] = uwsTask
            json["uwsSchedule"] = uwsSchedule
            json["uwsAddress"] = uwsAddress
            
            //call API
            OriNetworking.makeRequest(requestType: .put, url: ApiUrls.putAddCardUrl, parameters: json, view: self.view) {(status, error, response) in
                self.view.hideToastActivity()
                if status {
                    if response["status"].bool! {
                        self.myCard = OriUtil.parseSingleCardJSON(json: response["cardData"])
                        OriUtil.oriLog("Card from server ",self.myCard)
                        self.performSegue(withIdentifier: "toSuccess", sender: nil)
                    } else {
                        OriUtil.checkErrorFromJSON(view: self.view, json: response)
                    }
                }
                self.confirmButton.isEnabled = true
            }
        }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toSuccess" {
            let vc = segue.destination as! ServiceSuccessVC
            vc.myCard = self.myCard
        }
    }

}
