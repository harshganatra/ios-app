//
//  ServiceHistoryVC.swift
//  ori
//
//  Created by Harsh Ganatra on 01/03/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON

class ServiceHistoryVC: UIViewController, UITableViewDataSource, UITableViewDelegate, ReturnProtocol {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noCardsView: UIView!
    
    var deviceId = ""
    var myCardsArray:[MyCard] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        tableView.register(UINib(nibName:"MyCardsCardLayout",bundle:nil), forCellReuseIdentifier: "myCardsCell")
        tableView.register(UINib(nibName:"MyAmcCardLayout",bundle:nil), forCellReuseIdentifier: "myAmcCell")
    }
    
    func updateData(_ array:[MyCard]) {
        self.myCardsArray = array
        if self.myCardsArray.count == 0 { //if no devices and connections
            self.tableView.isHidden = true
            self.noCardsView.isHidden = false
        } else {
            self.tableView.isHidden = false
            self.noCardsView.isHidden = true
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myCardsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCard = self.myCardsArray[indexPath.item]
        let cell:MyCardsCell!
        
        if myCard.type.lowercased() == "uwa" {
            //for AMC
            cell = tableView.dequeueReusableCell(withIdentifier: "myAmcCell", for:indexPath) as! MyCardsCell
            cell.priceLabel.text = "Rs. "+myCard.amcPlan.amcPrice
            
            cell.priceSymbol.image = cell.priceSymbol.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.priceSymbol.tintColor = #colorLiteral(red: 1, green: 0, blue: 0.3037448227, alpha: 1)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "myCardsCell", for:indexPath) as! MyCardsCell
            cell.cardSchedule.text = myCard.schedule
            //            cell.progressBar.transform = cell.progressBar.transform.scaledBy(x: 1, y: 0.5)
            let status = myCard.status.lowercased()
            if status == "completed" || status == "finished" {
                cell.progressBar.progress = 1.0
                cell.firstStage.alpha = 1.0
                cell.middleStage.alpha = 1.0
                cell.finalStage.alpha = 1.0
                cell.middleText.alpha = 1.0
                cell.finalText.alpha = 1.0
                cell.middleStage.image = #imageLiteral(resourceName: "card_stage1")
                cell.progressBar.isHidden = false
                cell.firstStage.isHidden = false
                cell.finalStage.isHidden = false
                cell.firstText.isHidden = false
                cell.finalText.isHidden = false
                cell.middleText.text = "Confirmed"
            } else if status == "confirmed" || status == "ongoing" {
                cell.progressBar.progress = 0.75
                cell.firstStage.alpha = 1.0
                cell.middleStage.alpha = 1.0
                cell.finalStage.alpha = 0.4
                cell.middleText.alpha = 1.0
                cell.finalText.alpha = 0.4
                cell.firstStage.isHidden = false
                cell.finalStage.isHidden = false
                cell.progressBar.isHidden = false
                cell.middleStage.image = #imageLiteral(resourceName: "card_stage1")
                cell.firstText.isHidden = false
                cell.finalText.isHidden = false
                cell.middleText.text = "Confirmed"
            } else if status == "booked" {
                cell.progressBar.progress = 0.25
                cell.firstStage.alpha = 1.0
                cell.middleStage.alpha = 0.4
                cell.finalStage.alpha = 0.4
                cell.middleText.alpha = 0.4
                cell.finalText.alpha = 0.4
                cell.progressBar.isHidden = false
                cell.firstStage.isHidden = false
                cell.finalStage.isHidden = false
                cell.middleStage.image = #imageLiteral(resourceName: "card_stage1")
                cell.firstText.isHidden = false
                cell.finalText.isHidden = false
                cell.middleText.text = "Confirmed"
            } else if status == "cancelled" {
                cell.progressBar.isHidden = true
                cell.middleStage.alpha = 1.0
                cell.firstStage.isHidden = true
                cell.finalStage.isHidden = true
                var image =  #imageLiteral(resourceName: "card_cancelled")
                image = image.scaled(to: CGSize(width: 30, height: 30))
                cell.middleStage.image = image
                cell.firstText.isHidden = true
                cell.finalText.isHidden = true
                cell.middleText.text = "Cancelled"
            }
        }
        
        DownloadAndReadImage().renderLocalImage(imageName: myCard.brandLogoName, imageView: cell.brandImage, folderName: Values.amazonBrandsFolder)
        
        cell.deviceTag.text = myCard.title
        cell.deviceModel.text = myCard.subTitle
        cell.cardStatus.text = myCard.status
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //TODO send to card details
        let myCard = self.myCardsArray[indexPath.item]
        
        if myCard.type.lowercased() == "uwa" {
            let sb = UIStoryboard(name: "MyCardsStoryboard", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "amcDetails") as! AMCDetailsVC
            vc.myCard = myCard
            vc.delegate = self
            self.show(vc, sender: nil)
        } else {
            let sb = UIStoryboard(name: "MyCardsStoryboard", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "bookingDetails") as! CardDetailsVC
            vc.myCard = myCard
            vc.delegate = self
            self.show(vc, sender: nil)
        }
    }
    
    func fetchData(deviceId:String) {
        self.deviceId = deviceId
        self.view.makeToastActivity(.center)
        
        //call API
        OriNetworking.makeRequest(requestType: .get, url: ApiUrls.cardsByDevice+"?deviceID="+deviceId, view: self.view) {(status, error, response) in
            var myCardsCurrentArray:[MyCard] = []
            var myCardsHistoryArray:[MyCard] = []
            var myCardsCancelledArray:[MyCard] = []
            
            if status {
                if response["status"].bool! {
                    let data = response["data"]
                    
                    
                    for (_,subJson):(String, JSON) in data {
                        let myCard = OriUtil.parseSingleCardJSON(json: subJson)
                        
                        if (!(myCard.status.lowercased() == "finished") &&
                            !(myCard.status.lowercased() == "cancelled")
                            && !(myCard.status.lowercased() == "completed")) {
                            myCardsCurrentArray.append(myCard)
                        } else if myCard.status.lowercased() == "cancelled" {
                            myCardsCancelledArray.append(myCard)
                        } else {
                            myCardsHistoryArray.append(myCard)
                        }
                    } // for loop ends here
                    
                }
            }
            self.view.hideToastActivity()
            myCardsHistoryArray.append(contentsOf: myCardsCancelledArray)
            myCardsCurrentArray.append(contentsOf: myCardsHistoryArray)
            self.updateData(myCardsCurrentArray)
        }
    }
    
    func getReturnValue(value: ArrayModel) {
        if value.type == "removeCard" {
            self.fetchData(deviceId: self.deviceId)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
