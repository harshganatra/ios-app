//
//  HomeController.swift
//  ori
//
//  Created by Harsh Ganatra on 06/10/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import CoreGraphics

class HomeController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate {
    
//    weak var splashScreen:ReturnProtocol?
    var goToPage:toPage = .none
    var refreshControl:UIRefreshControl!
    let reuseIdentifier = "homeSlider"
    var sliderContent: [SliderContent] = []
    var scrollContent: [ScrollContent] = []
    
    //redirect variables
    var animatePages = true //set to false in case calling HomeVC just to redirect
    var myDevice = MyDevice()
    var serviceType = ""
    
    public enum toPage: String {
        case none     = ""
        case login    = "login"
        case logout    = "logout"
        case rate     = "rate"
        case share   = "share"
        case feedback  = "feedback"
        case help   = "help"
        case mycards = "mycards"
        case mydevices = "mydevices"
//        case home = "home"
        case account = "account"
        case adddevice = "adddevice"
        case repair = "Repair"
        case maintenance = "Maintenance"
        case installation = "Installation"
    }
    
    @IBOutlet weak var open: UIBarButtonItem!
    @IBOutlet weak var topButtonView: UIView!
    @IBOutlet weak var myDevicesCount: UIButton!
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var scrollContentTableView: UITableView!
    @IBOutlet weak var scrollNullView: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBAction func login(_ sender: Any) {
        self.openPage(.login)
    }
    
    @IBAction func trackBookings(_ sender: Any) {
        self.openPage(.mycards)
    }
    
    @IBAction func myDevices(_ sender: Any) {
        if LoginHelper.isLoggedIn() {
            self.openPage(.mydevices)
        } else {
            OriUtil.showToast(view: self.view, msg: "You need to login to see your device portfolio")
        }
    }
    
    @IBAction func addDevice(_ sender: Any) {
        self.openPage(.adddevice)
    }
    
    @IBOutlet weak var reqService: UIBarButtonItem!
    @IBAction func requestService(_ sender: Any) {
        let pickerController = UIAlertController()
        pickerController.title = "Request Service:"
        
        let repairAction = UIAlertAction(title:"Repair", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            let sb = UIStoryboard(name: "SelectDeviceStoryboard", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "SelectDeviceScreen") as! SelectDeviceVC
            vc.serviceType = "Repair"
            vc.page = .reqService
            self.showVC(vc)
        }
        var repair = UIImage(named: "repair")
        repair = repair?.scaled(to: CGSize(width: 35, height: 35))
        repairAction.setValue(repair, forKey: "image")
        
        let maintAction = UIAlertAction(title:"Maintenance", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            let sb = UIStoryboard(name: "SelectDeviceStoryboard", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "SelectDeviceScreen") as! SelectDeviceVC
            vc.serviceType = "Maintenance"
            vc.page = .reqService
            self.showVC(vc)
        }
        var image = UIImage(named: "maintenance")
        image = image?.scaled(to: CGSize(width: 35, height: 35))
        maintAction.setValue(image, forKey: "image")
        
        let installAction = UIAlertAction(title:"Installation", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            let sb = UIStoryboard(name: "SelectDeviceStoryboard", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "SelectDeviceScreen") as! SelectDeviceVC
            vc.serviceType = "Installation"
            vc.page = .reqService
            self.showVC(vc)
            //            self.dismiss(animated: true, completion: nil)
        }
        var image2 = UIImage(named: "installation")
        image2 = image2?.scaled(to: CGSize(width: 35, height: 35))
        installAction.setValue(image2, forKey: "image")
        
        let cancel = UIAlertAction(title:"Close", style: UIAlertActionStyle.cancel) { action in
            pickerController.dismiss(animated: true, completion: nil)
        }
        
        pickerController.addAction(cancel)

        pickerController.addAction(repairAction)
        pickerController.addAction(maintAction)
        pickerController.addAction(installAction)
        pickerController.popoverPresentationController?.barButtonItem = self.reqService // works for both iPhone & iPad

        pickerController.view.tintColor = #colorLiteral(red: 0.4401171207, green: 0.4401280284, blue: 0.4401221871, alpha: 1)
        
        self.present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction func buyAMC(_ sender: Any) {
        OriUtil.showToast(view: self.view, msg: "Coming soon!")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set up the navigation drawer
        let controller = self.revealViewController()
        controller?.rearViewRevealOverdraw = 0.0
        controller?.rearViewRevealDisplacement = 0.0
        open.target = controller
        open.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.scrollContentTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        scrollContentTableView.register(UINib(nibName:"ScrollContentLayout",bundle:nil), forCellReuseIdentifier: "scrollContentCell")
        scrollContentTableView.register(UINib(nibName:"MyConnectionsCardLayout",bundle:nil), forCellReuseIdentifier: "myConnectionsCell")
        
        self.refreshControl = UIRefreshControl()
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            self.scrollContentTableView.refreshControl = refreshControl
        } else {
            self.scrollContentTableView.addSubview(refreshControl)
        }
        self.refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        //load data
        self.loadSliderData()
        
    }
    
    @objc func refreshData(_ sender: Any! = nil) {
        self.loadSliderData()
        self.updateUserLogin()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        OriUtil.oriLog("view will appear")
        
        //string received from navigation drawer
        self.openPage(goToPage)
        goToPage = .none
    }
    
    func updateUserLogin() {
        if LoginHelper.isLoggedIn() {
            self.loginButton.isHidden = true
            self.scrollContentTableView.isHidden = false
            self.loadScrollContentData()
            
            //call device count API
            OriNetworking.makeRequest(requestType: .get, url: ApiUrls.getDeviceCount, parameters: nil, auth: .send_auth) {(status, error, response) in
                if status {
                    if response["status"].bool! {
                        let data = response["data"]
                        let deviceCount = data["totalDeviceCount"].intValue
                        self.myDevicesCount.setTitle(String(deviceCount), for: .normal)
                    } else {
                        self.myDevicesCount.setTitle("", for: .normal)
                    }
                }
            }
            
        } else { //logged out
//            OriUtil.oriLog("user logged out")
            self.myDevicesCount.setTitle("", for: .normal)
            
            self.scrollContentTableView.isHidden = true
            self.scrollNullView.isHidden = true
            self.loginButton.isHidden = false
        }
    }
    
    func openPage(_ page:toPage) {
        switch page {
        case .login:
//            OriUtil.oriLog("trying to login")
            let storyboard: UIStoryboard = UIStoryboard(name: "LoginStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginScreen") as UIViewController
            self.showVC(vc)
        case .logout:
            let alertVC = UIAlertController(title: "Logout", message: "Do you really want to logout?", preferredStyle: .alert)
            alertVC.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_:UIAlertAction) in
                OriUtil.oriLog("trying to logout")
                let defaults = UserDefaults.standard
                defaults.set("", forKey: Values.TOKEN_KEY)
                defaults.set("", forKey: Values.USERID_KEY)
                defaults.set("", forKey: Values.FIRST_NAME_KEY)
                defaults.set("", forKey: Values.LAST_NAME_KEY)
                defaults.set("", forKey: Values.EMAIL_KEY)
                defaults.set("", forKey: Values.MOBILE_KEY)
                defaults.set("", forKey: Values.GENDER_KEY)
                defaults.set("", forKey: Values.ISO_DOB_KEY)
                defaults.synchronize()
                
                self.updateUserLogin()
            }))
            alertVC.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alertVC.presentInOwnWindow(animated: true, completion: nil)
        case .mydevices:
            let storyboard: UIStoryboard = UIStoryboard(name: "MyDevicesStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyDevicesScreen") as UIViewController
            self.showVC(vc)
        case .mycards:
            let storyboard: UIStoryboard = UIStoryboard(name: "MyCardsStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyCardsScreen") as UIViewController
            self.showVC(vc)
        case .adddevice:
            let storyboard: UIStoryboard = UIStoryboard(name: "AddDeviceStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddDeviceScreen") as UIViewController
            self.showVC(vc)
        case .repair:
            let storyboard = UIStoryboard(name: "ReqServiceStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "repairScreen") as! RepairVC
            vc.myDevice = self.myDevice
            self.myDevice = MyDevice()
            vc.serviceType = goToPage.rawValue
            self.showVC(vc)
        case .maintenance:
            let storyboard = UIStoryboard(name: "ReqServiceStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "maintScreen") as! MaintenanceVC
            vc.myDevice = self.myDevice
            self.myDevice = MyDevice()
            vc.serviceType = goToPage.rawValue
            self.showVC(vc)
        case .installation:
            let storyboard = UIStoryboard(name: "ReqServiceStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "maintScreen") as! MaintenanceVC
            vc.myDevice = self.myDevice
            self.myDevice = MyDevice()
            vc.serviceType = goToPage.rawValue
            self.showVC(vc)
        case .account:
            let storyboard: UIStoryboard = UIStoryboard(name: "UserAccountStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserAccount") as UIViewController
            self.showVC(vc)
        case .help:
            let storyboard: UIStoryboard = UIStoryboard(name: "HelpStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HelpScreen") as UIViewController
            self.showVC(vc)
        case .feedback:
            self.performSegue(withIdentifier: "feedback", sender: nil)
        case .share:
//            self.updateUserLogin()
            let textToShare = "Hi, download the fastest way to avail complete care for all your appliances and gadgets! Get the ORI app for Android and iOS at https://goo.gl/rlu6rt today!"
            
//            if let myWebsite = NSURL(string: "http://www.codingexplorer.com/") {
//                let objectsToShare = [textToShare, myWebsite] as [Any]
            let objectsToShare = [textToShare] as [Any]

                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
//                activityVC.popoverPresentationController?.sourceView = sender
                self.present(activityVC, animated: true, completion: nil)
//            }
        case .rate:
//            self.updateUserLogin()
            
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id1103151565"),
                UIApplication.shared.canOpenURL(url)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                OriUtil.oriLog("Cannot open URL")
            }
//            UIApplication.shared.openURL(URL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4")!)
//            UIApplication.shared.open(URL(string: "itms://itunes.apple.com/de/app/ori-brand-authorized-service/id1103151565?ls=1&mt=8")!, options: [:], completionHandler: nil)
            
        default:
            self.updateUserLogin()
            OriUtil.oriLog(page)
        }
    }
    
    func showVC(_ vc:UIViewController) {
        if animatePages {
            self.show(vc, sender: self)
        } else {
            self.animatePages = true
            UIView.performWithoutAnimation {
                self.show(vc, sender: self)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //OriUtil.oriLog("slider count is \(self.sliderContent.count)")
        return self.sliderContent.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! HomeSliderCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.myLabel.text = self.sliderContent[indexPath.item].title
        //        OriUtil.oriLog(self.sliderContent[indexPath.item].imageURL)
        cell.imageView.contentMode = .scaleAspectFit
        cell.imageView.sd_setShowActivityIndicatorView(true)
        cell.imageView.sd_setIndicatorStyle(.gray)
        cell.imageView.sd_setImage(with: URL(string: self.sliderContent[indexPath.item].imageURL) , completed: { (image, error, cacheType, imageURL) in
            // url call completed
            if image == nil { //image failed to load from url
                cell.imageView.image = UIImage(named: "error.png")
            }
        })
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        let url = sliderContent[indexPath.item].url
        OriUtil.oriLog("url is ",url)
        self.performSegue(withIdentifier: "webContent", sender: url)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width / 1.1 , height: self.sliderCollectionView.bounds.height/1.1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if scrollContent.count == 0 {
            return 0
        } else {
            return (scrollContent.count + 1)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if  indexPath.item == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for:indexPath)
            cell.textLabel?.text = "News and Updates:"
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else {
            let scroll = self.scrollContent[indexPath.item-1] //-1 to take header into account
            let key = scroll.type.lowercased()
            if key == "dishtv" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "scrollContentCell", for:indexPath) as! ScrollContentCell
                
                let myConnection:MyConnection = scroll.myConnection
                DownloadAndReadImage().renderLocalImage(imageName: myConnection.brandLogoName, imageView: cell.brandImage, folderName: Values.amazonBrandsFolder)
                
                cell.title.text = myConnection.listName
                cell.subTitle.text = "Widget not available in this version of app"
                
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "scrollContentCell", for: indexPath) as! ScrollContentCell
                
                DownloadAndReadImage().renderLocalImage(imageName: scroll.imageURL, imageView: cell.brandImage, folderName: Values.amazonBrandsFolder)
                
                cell.title.text = scroll.title
                cell.subTitle.text = scroll.subtitle
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sc = self.scrollContent[indexPath.item-1] //-1 to take header into account
        let type = sc.type.lowercased()
        
        if type == "missingdateofpurchase" || type == "missinginvoice" {
            let storyboard = UIStoryboard(name: "MyDevicesStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "deviceDetails") as! DeviceDetailsVC
            vc.myDevice = sc.myDevice
            self.showVC(vc)
        } else if type == "warranty" {
            if sc.myDevice.amcAvailable {
                //TODO send to Buy AMC page
            } else {
                //send to device details here
            }
            let storyboard = UIStoryboard(name: "MyDevicesStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "deviceDetails") as! DeviceDetailsVC
            vc.myDevice = sc.myDevice
            self.showVC(vc)
        } else if type == "card" {
            let sb = UIStoryboard(name: "MyCardsStoryboard", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "bookingDetails") as! CardDetailsVC
            vc.myCard = sc.myCard
            self.showVC(vc)
        }
    }
    
    func loadSliderData() {
        //call slider API
        OriNetworking.makeRequest(requestType: .get, url: ApiUrls.homeSliderURL, parameters: nil, auth: .default_auth) {(status, error, response) in
            if status {
                if response["status"].bool! {
                    let data = response["data"]
                    self.sliderContent = []
                    for (_,subJson):(String, JSON) in data {
                        var slider: SliderContent = SliderContent()
                        slider.title = subJson["title"].stringValue
                        slider.type = subJson["type"].stringValue
                        if slider.type == "blog" {
                            slider.url = subJson["url"].stringValue
                            slider.imageURL = subJson["image_url"].stringValue
                            self.sliderContent.append(slider)
                        }
                    } // for loop ends here
                    self.sliderCollectionView.reloadData()
                }
            }
        }
    }
    
    func loadScrollContentData() {
        self.refreshControl.beginRefreshing()

        //call slider API
        OriNetworking.makeRequest(requestType: .get, url: ApiUrls.gethomePageUpdates, view: self.view) {(status, error, response) in
            if status {
                if response["status"].bool! {
                    let data = response["data"]
                    self.scrollContent = []
                    for (_,subJson):(String, JSON) in data {
                        var sc: ScrollContent = ScrollContent()
                        let type = subJson["type"].stringValue.lowercased()
                        
                        let brandId = subJson["brandId"].intValue
                        if brandId == 9999 {
                            sc.imageURL = "ori.png"
                        } else {
                            let b = (try! BrandDataHelper.find(id:brandId))!
                            sc.imageURL = b.brandLogoName
                        }
                        
                        var devicedesc = "Device";
                        if (subJson["shortName"].exists()) {
                            if subJson["deviceTag"].exists() {
                                devicedesc = subJson["shortName"].stringValue + " (" + subJson["deviceTag"].stringValue + ")"
                            } else {
                                devicedesc = subJson["shortName"].stringValue
                            }
                        }
                        
                        if type.contains("warranty") {
                            let dop = subJson["dateOfPurchase"].stringValue
                            sc.title = "Warranty expiring for " + devicedesc + " in " + String(subJson["warrantyRemaining"].intValue) + " days."
                            sc.subtitle = "It was purchased on " + OriUtil.getDateStringFromISO(dop) + "."
                            sc.type = "warranty"
                            sc.myDevice = OriUtil.parseSingleDeviceJSON(json: subJson)
                            self.scrollContent.append(sc)
                        } else if type.contains("activecards") {
                            sc.title = "Tracking card for " + devicedesc
                            sc.subtitle = "Status: " + subJson["status"].stringValue
                            sc.type = "card"
                            sc.myCard = OriUtil.parseSingleCardJSON(json: subJson)
                            sc.cardNumber = sc.myCard.cardNumber
                            self.scrollContent.append(sc)
                        } else if type.contains("missing") {
                            sc.myDevice = OriUtil.parseSingleDeviceJSON(json: subJson)
                            
                            let length:Int = "missing".count
                            let indexUpto = type.index(type.startIndex, offsetBy: length)
                            var value1:String = type[indexUpto...] + ""
                            
                            
                            if (value1 == ("dateofpurchase")) {
                                value1 = "date of purchase";
                                sc.type = "missingDateOfPurchase"
                            } else {
                                value1 = "invoice";
                                sc.type = "missingInvoice"
                            }
                            sc.title = devicedesc
                            sc.subtitle = "Click to add " + value1 + "."
                            self.scrollContent.append(sc)
                        } else if type.contains("dishtv") {
                            sc.myConnection = OriUtil.parseSingleConnectionJSON(json: subJson)
                            sc.type = type
//                            self.scrollContent.append(sc)
                        }
                        
                    } // for loop ends here
                    self.scrollContentTableView.reloadData()
                    
                    if self.scrollContent.count == 0 {
                        self.scrollNullView.isHidden = false
                        self.scrollContentTableView.isHidden = false
                    } else {
                        self.scrollNullView.isHidden = true
                        self.scrollContentTableView.isHidden = false
                    }
                } else { //false status from server
                    self.scrollNullView.isHidden = false
                    self.scrollContentTableView.isHidden = false
                }
            }
            self.refreshControl.endRefreshing()
        } //network request
    }
    
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.identifier == "webContent" {
            let vc = segue.destination as! WebContentVC
            vc.url = sender as! String
        }
     }
    
    
}

class HomeSliderCell: UICollectionViewCell {
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}

class ScrollContentCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var subTitle: UILabel!
    
}





