//
//  RepairVC.swift
//  ori
//
//  Created by Harsh Ganatra on 10/01/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON

class RepairVC: UIViewController, ReturnProtocol{
    
    var serviceType = ""
    var myDevice:MyDevice = MyDevice()
    var problemTexts:[ListWithCode] = []
    var myCard:MyCard = MyCard()
    var warrantyInfo = ""
    var problemList:[ArrayModel] = []
    let modelKey = "model"
    let other = "Other..."
    let textDelegate = TextFieldDelegate()
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var insetView: UIView!
    @IBOutlet weak var warrButton: UIButton!
    @IBOutlet weak var amcButton: UIButton!
    @IBOutlet weak var noneButton: UIButton!
    @IBOutlet weak var warrQuest: UILabel!
    @IBOutlet weak var AMCNumber: UITextField!
    @IBOutlet weak var problemPickerText: UITextField!
    @IBOutlet weak var problemText: UITextView!
    @IBOutlet weak var problemPickerHolder: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.AMCNumber.delegate = textDelegate
        problemText.layer.borderWidth = 0.5
        problemText.layer.borderColor = UIColor.lightGray.cgColor
        problemText.contentInset = UIEdgeInsetsMake(4.0, 4.0, 4.0, 4.0)
        self.getProblemData()
        
        let numberToolbar2: UIToolbar = UIToolbar()
        numberToolbar2.barStyle = UIBarStyle.default
        let button = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closeKeyboard))
        button.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        numberToolbar2.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button
        ]
        numberToolbar2.sizeToFit()
        self.problemText.inputAccessoryView = numberToolbar2
    }
    
    @objc func closeKeyboard() {
        self.problemText.resignFirstResponder()
    }

    @IBAction func warrClicked(_ sender: Any) {
        if warrantyInfo  == "Yes" {
            self.setButton(selected: false, button: self.warrButton)
            self.warrantyInfo = ""
            warrQuest.isHidden = true
        } else {
            warrQuest.text = "(Bill/Warranty card required at the time of visit)"
            warrQuest.isHidden = false
            self.AMCNumber.isHidden = true
            self.setButton(selected: true, button: self.warrButton)
            self.setButton(selected: false, button: self.amcButton)
            self.setButton(selected: false, button: self.noneButton)
            self.warrantyInfo = "Yes"
        }
    }
    
    @IBAction func amcClicked(_ sender: Any) {
        if warrantyInfo  == "Extended Warranty" {
            self.setButton(selected: false, button: self.amcButton)
            self.warrantyInfo = ""
            warrQuest.isHidden = true
        } else {
            warrQuest.text = "(AMC certificate may be required at the time of visit)"
            warrQuest.isHidden = false
            self.AMCNumber.isHidden = false
            self.setButton(selected: false, button: self.warrButton)
            self.setButton(selected: true, button: self.amcButton)
            self.setButton(selected: false, button: self.noneButton)
            self.warrantyInfo = "Extended Warranty"
        }
    }
    
    @IBAction func noneClicked(_ sender: Any) {
        if warrantyInfo  == "No" {
            self.setButton(selected: false, button: self.noneButton)
            self.warrantyInfo = ""
            warrQuest.isHidden = true
        } else {
            warrQuest.text = ""
            warrQuest.isHidden = true
            self.AMCNumber.isHidden = true
            self.setButton(selected: false, button: self.warrButton)
            self.setButton(selected: false, button: self.amcButton)
            self.setButton(selected: true, button: self.noneButton)
            self.warrantyInfo = "No"
        }
    }
    
    @IBAction func selectProblem(_ sender: Any) {
        let story = UIStoryboard(name: "PickerVC", bundle: nil)
        let pickerVC = story.instantiateViewController(withIdentifier: "pickerVC") as! PickerVC
        pickerVC.delegate = self
        pickerVC.key = self.modelKey
        pickerVC.totalArray = problemList
        pickerVC.totalSearchResults = problemList
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    func setButton(selected:Bool , button:UIButton!) {
        if selected {
            button.layer.borderWidth = 1.0
            button.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0.3037448227, alpha: 1)
            button.backgroundColor = UIColor.white
        } else {
            button.layer.borderWidth = 0.0
            button.backgroundColor = #colorLiteral(red: 0.9593952298, green: 0.9594177604, blue: 0.959405601, alpha: 1)
        }
    }
    
    @IBAction func nextButton(_ sender: Any) {
        if !(problemText.isHidden) {
            myCard.problem = problemText.text
        }
        
        if self.warrantyInfo == "" {
            OriUtil.showToast(view: self.view, msg: "Select warranty type")
        } else if myCard.problem == "" {
            OriUtil.showToast(view: self.view, msg: "Specify the problem details")
        } else {
            myCard.warrantyInfo = self.warrantyInfo
            self.performSegue(withIdentifier: "toMaint", sender: self)
        }
    }
    
    func getReturnValue(value: ArrayModel) {
        if value.type == "fromMaint" {
            self.myCard = (value.data as! MyCard)
        } else if value.type == modelKey {
            let str = value.data as! ListWithCode
            if str.problemText != problemPickerText.text {
                self.problemPickerText.text = str.problemText
                myCard.problem = str.problemCode
                
                if str.problemText == other {
                    self.problemText.isHidden = false
                } else {
                    self.problemText.isHidden = true
                    self.problemText.text = ""
                }
            }
        }
    }
    
    func getProblemData() {
        self.view.makeToastActivity(.center)
        
        let json:[String:Any]  = [
            "brandId": myDevice.brandId,
            "listName": myDevice.listName,
            "category": myDevice.category,
            "subCategory": myDevice.subCategory
        ]
        
        //call API
        OriNetworking.makeRequest(requestType: .post, url: ApiUrls.checkProblems, parameters: json, auth: .send_auth, view: self.view) {(status, error, response) in
            self.view.hideToastActivity()
            if status {
                if response["status"].bool! {
                    let data = response["data"]
                    self.problemList = []
                    for (_,subJson):(String, JSON) in data {
                        let model  = ListWithCode(problemText: subJson["problemText"].stringValue, problemCode: subJson["problem"].stringValue)
//                        OriUtil.oriLog("problem is ",model.problemText)
                        self.problemList.append(ArrayModel(type: self.modelKey, data: model))
                    } // for loop ends here
                    if self.problemList.count > 0 {
                        self.problemText.isHidden = true
                        self.problemPickerHolder.isHidden = false
                    } else {
                        self.problemText.isHidden = false
                        self.problemPickerHolder.isHidden = true
                    }
                    let model  = ListWithCode(problemText: self.other, problemCode: "")
                    self.problemList.append(ArrayModel(type: self.modelKey, data: model))
                    
                } else {
                    self.problemPickerHolder.isHidden = true
                    self.problemText.isHidden = false
                }
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toMaint" {
            let vc = segue.destination as! MaintenanceVC
            vc.myDevice = myDevice
            vc.serviceType = serviceType
            vc.amcNumber = self.AMCNumber.text!
            vc.myCard = myCard
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.unsubscribeToKeyboardNotifications()
    }
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(_ :)),
                                               name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: .UIKeyboardWillHide, object: nil)
    }
    func unsubscribeToKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyBoardWillShow(_ notification:Notification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        
//        var contentInset:UIEdgeInsets = self.scrollView.contentInset
//        contentInset.bottom = getKeyboardHeight(notification)
//        scrollView.contentInset = contentInset
        
        if !(self.problemText.isHidden) {
            let scrollTo = self.problemText!.frame.origin.y + self.problemText!.frame.height
            self.scrollView.setContentOffset(CGPoint(x:0, y:scrollTo), animated: false)
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification){
//        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
//        scrollView.contentInset = contentInset
        
        if !(self.problemText.isHidden) {
//            let scrollTo = self.problemText!.frame.origin.y + self.problemText!.frame.height
            self.scrollView.setContentOffset(CGPoint(x:0, y:0), animated: false)
        }
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }
    
}
