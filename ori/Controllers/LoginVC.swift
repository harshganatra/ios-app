//
//  LoginVC.swift
//  ori
//
//  Created by Harsh Ganatra on 18/10/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import UIKit
import Toast_Swift
import SwiftyJSON

class LoginVC: UIViewController {

    @IBOutlet weak var phoneTextField: UITextField!
    let phoneDelegate = NumericFieldDelegate()
    var toPage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.phoneTextField.delegate = phoneDelegate
        
        let numberToolbar: UIToolbar = UIToolbar()
        numberToolbar.barStyle = UIBarStyle.default
        let button = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closeKeyboard))
        button.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        numberToolbar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button
        ]
        numberToolbar.sizeToFit()
        
        self.phoneTextField.inputAccessoryView = numberToolbar //do it for every relevant textfield if there are more than one
    }
    
    @IBAction func tryLogin(_ sender: Any) {
        let phone = self.phoneTextField.text
        self.closeKeyboard()
        if let _=phone, Validation.isValidPhoneNumber(textField: self.phoneTextField) {
            let json:[String:Any]  = [
                "phone": phone!,
                "key": OriUtil.returnSHA(data: phone!,key: "secretkeyforexists")
            ]
            OriUtil.oriLog(json)
            
            //call API
            OriNetworking.makeRequest(requestType: .post, url: ApiUrls.customerExistsUrl, parameters: json, auth: .no_auth, view: self.view) {(status, error, response) in
                if status {

                    if response["status"].bool! {
                        let data = response["data"]
                        self.performSegue(withIdentifier: "loginOTP", sender: data)
                        
                    } else {
                        //go to sign up page
                        self.performSegue(withIdentifier: "signUp", sender: phone)
                    }
                }
                
            }
        } else {
            OriUtil.showToast(view: self.view, msg: "Enter a valid phone number")
        }
    }
    
    @objc func closeKeyboard () {
        self.phoneTextField.resignFirstResponder()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "signUp") {
            let signUpVC = segue.destination as! SignUpVC
            let phone = sender as! String
            signUpVC.phone = phone
            signUpVC.toPage = self.toPage
        } else if segue.identifier == "loginOTP" {
            let data = sender as! JSON
            
            let otpVC = segue.destination as! OtpVC
            otpVC.phone = self.phoneTextField.text
            otpVC.firstName = data["profile"]["firstName"].stringValue
            otpVC.lastName = data["profile"]["lastName"].stringValue
            
            if (data["profile"]["gender"].exists()) {
                otpVC.gender = data["profile"]["gender"].stringValue
            } else {
                otpVC.gender = ""
            }
            
            if (data["profile"]["dateOfBirth"].exists()) {
                let dob = data["profile"]["dateOfBirth"].stringValue
                if dob == Values.defaultDate {
                    otpVC.dob = ""
                } else {
                    otpVC.dob = dob
                }
            } else {
                otpVC.dob = ""
            }
            
            otpVC.emailID = data["emails"].arrayValue[0].dictionaryValue["address"]?.stringValue
            otpVC.toPage = self.toPage
        }
        
    }
    

}





