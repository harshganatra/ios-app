//
//  OriRefundPolicyVC.swift
//  ori
//
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import WebKit

class OriRefundPolicyVC: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.makeToastActivity(.center)
        
        webView.navigationDelegate = self
        
        let str = "<html><body style=\"text-align:justify;color:#5d5d5d;font-size:48px;background-color:#FAFAFA; \">"+"<p>Ori does not provide the services listed and Ori is not a service provider. It is a SOFTWARE or a Technology company which enables the user to book Services. These services are provided by various Brands who are associated with Ori.</p><p>On Ori user can access multiples post sales services offered by brands - ranging from after-sales service appointments to usage assistance to content/FAQs to subscription management to recharge of DTH / Broadband subscriptions.</p><p>Ori doesn\'t charge the user for any post sales service and charges are at the discretion of Service provider policies in which case Service providers Refund and cancellation policy takes into effect.</p><p>For any transaction done by user on Ori platform:</p><ol><li>Refund will be initiated if Cancellation is applicable</li><li>All sales of Recharge are final and there will be no refund or exchange permitted.</li><li>Please be advised that you are responsible for the mobile number, account number and other personal details you provide for DTH &amp; Broadband Recharge and all charges that result from those purchases.</li><li>Ori is your recharge partner. We provide online recharge for DTH. if you get recharge for some package at Ori and  DTH company include or exclude any channel from that package. We will not be responsible for that channel exclusion (removal) from your DTH package.</li><li>In case of payment failure we request you to send an email to support@oriserve.com with the following details: the mobile number or DTH/Broadband account number, name, recharge value, transaction date and order Number. Ori shall investigate the incident and if it is found that money was indeed charged to your card or bank account without delivery of the Recharge then you will be refunded the money within 6 working days from the date of the receipt of your email.</li>"+"</body></html>"
        
        webView.loadHTMLString(str, baseURL: nil)
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if webView.isLoading {
            return
        }
        self.view.hideToastActivity()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
