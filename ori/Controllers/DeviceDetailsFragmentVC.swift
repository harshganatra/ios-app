//
//  DeviceDetailsFragmentVC.swift
//  ori
//
//  Created by Harsh Ganatra on 01/03/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import MobileCoreServices

class DeviceDetailsFragmentVC: UIViewController, UIDocumentPickerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentInteractionControllerDelegate {
    //UINavigationControllerDelegate is for camera picker
    //UIDocumentInteractionControllerDelegate is to view files

    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var deviceCategory: UILabel!
    @IBOutlet weak var deviceModel: UILabel!
    @IBOutlet weak var deviceNickname: UILabel!
    @IBOutlet weak var verifiedIcon: UIImageView!
    @IBOutlet weak var nickname: UILabel!
    @IBOutlet weak var purchaseDate: UILabel!
    @IBOutlet weak var dateAdded: UILabel!
    @IBOutlet weak var warrantyText: UILabel!
    @IBOutlet weak var serialNo: UILabel!
    @IBOutlet weak var modelNo: UILabel!
    @IBOutlet weak var retailer: UILabel!
    @IBOutlet weak var listName: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var catHolder: UIStackView!
    @IBOutlet weak var subcategory: UILabel!
    @IBOutlet weak var subcatHolder: UIStackView!
    @IBOutlet weak var toggleDetails: UIStackView!
    
    @IBOutlet weak var addDevicePhotoHolder: UIStackView!
    @IBOutlet weak var devicePhoto: UIImageView!
    @IBOutlet weak var warrantyHolder: UIStackView!
    @IBOutlet weak var warranty: UIImageView!
    @IBOutlet weak var invoiceHolder: UIStackView!
    @IBOutlet weak var invoice: UIImageView!
    
    @objc public enum PhotoType:Int {
        
        case devicePhoto = 1
        case warranty = 2
        case invoice = 3
    }
    
    weak var delegate:ReturnProtocol?
    var myDevice:MyDevice = MyDevice()
    let imageHelper = DownloadAndReadImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        self.updateData()
//    }
    func registerTouch() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(chooseDeviceImage))
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressDeviceImage))
        addDevicePhotoHolder.isUserInteractionEnabled = true
        addDevicePhotoHolder.addGestureRecognizer(tapGestureRecognizer)
        addDevicePhotoHolder.addGestureRecognizer(longPressGesture)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(chooseWarranty))
        let longPressGesture2 = UILongPressGestureRecognizer(target: self, action: #selector(longPressWarranty))
        warrantyHolder.isUserInteractionEnabled = true
        warrantyHolder.addGestureRecognizer(tapGestureRecognizer2)
        warrantyHolder.addGestureRecognizer(longPressGesture2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(chooseInvoice))
        let longPressGesture3 = UILongPressGestureRecognizer(target: self, action: #selector(longPressInvoice))
        invoiceHolder.isUserInteractionEnabled = true
        invoiceHolder.addGestureRecognizer(tapGestureRecognizer3)
        invoiceHolder.addGestureRecognizer(longPressGesture3)
    }
    
    func updateData() {
        self.deviceCategory.text = myDevice.listName
        self.listName.text = myDevice.listName
        if myDevice.category == "" {
            self.catHolder.isHidden = true
        } else {
            self.catHolder.isHidden = false
            self.category.text = myDevice.category
        }
        if myDevice.subCategory == "" {
            self.subcatHolder.isHidden = true
        } else {
            self.subcatHolder.isHidden = false
            self.subcategory.text = myDevice.subCategory
        }
        DownloadAndReadImage().renderLocalImage(imageName: myDevice.brandLogoName, imageView: self.brandImage, folderName: Values.amazonBrandsFolder)
        if !myDevice.vidBrandVerified {
            self.verifiedIcon.isHidden = true
        } else {
            self.verifiedIcon.isHidden = false
        }
        self.deviceNickname.text = myDevice.deviceTag
        self.nickname.text = myDevice.deviceTag
        var s = ""
        if myDevice.category == "" {
            s = myDevice.brandName
        } else {
            s = myDevice.category
        }
        if myDevice.modelNo != "" {
            s += " | "+myDevice.modelNo
        }
        self.deviceModel.text = s
        
        if myDevice.dateOfPurchase != "" {
            self.purchaseDate.text = OriUtil.getDateStringFromISO(myDevice.dateOfPurchase)
        }
        
        if myDevice.createdAt != "" {
            self.dateAdded.text = OriUtil.getDateStringFromISO(myDevice.createdAt)
        }
        self.warrantyText.text = myDevice.warrantyStatus
        self.modelNo.text = myDevice.modelNo
        self.serialNo.text = myDevice.serialNumber
        self.retailer.text = myDevice.retailerName
        
        //setting images
        OriUtil.oriLog("images",myDevice.deviceImage,myDevice.invoiceFile,myDevice.warrantyFile)
        imageHelper.renderLocalImage(imageName: myDevice.deviceImage, imageView: self.devicePhoto, folderName: "")
        imageHelper.renderLocalImage(imageName: myDevice.invoiceFile, imageView: self.invoice, folderName: "")
        imageHelper.renderLocalImage(imageName: myDevice.warrantyFile, imageView: self.warranty, folderName: "")
    }

    @IBOutlet weak var moreButton: UIButton!
    @IBAction func moreButton(_ sender: Any) {
        if self.toggleDetails.isHidden {
            self.toggleDetails.isHidden = false
            self.moreButton.setTitle("Less", for: .normal)
        } else {
            self.toggleDetails.isHidden = true
            self.moreButton.setTitle("More", for: .normal)
        }
    }
    
    @IBOutlet weak var reqService: UIBarButtonItem!
    @IBAction func reqService(_ sender: Any) {
        let pickerController = UIAlertController()
        pickerController.title = "Request Service:"
        
        let repairAction = UIAlertAction(title:"Repair", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            self.leavePage(animated:false, goTo: .repair, myDevice: self.myDevice)
        }
        var repair = UIImage(named: "repair")
        repair = repair?.scaled(to: CGSize(width: 35, height: 35))
        repairAction.setValue(repair, forKey: "image")
        
        let maintAction = UIAlertAction(title:"Maintenance", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            self.leavePage(animated:false, goTo: .maintenance, myDevice: self.myDevice)
        }
        var image = UIImage(named: "maintenance")
        image = image?.scaled(to: CGSize(width: 35, height: 35))
        maintAction.setValue(image, forKey: "image")
        
        let installAction = UIAlertAction(title:"Installation", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            self.leavePage(animated:false, goTo: .installation, myDevice: self.myDevice)
        }
        var image2 = UIImage(named: "installation")
        image2 = image2?.scaled(to: CGSize(width: 35, height: 35))
        installAction.setValue(image2, forKey: "image")
        
        let cancel = UIAlertAction(title:"Close", style: UIAlertActionStyle.cancel) { action in
            pickerController.dismiss(animated: true, completion: nil)
        }
        
        pickerController.addAction(repairAction)
        pickerController.addAction(maintAction)
        pickerController.addAction(installAction)
        pickerController.addAction(cancel)
        
        pickerController.popoverPresentationController?.barButtonItem = self.reqService // works for both iPhone & iPad
        pickerController.view.tintColor = #colorLiteral(red: 0.4401171207, green: 0.4401280284, blue: 0.4401221871, alpha: 1)
        
        self.present(pickerController, animated: true, completion: nil)
    }
    
    func leavePage(animated:Bool = true, goTo:HomeController.toPage = .none, myDevice:MyDevice = MyDevice()) {
//        if goTo != .none {
//            if let navController = self.navigationController {
//                for controller in navController.viewControllers as Array {
//                    if controller.isKind(of: HomeController.self) {
//                        let homeVC = controller as! HomeController
//                        homeVC.goToPage = goTo
//                        homeVC.animatePages = animated
//                        homeVC.myDevice = myDevice
//                    }
//                }
//            }
//        }
//
////        for view in (navigationController?.navigationBar.subviews)!{ //remove the view
////            if view.tag == 13 {
////                view.removeFromSuperview()
////            }
////        }
//        self.navigationController?.popToRootViewController(animated: animated)
        
        if goTo == .repair {
            let storyboard = UIStoryboard(name: "ReqServiceStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "repairScreen") as! RepairVC
            vc.myDevice = self.myDevice
            vc.serviceType = goTo.rawValue
            self.show(vc, sender: self)
        } else {
            let storyboard = UIStoryboard(name: "ReqServiceStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "maintScreen") as! MaintenanceVC
            vc.myDevice = self.myDevice
            vc.serviceType = goTo.rawValue
            self.show(vc, sender: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @objc func chooseDeviceImage() {
        if myDevice.deviceImage != "" {
            self.openFile(imageName: myDevice.deviceImage)
        } else {
            showPicker(photoType: .devicePhoto)
        }
    }
    
    @objc func longPressDeviceImage(sender:UILongPressGestureRecognizer) {
        if sender.state == .began && myDevice.deviceImage != "" {
            showMenu(photoType: .devicePhoto)
        }
    }
    
    @objc func deleteDeviceImage() {
        self.devicePhoto.image = #imageLiteral(resourceName: "ic_add_device_image")
        
        let json:[String:Any] = [
            "_id": myDevice._id,
            "deviceImg": " "
        ]
        self.updateDevice(json: json, type: .devicePhoto, imageName: "")
    }
    
    @objc func replaceDeviceImage() {
        showPicker(photoType: .devicePhoto)
    }
    
    @objc func chooseWarranty() {
        if myDevice.warrantyFile != "" {
            self.openFile(imageName: myDevice.warrantyFile)
        } else {
            showPicker(photoType: .warranty)
        }
    }
    
    @objc func longPressWarranty(sender:UILongPressGestureRecognizer) {
        if sender.state == .began && myDevice.warrantyFile != "" {
            showMenu(photoType: .warranty)
        }
    }
    
    @objc func deleteWarranty() {
        self.warranty.image = #imageLiteral(resourceName: "ic_add_warranty")
        
        let json:[String:Any] = [
            "_id": myDevice._id,
            "warrantyFile": " "
        ]
        self.updateDevice(json: json, type: .warranty, imageName: "")
    }
    
    @objc func replaceWarranty() {
        showPicker(photoType: .warranty)
    }
    
    @objc func chooseInvoice() {
        if myDevice.invoiceFile != "" {
            self.openFile(imageName: myDevice.invoiceFile)
        } else {
            showPicker(photoType: .invoice)
        }
    }
    
    @objc func longPressInvoice(sender:UILongPressGestureRecognizer) {
        if sender.state == .began && myDevice.invoiceFile != "" {
            showMenu(photoType: .invoice)
        }
    }
    
    @objc func deleteInvoice() {
        self.invoice.image = #imageLiteral(resourceName: "ic_add_invoice")
        
        let json:[String:Any] = [
            "_id": myDevice._id,
            "invoiceFile": " "
        ]
        self.updateDevice(json: json, type: .invoice, imageName: "")
    }
    
    @objc func replaceInvoice() {
        showPicker(photoType: .invoice)
    }
    
    func showMenu(photoType:PhotoType) {
        OriUtil.oriLog("long pressed ",photoType)
        becomeFirstResponder()
        let menu = UIMenuController.shared
        var item, item2: UIMenuItem
        if photoType == .devicePhoto {
            item = UIMenuItem(title: "Replace", action: #selector(AddDevice2VC.replaceDeviceImage))
            item2 = UIMenuItem(title: "Delete", action: #selector(AddDevice2VC.deleteDeviceImage))
            menu.menuItems = [item, item2]
            let bounds = devicePhoto.bounds
            let rect = CGRect(x: bounds.midX, y:  bounds.midY/2.0, width: 0, height: 0)
            menu.setTargetRect(rect, in: addDevicePhotoHolder)
        } else if photoType == .invoice {
            item = UIMenuItem(title: "Replace", action: #selector(AddDevice2VC.replaceInvoice))
            item2 = UIMenuItem(title: "Delete", action: #selector(AddDevice2VC.deleteInvoice))
            menu.menuItems = [item, item2]
            let bounds = invoiceHolder.bounds
            let rect = CGRect(x: bounds.midX, y:  bounds.midY/2.0, width: 0, height: 0)
            
            menu.setTargetRect(rect, in: invoiceHolder)
        } else if photoType == .warranty {
            item = UIMenuItem(title: "Replace", action: #selector(AddDevice2VC.replaceWarranty))
            item2 = UIMenuItem(title: "Delete", action: #selector(AddDevice2VC.deleteWarranty))
            menu.menuItems = [item, item2]
            let bounds = warrantyHolder.bounds
            let rect = CGRect(x: bounds.midX, y:  bounds.midY/2.0, width: 0, height: 0)
            menu.setTargetRect(rect, in: warrantyHolder)
        }
        
        menu.arrowDirection = .down
        
        menu.setMenuVisible(true, animated: true)
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    func showPicker(photoType:PhotoType) {
        let pickerController = UIAlertController()
        
        let photoAction = UIAlertAction(title:"Take Photo", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let cameraPicker = UIImagePickerController()
                cameraPicker.sourceType = UIImagePickerControllerSourceType.camera
                cameraPicker.cameraCaptureMode = .photo
                cameraPicker.allowsEditing=false
                cameraPicker.delegate=self
                cameraPicker.view.tag = photoType.rawValue
                self.present(cameraPicker, animated: true, completion: nil)
            } else {
                OriUtil.showToast(view: self.view, msg: "Camera not available")
            }
        }
        var repair = #imageLiteral(resourceName: "take_photo")
        repair = repair.scaled(to: CGSize(width: 35, height: 35))
        photoAction.setValue(repair, forKey: "image")
        
        let galleryAction = UIAlertAction(title:"Upload from Gallery", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            imagePicker.view.tag = photoType.rawValue
            self.present(imagePicker, animated: true, completion: nil)
        }
        var image = #imageLiteral(resourceName: "choose_from_gallery")
        image = image.scaled(to: CGSize(width: 35, height: 35))
        galleryAction.setValue(image, forKey: "image")
        
        let pdfAction = UIAlertAction(title:"Upload a PDF", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            importMenu.allowsMultipleSelection = false
            importMenu.delegate = self
            importMenu.view.tag = photoType.rawValue
            importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion: nil)
        }
        var image2 = #imageLiteral(resourceName: "choose_pdf")
        image2 = image2.scaled(to: CGSize(width: 35, height: 35))
        pdfAction.setValue(image2, forKey: "image")
        
        let cancel = UIAlertAction(title:"Close", style: UIAlertActionStyle.cancel) { action in
            pickerController.dismiss(animated: true, completion: nil)
        }
        
        pickerController.addAction(photoAction)
        pickerController.addAction(galleryAction)
        if photoType != .devicePhoto { // no PDF for device photo
            pickerController.addAction(pdfAction)
        }
        pickerController.addAction(cancel)
        
        if photoType == .devicePhoto {
            pickerController.title = "Upload Device Image:"
            pickerController.popoverPresentationController?.sourceView = self.addDevicePhotoHolder // works for both iPhone & iPad
        } else if photoType == .invoice {
            pickerController.title = "Upload Invoice:"
            pickerController.popoverPresentationController?.sourceView = self.invoiceHolder // works for both iPhone & iPad
        } else if photoType == .warranty {
            pickerController.title = "Upload Warranty:"
            pickerController.popoverPresentationController?.sourceView = self.warrantyHolder // works for both iPhone & iPad
        }

        pickerController.view.tintColor = #colorLiteral(red: 0.4401171207, green: 0.4401280284, blue: 0.4401221871, alpha: 1)
        
        self.present(pickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true, completion: nil)
        
        if picker.view.tag == PhotoType.devicePhoto.rawValue {
            OriUtil.oriLog("Picked device image")
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                let fileName = getImageName(type: .devicePhoto)
                imageHelper.saveImageToLocalStorage(image: image, fileNameWithExtension: fileName)
                let json:[String:Any] = [
                    "_id": myDevice._id,
                    "deviceImg": fileName
                ]
                self.updateDevice(json: json, type: .devicePhoto, imageName: fileName)
            }
        } else if picker.view.tag == PhotoType.warranty.rawValue {
            OriUtil.oriLog("Picked warranty image")
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                let fileName = getImageName(type: .warranty)
                imageHelper.saveImageToLocalStorage(image: image, fileNameWithExtension: fileName)
                let json:[String:Any] = [
                    "_id": myDevice._id,
                    "warrantyFile": fileName
                ]
                self.updateDevice(json: json, type: .warranty, imageName: fileName)
            }
        } else if picker.view.tag == PhotoType.invoice.rawValue {
            OriUtil.oriLog("Picked invoice image")
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                let fileName = getImageName(type: .invoice)
                imageHelper.saveImageToLocalStorage(image: image, fileNameWithExtension: fileName)
                let json:[String:Any] = [
                    "_id": myDevice._id,
                    "invoiceFile": fileName
                ]
                self.updateDevice(json: json, type: .invoice, imageName: fileName)
            }
        }
    }
    
    
    func getImageName(type:PhotoType, ext:String = ".png") -> String {
        if type == .devicePhoto {
            return "ori_devimg_"+OriUtil.getDateTimeStamp()+ext
        } else if type == .invoice {
            return "ori_billimg_"+OriUtil.getDateTimeStamp()+ext
        } else if type == .warranty {
            return "ori_warrimg_"+OriUtil.getDateTimeStamp()+ext
        } else {
            OriUtil.oriLog("invalid photo type")
            return ""
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let url = urls[0] as URL
//        OriUtil.oriLog("The Url is : \(url)")
//        OriUtil.oriLog("last component of url" , url.lastPathComponent)
        
        if controller.view.tag == PhotoType.invoice.rawValue {
            let fileName = getImageName(type: .invoice, ext: ".pdf")
            imageHelper.copyUrlToLocal(url: url, fileName: fileName)
            let json:[String:Any] = [
                "_id": myDevice._id,
                "invoiceFile": fileName
            ]
            self.updateDevice(json: json, type: .invoice, imageName: fileName)
        } else if controller.view.tag == PhotoType.warranty.rawValue {
            let fileName = getImageName(type: .warranty, ext: ".pdf")
            imageHelper.copyUrlToLocal(url: url, fileName: fileName)
            let json:[String:Any] = [
                "_id": myDevice._id,
                "warrantyFile": fileName
            ]
            self.updateDevice(json: json, type: .warranty, imageName: fileName)
        }
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func openFile(imageName:String) {
        let x = imageHelper.fileExistsInLocal(imageName: imageName)
        if x.0 {
            let documentInteractionController = UIDocumentInteractionController(url: x.1!)
            documentInteractionController.delegate = self
            documentInteractionController.presentPreview(animated: true)
        }
    }
    
    func updateDevice(json:[String:Any], type:PhotoType, imageName:String) {
        self.view.makeToastActivity(.center)
        
        //call API
        OriNetworking.makeRequest(requestType: .post, url: ApiUrls.postUpdateDeviceUrl, parameters: json, view: self.view) {(status, error, response) in
            self.view.hideToastActivity()
            if status {
                if response["status"].bool! {
                    if type == .devicePhoto {
                        if self.myDevice.deviceImage != "" || self.myDevice.deviceImage != "NOIMG" {
                            self.imageHelper.deleteAWSFile(fileName: self.myDevice.deviceImage)
                            self.imageHelper.deleteLocalFile(fileName: self.myDevice.deviceImage)
                        }
                        self.myDevice.deviceImage = imageName
                        if self.imageHelper.setImageIfExists(imageView: self.devicePhoto, imageName: imageName) {
                            self.imageHelper.uploadFileToAWS(imageName: imageName)
                        }
                    } else if type == .invoice {
                        if self.myDevice.invoiceFile != "" || self.myDevice.invoiceFile != "NOIMG" {
                            self.imageHelper.deleteAWSFile(fileName: self.myDevice.invoiceFile)
                            self.imageHelper.deleteLocalFile(fileName: self.myDevice.invoiceFile)
                        }
                        self.myDevice.invoiceFile = imageName
                        if self.imageHelper.setImageIfExists(imageView: self.invoice, imageName: imageName) {
                            self.imageHelper.uploadFileToAWS(imageName: imageName)
                        }
                    } else if type == .warranty {
                        if self.myDevice.warrantyFile != "" || self.myDevice.warrantyFile != "NOIMG" {
                            self.imageHelper.deleteAWSFile(fileName: self.myDevice.warrantyFile)
                            self.imageHelper.deleteLocalFile(fileName: self.myDevice.warrantyFile)
                        }
                        self.myDevice.warrantyFile = imageName
                        if self.imageHelper.setImageIfExists(imageView: self.warranty, imageName: imageName) {
                            self.imageHelper.uploadFileToAWS(imageName: imageName)
                        }
                    }
                    
                    if self.delegate != nil {
                        self.delegate?.getReturnValue(value: ArrayModel(type: "myDevice", data: self.myDevice))
                    }
                    
                } else {
                    OriUtil.showToast(view: self.view, msg: "Could not update device, please try again later")
                }
            }
        }
    }

}
