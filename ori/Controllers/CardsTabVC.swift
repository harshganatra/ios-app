//
//  CardsTabVC.swift
//  ori
//
//  Created by Harsh Ganatra on 20/12/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON

class CardsTabVC: AnimatedTabBarController, UITabBarControllerDelegate, ReturnProtocol {

    var ongoingVC,completedVC:MyCardsVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.selectedIndex = 0
        
        if let vcList = self.viewControllers {
            ongoingVC = vcList[0] as! MyCardsVC
            ongoingVC.currentPage = .ongoing
            ongoingVC.delegate = self
            
            completedVC = vcList[1] as! MyCardsVC
            completedVC.currentPage = .completed
            completedVC.delegate = self
            let _ = completedVC.view //to load the second view together with the default first view
        } else {
            OriUtil.oriLog("vc List is nil")
        }
        if LoginHelper.isLoggedIn() {
            self.refreshData()
        } else { //update empty data in both controllers
            self.ongoingVC.updateData([])
            self.completedVC.updateData([])
        }
    }

    func refreshData() {
        self.view.makeToastActivity(.center)

        //call API
        OriNetworking.makeRequest(requestType: .get, url: ApiUrls.getAllCardsByUserIdUrl, view: self.view) {(status, error, response) in
            var myCardsCurrentArray:[MyCard] = []
            var myCardsHistoryArray:[MyCard] = []
            var myCardsCancelledArray:[MyCard] = []
            
            if status {
                if response["status"].bool! {
                    let data = response["data"]
                    
                    
                    for (_,subJson):(String, JSON) in data {
                        let myCard = OriUtil.parseSingleCardJSON(json: subJson)
                        
                        if (!(myCard.status.lowercased() == "finished") &&
                            !(myCard.status.lowercased() == "cancelled")
                            && !(myCard.status.lowercased() == "completed")) {
                            myCardsCurrentArray.append(myCard)
                        } else if myCard.status.lowercased() == "cancelled" {
                            myCardsCancelledArray.append(myCard)
                        } else {
                            myCardsHistoryArray.append(myCard)
                        }
                    } // for loop ends here
                    
                } 
            }
            self.view.hideToastActivity()
            myCardsHistoryArray.append(contentsOf: myCardsCancelledArray)
            
            self.ongoingVC.updateData(myCardsCurrentArray)
            
            self.completedVC.updateData(myCardsHistoryArray)
        }
    }
    
    func getReturnValue(value: ArrayModel) {
        if value.type == "removeCard" {
            self.refreshData()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
