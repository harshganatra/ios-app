//
//  HelpVC.swift
//  ori
//
//  Created by Harsh Ganatra on 20/02/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import MessageUI

class HelpVC: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var versionText: UILabel!
    @IBOutlet weak var refundTint: UIImageView!
    @IBOutlet weak var facebook: UIImageView!
    @IBOutlet weak var twitter: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        refundTint.tintColor = #colorLiteral(red: 1, green: 0, blue: 0.3037448227, alpha: 1)
        
        let versionNumberString =
            Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")
                as! String
        versionText.text = "Version: "+versionNumberString
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(fbTapped(tapGestureRecognizer:)))
        facebook.isUserInteractionEnabled = true
        facebook.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(twitterTapped(tapGestureRecognizer:)))
        twitter.isUserInteractionEnabled = true
        twitter.addGestureRecognizer(tapGestureRecognizer2)
    }
    
    @objc func fbTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let fbAppLink = URL(string:"fb://page/1017749198316613")
        let fbURL = URL(string:"https://www.facebook.com/oriserve")
        
        if (!OriUtil.openApp(url: fbAppLink!)) {
            if (OriUtil.openApp(url: fbURL!)) {
                OriUtil.oriLog("Opened fb link in browser")
            }
        }
    }
    
    @objc func twitterTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let twAppLink = URL(string:"twitter://user?user_id=731786395400994817")
        let twURL = URL(string:"https://www.twitter.com/oriserve")
        
        if (!OriUtil.openApp(url: twAppLink!)) {
            if (OriUtil.openApp(url: twURL!)) {
                OriUtil.oriLog("Opened twitter link in browser")
            }
        }
    }

    @IBAction func mailButton(_ sender: Any) {
        var body = "....\n\n\n\n\n--------------------------\nFill any queries above this message. Please do not edit any of the details below.\n"
        let login = LoginHelper.getCurrentUser()
        if login.0 {
            body = body+"\nUser Details:\nName: "+login.1.name+"\nPhone number: "+login.1.phoneNumber
        } else {
            body = body+"\nUser not logged in"
        }
        let versionNumberString =
            Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")
                as! String
        let systemVersion = UIDevice.current.systemVersion
        let model = UIDevice.current.model
        body += "\nApp Version: "+versionNumberString+"\nOS Version: "+systemVersion
        body += "\nDevice type: "+model
        
//        var urlComps = URLComponents(string:"mailto:support@oriserve.com")
//        let subjectQuery = URLQueryItem(name: "subject", value: "Ori Customer Help and Support")
//        let bodyQuery = URLQueryItem(name: "body", value: body)
//
//        urlComps?.queryItems = [subjectQuery, bodyQuery]
//
//        if !(OriUtil.openApp(url: (urlComps?.url)!)) {
//            OriUtil.showToast(view: self.view, msg: "Could not open mail application")
//        }
        var picker:MFMailComposeViewController?
        picker = MFMailComposeViewController()
        picker?.mailComposeDelegate = self
        picker?.setSubject("Ori Customer Help and Support")
        picker?.setMessageBody(body, isHTML: false)
        picker?.setToRecipients(["support@oriserve.com"])
        if picker != nil {
            self.present(picker!, animated: true, completion: nil)
        } else {
            OriUtil.oriLog("Cannot send mail")
        }
    }
    
    @IBAction func callButton(_ sender: Any) {
        let phone = "+918080668066"
        let msg = "Call our help-line number "+phone+" for any assistance. \n(Available from 9am-9pm)"
        let alertVC = UIAlertController(title: "Contact Ori?", message: msg, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        alertVC.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
            OriUtil.callNumber(number: phone)
        }))
        alertVC.presentInOwnWindow(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
