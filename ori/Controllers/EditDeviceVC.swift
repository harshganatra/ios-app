//
//  EditDeviceVC.swift
//  ori
//
//  Created by Harsh Ganatra on 08/03/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON

class EditDeviceVC: UIViewController, ReturnProtocol {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var serialNoText: UITextField!
    @IBOutlet weak var modelLoadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var modelPickerHolder: UIView!
    @IBOutlet weak var modelPickerButton: UIButton!
    @IBOutlet weak var modelPickerText: UITextField!
    @IBOutlet weak var modelTextHolder: UIView!
    @IBOutlet weak var modelTextView: UITextField!
    @IBOutlet weak var nickNameText: UITextField!
    @IBOutlet weak var retailerNameText: UITextField!
    @IBOutlet weak var datePickerText: UITextField!
    @IBOutlet weak var warrantyText: UITextField!
    
    weak var delegate: ReturnProtocol?
    var myDevice = MyDevice()

    let textDelegate = TextFieldDelegate()
    let modelKey = "model"
    let other = "Other..."
    var modelList:[ArrayModel] = []
    let datePicker = UIDatePicker()
    let numberDelegate = NumericFieldDelegate(digits: 2)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.modelTextView.delegate = textDelegate
        self.modelTextView.tag = 0
        self.serialNoText.delegate = textDelegate
        self.serialNoText.tag = 2
        
        nickNameText.delegate = textDelegate
        nickNameText.tag = 4
        retailerNameText.delegate = textDelegate
        retailerNameText.tag = 6
        
        let button = UIBarButtonItem(title: "Don't Know", style: UIBarButtonItemStyle.done, target: self, action: #selector(clearPicker))
        button.tintColor = #colorLiteral(red: 0.4401171207, green: 0.4401280284, blue: 0.4401221871, alpha: 1)
        let button3 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closePicker))
        button3.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        let numberToolbar: UIToolbar = UIToolbar()
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items=[
            button,
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button3
        ]
        numberToolbar.sizeToFit()
        self.datePickerText.inputAccessoryView = numberToolbar //do it for every relevant textfield if there are more than one
        datePicker.setDate(Date(), animated: false)
        datePicker.timeZone = TimeZone.init(abbreviation: "IST")
        datePicker.maximumDate = Date()
        datePicker.minimumDate = Date(timeIntervalSince1970: 0)
        //        datePicker.backgroundColor = #colorLiteral(red: 0.9844431281, green: 0.9844661355, blue: 0.9844536185, alpha: 1)
        datePicker.datePickerMode = UIDatePickerMode.date
        datePickerText.inputView = datePicker
        
        let numberToolbar2: UIToolbar = UIToolbar()
        numberToolbar2.barStyle = UIBarStyle.default
        let button2 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closeWarrantyText))
        button2.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        numberToolbar2.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button2
        ]
        numberToolbar2.sizeToFit()
        self.warrantyText.inputAccessoryView = numberToolbar2 //do it for every relevant textfield if there are more than one
        self.warrantyText.delegate = numberDelegate
        
        serialNoText.text = myDevice.serialNumber
        nickNameText.text = myDevice.deviceTag
        retailerNameText.text = myDevice.retailerName
        
        if myDevice.dateOfPurchase != "" {
            let date = OriUtil.dateFromISO(myDevice.dateOfPurchase)
            datePicker.setDate(date, animated: false)
            datePickerText.text = OriUtil.dateToString(date: datePicker.date)
        }
        
        self.warrantyText.text = myDevice.warrantyPeriod
        
        self.getModelData()
    }

    @IBAction func saveButton(_ sender: Any) {
        self.view.makeToastActivity(.center)
        self.addDetails()
        let myDevice = self.myDevice
        var json:[String:Any]  = [
            "_id": myDevice._id,
            "deviceTag": myDevice.deviceTag,
            "retailer": myDevice.retailerName,
            "model": myDevice.modelNo,
            "model_code": myDevice.modelCode,
            "serialNumber": myDevice.serialNumber
        ]
        if !(warrantyText.text == "") {
            json["warrantyPeriod"] = Int(warrantyText.text!)
        } else {
            json["warrantyPeriod"] = 0
        }
        json["dateOfPurchase"] = myDevice.dateOfPurchase
        
        
        //call API
        OriNetworking.makeRequest(requestType: .post, url: ApiUrls.postUpdateDeviceUrl, parameters: json, view: self.view) {(status, error, response) in
            self.view.hideToastActivity()
            if status {
                if response["status"].bool! {
                    if self.delegate != nil {
                        self.delegate?.getReturnValue(value: ArrayModel(type: "myDevice", data: self.myDevice))
                    }
                    self.navigationController?.popViewController(animated: true)
                } else {
                    OriUtil.showToast(view: self.view, msg: "Could not update device, kindly try again later")
                }
            }
        }
    }
    
    func addDetails() {
        if modelTextHolder.isHidden == false {
            myDevice.modelNo = modelTextView.text!
            myDevice.modelCode = ""
        }
        myDevice.serialNumber = serialNoText.text!
        myDevice.deviceTag = self.nickNameText.text!
        myDevice.retailerName = self.retailerNameText.text!
        if datePickerText.text != "" {
            myDevice.dateOfPurchase = OriUtil.getISO8601StringForDate(self.datePicker.date)
        } else {
            myDevice.dateOfPurchase = ""
        }
        myDevice.warrantyPeriod = self.warrantyText.text!
    }
    
    @IBAction func skipButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func clearPicker () {
        self.datePickerText.resignFirstResponder()
        self.datePickerText.text = ""
    }
    
    @objc func closePicker () {
        self.datePickerText.resignFirstResponder()
        self.datePickerText.text = OriUtil.dateToString(date: self.datePicker.date)
        //        OriUtil.oriLog("date got is ", self.datePicker.date.debugDescription)
    }
    
    @objc func closeWarrantyText () {
        self.warrantyText.resignFirstResponder()
    }
    
    
    @IBAction func modelPicker(_ sender: Any) {
        let story = UIStoryboard(name: "PickerVC", bundle: nil)
        let pickerVC = story.instantiateViewController(withIdentifier: "pickerVC") as! PickerVC
        pickerVC.delegate = self
        pickerVC.key = self.modelKey
        pickerVC.totalArray = modelList
        pickerVC.totalSearchResults = modelList
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    func getModelData() {
        modelLoadIndicator.startAnimating()
        self.modelTextView.text = myDevice.modelNo
        self.modelTextView.isUserInteractionEnabled = false
        
        let json:[String:Any]  = [
            "brandId": myDevice.brandId,
            "categorySlug": myDevice.categorySlug,
            "category": myDevice.category,
            "subCategory": myDevice.subCategory
        ]
        
        //call API
        OriNetworking.makeRequest(requestType: .post, url: ApiUrls.listModels, parameters: json, auth: .default_auth, view: self.view) {(status, error, response) in
            self.modelLoadIndicator.stopAnimating()
            if status {
                if response["status"].bool! {
                    let data = response["data"]
                    self.modelList = []
                    var foundModel:Bool = false
                    for (_,subJson):(String, JSON) in data {
                        let modelNo = subJson["model"].stringValue
                        if !foundModel && modelNo == self.myDevice.modelNo {
                            self.myDevice.modelCode = subJson["model_code"].stringValue
                            foundModel = true
                        }
                        let model  = ListWithCode(problemText: modelNo, problemCode: subJson["model_code"].stringValue)
                        self.modelList.append(ArrayModel(type: self.modelKey, data: model))
                    } // for loop ends here
                    if self.modelList.count > 0 {
                        self.modelPickerHolder.isHidden = false
                        
                        if foundModel { //model selected from list
                            self.modelPickerText.text = self.myDevice.modelNo
                            self.modelTextHolder.isHidden = true
                        } else {
                            self.modelPickerText.text = self.other
                            self.modelTextHolder.isHidden = false
                            self.modelTextView.text = self.myDevice.modelNo
                        }
                    } else { // picker is hidden, just fill the text view
                        self.modelTextView.text = self.myDevice.modelNo
                    }
                    let model  = ListWithCode(problemText: self.other, problemCode: "")
                    self.modelList.append(ArrayModel(type: self.modelKey, data: model))
                }
            }
            self.modelTextView.isUserInteractionEnabled = true
        }
    }
    
    func getReturnValue(value: ArrayModel) {
        if value.type == modelKey {
            let str = value.data as! ListWithCode
            if str.problemText != modelPickerText.text {
                self.modelPickerText.text = str.problemText
                myDevice.modelNo = str.problemText
                myDevice.modelCode = str.problemCode
                
                if str.problemText == other {
                    self.modelTextHolder.isHidden = false
                } else {
                    self.modelTextHolder.isHidden = true
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.unsubscribeToKeyboardNotifications()
    }
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(_ :)),
                                               name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: .UIKeyboardWillHide, object: nil)
    }
    func unsubscribeToKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyBoardWillShow(_ notification:Notification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = getKeyboardHeight(notification)
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(_ notification:Notification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }

}
