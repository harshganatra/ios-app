//
//  FeedbackVC.swift
//  ori
//
//  Created by Harsh Ganatra on 23/02/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var suggestion: UIButton!
    @IBOutlet weak var problem: UIButton!
    @IBOutlet weak var question: UIButton!
    @IBOutlet weak var praise: UIButton!
    @IBOutlet weak var textView: UITextView!
    
    var type = "Suggestion"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.delegate = self

        OriUtil.setButton(selected: true, button: self.suggestion)
    
        let numberToolbar2: UIToolbar = UIToolbar()
        numberToolbar2.barStyle = UIBarStyle.default
        let button = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closeKeyboard))
        button.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        numberToolbar2.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button
        ]
        numberToolbar2.sizeToFit()
        self.textView.inputAccessoryView = numberToolbar2
    }
    
    @objc func closeKeyboard() {
        self.textView.resignFirstResponder()
    }
    
    @IBAction func submitFeedback(_ sender: Any) {
        if self.textView.text == placeHolderText {
            OriUtil.showToast(view: self.view, msg: "Please enter your feedback")
        } else {
            self.view.makeToastActivity(.center)
            
            let versionNumberString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
            let feedback = textView.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            
            let json:[String:Any]  = [
                "userId": LoginHelper.getCurrentUser().1.userID,
                "appVersion": versionNumberString,
                "createdAt": OriUtil.getISO8601StringForCurrentDate(),
                "platform": "iOS",
                "brand": "Apple",
                "type": self.type,
                "description": feedback!
            ]
            
            //call API
            OriNetworking.makeRequest(requestType: .put, url: ApiUrls.putFeedbackUrl, parameters: json, auth: .default_auth, view: self.view) {(status, error, response) in
                self.view.hideToastActivity()
                if status {
                    if response["status"].bool! {
                        
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        OriUtil.showToast(view: self.view, msg: "Something went wrong. Kindly retry")
                    }
                }
            }
        }
        
    }
    
    @IBAction func suggestion(_ sender: Any) {
        if self.type != "Suggestion" {
            self.type = "Suggestion"
            OriUtil.setButton(selected: true, button: self.suggestion)
            OriUtil.setButton(selected: false, button: self.question)
            OriUtil.setButton(selected: false, button: self.praise)
            OriUtil.setButton(selected: false, button: self.problem)
        }
    }
    
    @IBAction func problem(_ sender: Any) {
        if self.type != "Problem" {
            self.type = "Problem"
            OriUtil.setButton(selected: false, button: self.suggestion)
            OriUtil.setButton(selected: false, button: self.question)
            OriUtil.setButton(selected: false, button: self.praise)
            OriUtil.setButton(selected: true, button: self.problem)
        }
    }
    
    @IBAction func question(_ sender: Any) {
        if self.type != "Question" {
            self.type = "Question"
            OriUtil.setButton(selected: false, button: self.suggestion)
            OriUtil.setButton(selected: true, button: self.question)
            OriUtil.setButton(selected: false, button: self.praise)
            OriUtil.setButton(selected: false, button: self.problem)
        }
    }
    
    @IBAction func praise(_ sender: Any) {
        if self.type != "Praise" {
            self.type = "Praise"
            OriUtil.setButton(selected: false, button: self.suggestion)
            OriUtil.setButton(selected: false, button: self.question)
            OriUtil.setButton(selected: true, button: self.praise)
            OriUtil.setButton(selected: false, button: self.problem)
        }
    }
    
    var placeHolderText = "Eg. Love your bill saving feature"
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        self.textView.textColor = #colorLiteral(red: 0.4401171207, green: 0.4401280284, blue: 0.4401221871, alpha: 1)
        
        if(self.textView.text == placeHolderText) {
            self.textView.text = ""
        }
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == "") {
            self.textView.text = placeHolderText
            self.textView.textColor = UIColor.lightGray
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.textView.text = placeHolderText
        self.textView.textColor = UIColor.lightGray
        
        self.subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.unsubscribeToKeyboardNotifications()
    }
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(_ :)),name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),name: .UIKeyboardWillHide, object: nil)
    }
    func unsubscribeToKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyBoardWillShow(_ notification:Notification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        
        //        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        //        contentInset.bottom = getKeyboardHeight(notification)
        //        scrollView.contentInset = contentInset
        
        if !(self.textView.isHidden) {
            let scrollTo = self.textView!.frame.origin.y + self.textView!.frame.height
            self.scrollView.setContentOffset(CGPoint(x:0, y:scrollTo), animated: false)
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification){
        //        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        //        scrollView.contentInset = contentInset
        
        if !(self.textView.isHidden) {
            //            let scrollTo = self.problemText!.frame.origin.y + self.problemText!.frame.height
            self.scrollView.setContentOffset(CGPoint(x:0, y:0), animated: false)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
