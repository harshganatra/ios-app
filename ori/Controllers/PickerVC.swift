//
//  PickerVC.swift
//  ori
//
//  Created by Harsh Ganatra on 02/01/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit

// put key as "item" to make normal string picker
class PickerVC: UIViewController , UITableViewDataSource, UITableViewDelegate , UISearchBarDelegate{

    weak var delegate: ReturnProtocol?
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var popupView: UIView!
    var totalSearchResults:[ArrayModel] = []
    var totalArray:[ArrayModel] = []
    var key:String = ""
    let listNameKey = "listName"
    let brandKey = "brand"
    
    @IBAction func closeButton(_ sender: Any) {
        delegate?.getReturnValue(value: ArrayModel(type:"cancelled", data: ""))
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if totalArray.count <= 6 { //hiding searchbar when all options will be visible
            self.searchBar.isHidden = true
        }

        self.tableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        self.tableView.register(UINib(nibName:"PickerLayout",bundle:nil), forCellReuseIdentifier: "scrollContentCell")
        
        //readjust tableview height contraint to get wrap_content effect
        let heightOfTableViewConstraint = NSLayoutConstraint(item: self.tableView, attribute: .height, relatedBy: .equal, toItem: tableView, attribute: .height, multiplier: 0.0, constant: 500)
        heightOfTableViewConstraint.priority = UILayoutPriority.defaultLow
        self.popupView.addConstraint(heightOfTableViewConstraint)
        
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            heightOfTableView += 20 //for top and bottom insets
            // Edit heightOfTableViewConstraint's constant to update height of table view
            heightOfTableViewConstraint.constant = heightOfTableView
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.totalSearchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var item:ArrayModel
        item = totalSearchResults[indexPath.item]
        
        if self.key == listNameKey {
            let cat:OriCategory = item.data as! OriCategory
            let cell = tableView.dequeueReusableCell(withIdentifier: "scrollContentCell", for: indexPath) as! ScrollContentCell
            
            DownloadAndReadImage().renderLocalImage(imageName: cat.imageName, imageView: cell.brandImage, folderName: Values.amazonCategoriesFolder)
            
            cell.title.text = cat.listName
//            cell.subTitle.isHidden = true
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else if self.key == brandKey {
            let cat = item.data as! Brand
            let cell = tableView.dequeueReusableCell(withIdentifier: "scrollContentCell", for: indexPath) as! ScrollContentCell
            
            DownloadAndReadImage().renderLocalImage(imageName: cat.brandLogoName, imageView: cell.brandImage, folderName: Values.amazonBrandsFolder)
            
            cell.title.text = cat.brandName
            //            cell.subTitle.isHidden = true
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else if self.key == "item" {
            let str = item.data as! String
            let cell = tableView.dequeueReusableCell(withIdentifier: "scrollContentCell", for: indexPath) as! ScrollContentCell
            cell.brandImage.isHidden = true
            cell.title.text = str
            //            cell.subTitle.isHidden = true
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else if self.key == "model" {
            let str = item.data as! ListWithCode
            let cell = tableView.dequeueReusableCell(withIdentifier: "scrollContentCell", for: indexPath) as! ScrollContentCell
            cell.brandImage.isHidden = true
            cell.title.text = str.problemText
            //            cell.subTitle.isHidden = true
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        self.tableView.deselectRow(at: indexPath, animated: false)
        var item:ArrayModel
        item = totalSearchResults[indexPath.item]
        
        delegate?.getReturnValue(value: item)
        self.dismiss(animated: true, completion: nil)
//        if self.key == listNameKey {
//
//        } else if self.key == brandKey {
//            delegate?.getReturnValue(value: item)
//            self.dismiss(animated: true, completion: nil)
//        } else {
//            delegate?.getReturnValue(value: item)
//            self.dismiss(animated: true, completion: nil)
//        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if self.totalArray.count == 0 {
            return
        }
        if searchText == "" {
            totalSearchResults = totalArray
            return
        }
        
        let st = searchText.lowercased()
        self.totalSearchResults = []

        if self.key == listNameKey {
            for mod in totalArray {
                let a = mod.data as! OriCategory
                if a.listName.lowercased().contains(st) ||
                    a.searchArray.lowercased().contains(st) {
                    self.totalSearchResults.append(mod)
                }
            }
        } else if self.key == brandKey {
            for mod in totalArray {
                let a = mod.data as! Brand
                if a.brandName.lowercased().contains(st) {
                    self.totalSearchResults.append(mod)
                }
            }
        } else if self.key == "item" {
            for mod in totalArray {
                let a = mod.data as! String
                if a.lowercased().contains(st) {
                    self.totalSearchResults.append(mod)
                }
            }
        } else if self.key == "model" {
            for mod in totalArray {
                let a = mod.data as! ListWithCode
                if a.problemText.lowercased().contains(st) {
                    self.totalSearchResults.append(mod)
                }
            }
        }
        
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
