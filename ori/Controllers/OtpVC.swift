//
//  OtpVC.swift
//  ori
//
//  Created by Harsh Ganatra on 01/11/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import UIKit

class OtpVC: UIViewController , UITextFieldDelegate{
    
    var phone:String!
    var firstName:String!
    var lastName:String!
    var emailID:String!
    var gender:String!
    var dob:String!
    var count:Int = 60
    var timer:Timer?
    var toPage = ""
    
    @IBOutlet weak var otpField: UITextField!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.numberLabel.text = "+91 "+self.phone+" below"
        self.otpField.delegate = self
        let numberToolbar: UIToolbar = UIToolbar()
        numberToolbar.barStyle = UIBarStyle.default
        let button = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closeKeyboard))
        button.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        numberToolbar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button
        ]
        numberToolbar.sizeToFit()
        self.otpField.inputAccessoryView = numberToolbar //do it for every relevant textfield if there are more than one
        
        self.resendPressed()
    }
    
    @objc func closeKeyboard () {
        self.otpField.resignFirstResponder()
    }
    
    func resendPressed() {
        resendButton.isEnabled = false
        resendButton.alpha = 0.40
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(OtpVC.countdown), userInfo: nil, repeats: true)
        
        let json:[String:Any]  = [
            "phone": phone!,
            "key": OriUtil.returnSHA(data: phone,key: "secretkeyforotp"),
            "firstName": firstName
        ]
        
        //call API
        OriNetworking.makeRequest(requestType: .post, url: ApiUrls.postSendOtpUrl, parameters: json, auth: .no_auth, view: self.view) {(status, error, response) in
            if status {
                if response["status"].bool! {
                    //OriUtil.oriLog("Otp Success")
                } else {
                    OriUtil.showToast(view: self.view, msg: "Something  went wrong. Kindly retry")
                }
            }
        }
    }
    
    @objc func countdown() {
        if count > 0 {
            countdownLabel.text = String(count)
            count -= 1
        } else {
            countdownLabel.text = ""
            resendButton.isEnabled = true
            resendButton.alpha = 1.0
            timer?.invalidate()
            timer = nil
            count = 60
        }
    }
    
    @IBAction func loginButton(_ sender: Any) {
        self.closeKeyboard()
        
        if Validation.hasText(textField: otpField) {
            let otp = self.otpField.text!
            OriUtil.oriLog("\(phone!)\(otp)")
            let json:[String:Any]  = [
                "phone": phone!,
                "code": otp,
                "key": OriUtil.returnSHA(data: ("\(phone!)\(otp)"),key: "secretkeyforotp"),
                ]
            self.view.makeToastActivity(.center)
            //call API
            OriNetworking.makeRequest(requestType: .post, url: ApiUrls.postConfirmOTPUrl, parameters: json, auth: .no_auth, view: self.view) {(status, error, response) in
                if status {
                    if response["status"].bool! {
                        //OriUtil.oriLog("Otp Success")
                        var user:OriUser = OriUser()
                        user.email = self.emailID
                        user.phoneNumber = self.phone
                        user.firstName = self.firstName
                        user.lastName = self.lastName
                        user.gender = self.gender
                        user.ISOdateOfBirth = self.dob
                        
                        self.completeLogin(user: user)
                    } else {
                        OriUtil.showToast(view: self.view, msg: "Incorrect OTP")
                    }
                } else {
                    self.view.hideToastActivity()
                }
            }
        } else {
            OriUtil.showToast(view: self.view, msg: "Please enter the OTP")
        }
        
    }
    
    @IBAction func resendOtpButton(_ sender: Any) {
        self.resendPressed()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == otpField {
            if string.count == 0 { // to allow backspace key to work
                return true
            }
            var newText = textField.text! as NSString
            var isDigit: Bool = false
            newText = newText.replacingCharacters(in: range, with: string) as NSString
            if let _ = Int(string) { isDigit = true }
            return isDigit && newText.length <= 4
        }
        return true
    }
    
    func completeLogin(user:OriUser) {
        var user = user
        let json:[String:Any]  = [
            "firstName": user.firstName,
            "lastName": user.lastName,
            "email": user.email,
            "phone": user.phoneNumber,
            "verified": "true",
            "key": OriUtil.returnSHA(data: ("\(user.phoneNumber)\(user.firstName)"),key: "secretkeyforotp"),
            "platform": "iOS",
            "gcmRegistrationToken": ""
        ]
        
        //call API
        OriNetworking.makeRequest(requestType: .put, url: ApiUrls.putAddCustUrl, parameters: json, auth: .no_auth, view: self.view) {(status, error, response) in
            if status {
                if response["status"].bool! {
                    //OriUtil.oriLog("Otp Success")
                    user.userID = response["data"]["userId"].stringValue
                    let password = response["data"]["password"].stringValue
                    
                    
                    //TODO: Crashlyticcs update info
                    //TODO: Clevertap update info
                    
                    self.loginCall(password, user:user)
                } else {
                    OriUtil.checkErrorFromJSON(view: self.view, json: response)
                }
            } else {
                self.view.hideToastActivity()
            }
        }
    }
    
    func loginCall(_ password:String, user:OriUser) {
        var user = user
        let json:[String:Any]  = [
            "phone": user.phoneNumber,
            "password": password
        ]
        
        //call API
        OriNetworking.makeRequest(requestType: .post, url: ApiUrls.postLoginUrl, parameters: json, auth: .no_auth, view: self.view) {(status, error, response) in
            self.view.hideToastActivity()
            if status {
                if response["status"].bool! {
                    //OriUtil.oriLog("Otp Success")
                    user.authToken = response["data"]["token"].stringValue
                    
                    let defaults = UserDefaults.standard
                    defaults.set(user.authToken, forKey: Values.TOKEN_KEY)
                    defaults.set(user.userID, forKey: Values.USERID_KEY)
                    defaults.set(user.firstName, forKey: Values.FIRST_NAME_KEY)
                    defaults.set(user.lastName, forKey: Values.LAST_NAME_KEY)
                    defaults.set(user.email, forKey: Values.EMAIL_KEY)
                    defaults.set(user.phoneNumber, forKey: Values.MOBILE_KEY)
                    defaults.set(user.gender, forKey: Values.GENDER_KEY)
                    defaults.set(user.ISOdateOfBirth, forKey: Values.ISO_DOB_KEY)
                                        
                    if self.toPage == "addDevice" {
                        if let navController = self.navigationController {
                            for controller in navController.viewControllers as Array {
                                if controller.isKind(of: AddDevice2VC.self) {
                                    let addDevice2VC = controller as! AddDevice2VC
                                    navController.popToViewController(controller, animated: false)
                                    addDevice2VC.nextButton(self)
                                    break
                                }
                            }
                        }
                    } else {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    
                } else {
                    OriUtil.showToast(view: self.view, msg: "Something went wrong. Kindly retry.")
                }
            }
        }
    }
    
    func goToHome() {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as UIViewController
        let navigationController = self.navigationController
        navigationController?.setViewControllers([vc], animated: true)
        self.show(vc, sender: self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
