//
//  AddAddressVC.swift
//  ori
//
//  Created by Harsh Ganatra on 14/02/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController {

    weak var delegate: ReturnProtocol?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var address1: UITextField!
    @IBOutlet weak var address2: UITextField!
    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var pincodeText: UITextField!
    @IBOutlet weak var landmarkText: UITextField!
    @IBOutlet weak var home: UIButton!
    @IBOutlet weak var work: UIButton!
    @IBOutlet weak var other: UIButton!
    @IBOutlet weak var nicknameText: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    var nickname:String = ""
    
    let textDelegate = TextFieldDelegate()
    let pincodeDelegate = NumericFieldDelegate(digits: 6)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        address1.delegate = self.textDelegate
        address1.tag = 1
        address2.delegate = self.textDelegate
        address2.tag = 2
        cityText.delegate = self.textDelegate
        cityText.tag = 3
        pincodeText.delegate = pincodeDelegate
        pincodeText.tag = 4
        landmarkText.delegate = self.textDelegate
        landmarkText.tag = 6
        nicknameText.delegate = self.textDelegate
        nicknameText.tag = 10
        
        let numberToolbar: UIToolbar = UIToolbar()
        numberToolbar.barStyle = UIBarStyle.default
        let button = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closeKeyboard))
        button.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        numberToolbar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button
        ]
        numberToolbar.sizeToFit()
        
        self.pincodeText.inputAccessoryView = numberToolbar
        
        // Do any additional setup after loading the view.
    }
    
    @objc func closeKeyboard () {
        if let nextField = view?.viewWithTag(self.pincodeText.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            self.pincodeText.resignFirstResponder()
        }
    }
    
    @IBAction func homeButton(_ sender: Any) {
        if nickname  == "Home" {
            OriUtil.setButton(selected: false, button: self.home)
            self.nickname = ""
        } else {
            OriUtil.setButton(selected: true, button: self.home)
            OriUtil.setButton(selected: false, button: self.work)
            OriUtil.setButton(selected: false, button: self.other)
            self.nicknameText.isHidden = true
            self.nickname = "Home"
        }
    }
    
    @IBAction func workButton(_ sender: Any) {
        if nickname  == "Work" {
            OriUtil.setButton(selected: false, button: self.work)
            self.nickname = ""
        } else {
            OriUtil.setButton(selected: false, button: self.home)
            OriUtil.setButton(selected: true, button: self.work)
            OriUtil.setButton(selected: false, button: self.other)
            self.nicknameText.isHidden = true
            self.nickname = "Work"
        }
    }

    @IBAction func otherButton(_ sender: Any) {
        if nickname  == "Other" {
            OriUtil.setButton(selected: false, button: self.other)
            self.nickname = ""
            self.nicknameText.isHidden = true
        } else {
            OriUtil.setButton(selected: false, button: self.home)
            OriUtil.setButton(selected: false, button: self.work)
            OriUtil.setButton(selected: true, button: self.other)
            self.nicknameText.isHidden = false
            self.nickname = "Other"
        }
    }
    
    @IBAction func saveButton(_ sender: Any) {
        let add1 = address1.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let add2 = address2.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let city = cityText.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        if add1 == "" {
//            self.address1.becomeFirstResponder()
            OriUtil.showToast(view: self.view, msg: "Please specify a house number")
        } else if add2 == "" {
            OriUtil.showToast(view: self.view, msg: "Please specify a street name")
        } else if city == "" {
            OriUtil.showToast(view: self.view, msg: "Please specify your city")
        } else if pincodeText.text?.count != 6 {
            OriUtil.showToast(view: self.view, msg: "Enter a valid 6-digit pincode")
        } else {
            let landmark = landmarkText.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            var name:String? = ""
            if nickname == "Other" {
                name = nicknameText.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            } else {
                name = nickname
            }
            
            self.view.makeToastActivity(.center)
            self.saveButton.isEnabled = false
            
            let json:[String:Any]  = [
                "userId": LoginHelper.getCurrentUser().1.userID,
                "addressLine1": add1!,
                "addressLine2": add2!,
                "city": city!,
                "zipcode": pincodeText.text!,
                "landmark": landmark!,
                "nickname": name!,
                "state": "",
                "isDefault": false
            ]
            
            //call API
            OriNetworking.makeRequest(requestType: .put, url: ApiUrls.putAddAddressUrl, parameters: json, view: self.view) {(status, error, response) in
                self.view.hideToastActivity()
                if status {
                    if response["status"].bool! {
                        var address = Address()
                        address._id = response["data"].stringValue
                        address.addressLine1 = add1!
                        address.addressLine2 = add2!
                        address.city = city!
                        address.zipcode = self.pincodeText.text!
                        address.landmark = landmark!
                        address.nickName = name!
                        
                        self.delegate?.getReturnValue(value: ArrayModel(type: Values.address_key, data: address))
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        OriUtil.showToast(view: self.view, msg: "Something went wrong. Kindly retry")
                    }
                }
                self.saveButton.isEnabled = true
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        self.subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.unsubscribeToKeyboardNotifications()
    }
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(_ :)),
                                               name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: .UIKeyboardWillHide, object: nil)
    }
    func unsubscribeToKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyBoardWillShow(_ notification:Notification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = getKeyboardHeight(notification)
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(_ notification:Notification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }

}
