//
//  AddDeviceVC.swift
//  ori
//
//  Created by Harsh Ganatra on 28/12/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddDeviceVC: UIViewController , ReturnProtocol {
    
    let listNameKey = "listName"
    let brandKey = "brand"
    let catKey = "category"
    let subCatKey = "subCategory"
    let modelKey = "model"
    
    let other = "Other..."
    
    var myDevice:MyDevice = MyDevice()
    var cat:OriCategory?
    var brand:Brand?
    var catList:[ArrayModel] = []
    var subCatList:[ArrayModel] = []
    var modelList:[ArrayModel] = []
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var chooseListNameButton: UIButton!
    @IBOutlet weak var chooseListNameText: UITextField!
    @IBOutlet weak var chooseBrandButton: UIButton!
    @IBOutlet weak var chooseBrandText: UITextField!
    @IBOutlet weak var chooseCatHolder: UIStackView!
    @IBOutlet weak var chooseCatText: UITextField!
    @IBOutlet weak var chooseCatButton: UIButton!
    @IBOutlet weak var chooseSubCatHolder: UIStackView!
    @IBOutlet weak var chooseSubCatText: UITextField!
    @IBOutlet weak var chooseSubCatButton: UIButton!
    @IBOutlet weak var serialNoText: UITextField!
    @IBOutlet weak var modelLoadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var modelPickerHolder: UIView!
    @IBOutlet weak var modelPickerButton: UIButton!
    @IBOutlet weak var modelPickerText: UITextField!
    @IBOutlet weak var modelTextHolder: UIView!
    @IBOutlet weak var modelTextView: UITextField!
    
    let textDelegate = TextFieldDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.modelTextView.delegate = textDelegate
        self.modelTextView.tag = 0
        self.serialNoText.delegate = textDelegate
        self.serialNoText.tag = 1
    }
    
    
    @IBAction func chooseListName(_ sender: Any) {
        let story = UIStoryboard(name: "PickerVC", bundle: nil)
        let pickerVC = story.instantiateViewController(withIdentifier: "pickerVC") as! PickerVC
        pickerVC.delegate = self
        pickerVC.key = listNameKey
        let list = (try! CategoryDataHelper.findAll())!
        var modelList:[ArrayModel] = []
        for c in list {
            if c.category == "Appliances" {
                modelList.append(ArrayModel(type: listNameKey, data: c))
            }
        }
        pickerVC.totalArray = modelList
        pickerVC.totalSearchResults = modelList
        
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    @IBAction func chooseBrand(_ sender: Any) {
        if self.cat == nil {
            OriUtil.showToast(view: self.view, msg: "Choose Device first")
        } else {
            let story = UIStoryboard(name: "PickerVC", bundle: nil)
            let pickerVC = story.instantiateViewController(withIdentifier: "pickerVC") as! PickerVC
            pickerVC.delegate = self
            pickerVC.key = brandKey
            var modelList:[ArrayModel] = []
            let brandIds = JSON.init(parseJSON: (cat?.respectiveBrandIDs)!)
//            OriUtil.oriLog("brand ids are ",brandIds)
            var numbers:[String] = []
            
            for (_,id) in brandIds {
                numbers.append(String(describing: id))
            }

            let list = (try! BrandDataHelper.findAll())!
            for b in list {
                if (numbers.contains(String(b.brandId))) {
                    modelList.append(ArrayModel(type: brandKey, data: b))
                }
            }
            pickerVC.totalArray = modelList
            pickerVC.totalSearchResults = modelList
            self.present(pickerVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func category(_ sender: Any) {
        let story = UIStoryboard(name: "PickerVC", bundle: nil)
        let pickerVC = story.instantiateViewController(withIdentifier: "pickerVC") as! PickerVC
        pickerVC.delegate = self
        pickerVC.key = "item"
        pickerVC.totalArray = catList
        pickerVC.totalSearchResults = catList
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    @IBAction func subCategory(_ sender: Any) {
        let story = UIStoryboard(name: "PickerVC", bundle: nil)
        let pickerVC = story.instantiateViewController(withIdentifier: "pickerVC") as! PickerVC
        pickerVC.delegate = self
        pickerVC.key = "item"
        pickerVC.totalArray = subCatList
        pickerVC.totalSearchResults = subCatList
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    @IBAction func modelPicker(_ sender: Any) {
        let story = UIStoryboard(name: "PickerVC", bundle: nil)
        let pickerVC = story.instantiateViewController(withIdentifier: "pickerVC") as! PickerVC
        pickerVC.delegate = self
        pickerVC.key = self.modelKey
        pickerVC.totalArray = modelList
        pickerVC.totalSearchResults = modelList
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    @IBAction func nextButton(_ sender: Any) {
        if cat == nil {
            OriUtil.showToast(view: self.view, msg: "Please select Device")
        } else if brand == nil {
            OriUtil.showToast(view: self.view, msg: "Please select Brand")
        } else if chooseCatHolder.isHidden == false && myDevice.category == "" {
            OriUtil.showToast(view: self.view, msg: "Please select Device Type")
        } else if chooseSubCatHolder.isHidden == false && myDevice.subCategory == "" {
            OriUtil.showToast(view: self.view, msg: "Please select Device Variant")
        } else {
            if modelTextHolder.isHidden == false {
                myDevice.modelNo = modelTextView.text!
                myDevice.modelCode = ""
            }
            myDevice.serialNumber = serialNoText.text!
            
            self.performSegue(withIdentifier: "toAddDevice2", sender: nil)
        }
    }
    
    func getReturnValue(value: ArrayModel) {
        if value.type == listNameKey {
            let c:OriCategory = value.data as! OriCategory
            if c.listName != self.chooseListNameText.text { //different category selected
                self.cat = c
                self.chooseListNameText.text = c.listName
                self.chooseBrandText.text = ""
                self.brand = nil
                self.chooseCatText.text = ""
                self.chooseCatHolder.isHidden = true
                self.chooseSubCatText.text = ""
                self.chooseSubCatHolder.isHidden = true
                self.modelPickerHolder.isHidden = true
                self.modelTextHolder.isHidden = false
                let (loggedIn,user) = LoginHelper.getCurrentUser()
                if loggedIn {
                    myDevice.userId = user.userID
                }
                myDevice.categorySlug = (cat?.categorySlug)!
                myDevice.listName = (cat?.listName)!
                myDevice.defaultDeviceImage = (cat?.imageName)!
                myDevice.categorySearchArray = (cat?.searchArray)!
                myDevice.amcAvailable = ((cat?.amcAvailable) != nil)
                myDevice.applianceOrConnection = (cat?.category)!
            }
        } else if value.type == brandKey {
            let b = value.data as! Brand
            if b.brandName != self.chooseBrandText.text {
                self.brand = b
                self.chooseBrandText.text = b.brandName
                self.chooseCatText.text = ""
                self.chooseCatHolder.isHidden = true
                self.chooseSubCatText.text = ""
                self.chooseSubCatHolder.isHidden = true
                self.modelPickerHolder.isHidden = true
                self.modelTextHolder.isHidden = false
                myDevice.brandName = b.brandName
                myDevice.isOnboarded = b.isOnboarded
                myDevice.brandLogoName = b.brandLogoName
                myDevice.brandId = b.brandId
                myDevice.brandPhone = b.serviceNumber
                myDevice.brandSearchArray = b.androidBrandArray
                
                if b.hasCategory == "1" {
                    let slug = (cat?.categorySlug)!
                    let x = JSON.init(parseJSON: b.categoryJSON)
                    catList = []
                    for (_,subJson):(String,JSON) in x[slug] {
                        catList.append(ArrayModel.init(type: catKey, data: subJson["cat"].stringValue))
                    }
                    if catList.count > 0 {
                        chooseCatHolder.isHidden = false
                    } else { //could not retrieve category
                        self.getModelData()
                    }
                } else { //has no category
                    self.getModelData()
                }
            }
        } else if value.type == catKey {
            let str = value.data as! String
            if str != chooseCatText.text {
                self.chooseCatText.text = str
                myDevice.category = str
                self.chooseSubCatText.text = ""
                self.chooseSubCatHolder.isHidden = true
                self.modelPickerHolder.isHidden = true
                self.modelTextHolder.isHidden = false
                let x = JSON.init(parseJSON: (brand?.categoryJSON)!)
                subCatList = []
                for (_,subJson):(String,JSON) in x[myDevice.categorySlug] {
                    if subJson["cat"].stringValue == myDevice.category &&
                        subJson["subCategory"].exists() {
                        
                        for (_,subJson):(String,JSON) in subJson["subCategory"] {
                            subCatList.append(ArrayModel.init(type: subCatKey, data: subJson["subCat"].stringValue))
                        }
                        
                        break
                    }
                } //for loop ends here
                
                if subCatList.count > 0 {
                    self.chooseSubCatHolder.isHidden = false
                } else {
                    self.getModelData()
                }
            }
        } else if value.type == subCatKey {
            let str = value.data as! String
            if str != chooseSubCatText.text {
                self.chooseSubCatText.text = str
                myDevice.subCategory = str
                self.modelPickerHolder.isHidden = true
                self.modelTextHolder.isHidden = false
                self.getModelData()
            }
        } else if value.type == modelKey {
            let str = value.data as! ListWithCode
            if str.problemText != modelPickerText.text {
                self.modelPickerText.text = str.problemText
                myDevice.modelNo = str.problemText
                myDevice.modelCode = str.problemCode
                
                if str.problemText == other {
                    self.modelTextHolder.isHidden = false
                } else {
                    self.modelTextHolder.isHidden = true
                }
            }
        } else if value.type == "addDevice2" {
            let str = value.data as! MyDevice
            self.myDevice = str
        }
    }
    
    func getModelData() {
        modelLoadIndicator.startAnimating()
        self.modelTextView.isUserInteractionEnabled = false
        
        let json:[String:Any]  = [
            "brandId": myDevice.brandId,
            "categorySlug": myDevice.categorySlug,
            "category": myDevice.category,
            "subCategory": myDevice.subCategory
            ]
        
        //call API
        OriNetworking.makeRequest(requestType: .post, url: ApiUrls.listModels, parameters: json, auth: .default_auth, view: self.view) {(status, error, response) in
            self.modelLoadIndicator.stopAnimating()
            if status {
                if response["status"].bool! {
                    let data = response["data"]
                    self.modelList = []
                    for (_,subJson):(String, JSON) in data {
                        let model  = ListWithCode(problemText: subJson["model"].stringValue, problemCode: subJson["model_code"].stringValue)
                        self.modelList.append(ArrayModel(type: self.modelKey, data: model))
                    } // for loop ends here
                    if self.modelList.count > 0 {
                        self.modelTextHolder.isHidden = true
                        self.modelPickerHolder.isHidden = false
                    }
                    let model  = ListWithCode(problemText: self.other, problemCode: "")
                    self.modelList.append(ArrayModel(type: self.modelKey, data: model))
                }
            }
            self.modelTextView.isUserInteractionEnabled = true
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "toAddDevice2") {
            let vc = segue.destination as! AddDevice2VC
            vc.delegate = self
            vc.myDevice = self.myDevice
        }
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if let nextField = self.view.viewWithTag(textField.tag + 1) as? UITextField {
//            nextField.becomeFirstResponder()
//        } else {
//            textField.resignFirstResponder()
//            return true;
//        }
//        return false
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.unsubscribeToKeyboardNotifications()
    }
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(_ :)),
                                               name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: .UIKeyboardWillHide, object: nil)
    }
    func unsubscribeToKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyBoardWillShow(_ notification:Notification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = getKeyboardHeight(notification)
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(_ notification:Notification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }
}
