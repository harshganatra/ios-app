//
//  SelectDeviceVC.swift
//  ori
//
//  Created by Harsh Ganatra on 10/01/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON

class SelectDeviceVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDevicesHolder: UIView!
    @IBOutlet weak var noDevicesText: UILabel!
     var myDevicesArray: [MyDevice] = []
     var myConnectionsArray: [MyConnection] = []
     var totalArray: [ArrayModel] = []
     var totalSearchResults: [ArrayModel] = []
     var loaderCount = 0
     var searchController:UISearchController!
     var refreshControl:UIRefreshControl!
    var page:toSelect = .none
    var serviceType = ""

    public enum toSelect: String {
        case none     = ""
        case buyAMC    = "buyAMC"
        case reqService    = "reqService"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.barStyle = UIBarStyle.default
            controller.searchBar.barTintColor = UIColor.white
            //            controller.searchBar.backgroundColor = UIColor.clear
            controller.searchBar.placeholder = "Search for device.."
            self.tableView.tableHeaderView = controller.searchBar
            return controller
        })()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        tableView.register(UINib(nibName:"MyDevicesCardLayout",bundle:nil), forCellReuseIdentifier: "myDevicesCell")
        tableView.register(UINib(nibName:"MyConnectionsCardLayout",bundle:nil), forCellReuseIdentifier: "myConnectionsCell")
        self.tableView.tableHeaderView = searchController.searchBar
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.backgroundColor = #colorLiteral(red: 0.9844431281, green: 0.9844661355, blue: 0.9844536185, alpha: 1)
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        
        self.refreshData()
    }
    
    @IBAction func addDevice(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "AddDeviceStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddDeviceScreen") as UIViewController
        self.show(vc, sender: self)
    }
    
    @objc func refreshData(_ sender: Any! = nil) {
        if LoginHelper.isLoggedIn() {
            self.view.makeToastActivity(.center)
            loaderCount += 1; self.fetchDevices()
//            self.fetchConnections(); loaderCount += 1
        } else {
            self.updateData()
        }
        //        self.updateData()
    }
    
    func fetchDevices() {
        //call API
        OriNetworking.makeRequest(requestType: .get, url: ApiUrls.getAllDevicesByIdUrl, parameters: nil, auth: .send_auth, view: self.view) {(status, error, response) in
            if status {
                if response["status"].bool! {
                    let data = response["data"]
                    self.myDevicesArray = []
                    for (_,subJson):(String, JSON) in data {
                        let device = OriUtil.parseSingleDeviceJSON(json: subJson)
                        if self.page == .buyAMC {
                            if device.amcAvailable {
                                self.myDevicesArray.append(device)
                            }
                        } else if self.page == .reqService {
                            self.myDevicesArray.append(device)
                        }
                    } // for loop ends here
                    //                        OriUtil.oriLog("Got devices array here,length: "+String(self.myDevicesArray.count))
                }
            }
            
            self.loaderCount -= 1
            if self.loaderCount == 0 {
                self.updateData()
            }
        }
    }
    
    func fetchConnections() {
        //call API
        OriNetworking.makeRequest(requestType: .get, url: ApiUrls.getConnections, parameters: nil, auth: .send_auth, view: self.view) {(status, error, response) in
            if status {
                if response["status"].bool! {
                    let data = response["data"]
                    self.myConnectionsArray = []
                    for (_,subJson):(String, JSON) in data {
                        self.myConnectionsArray.append(OriUtil.parseSingleConnectionJSON(json: subJson))
                    } // for loop ends here
                    //                        OriUtil.oriLog("Got connections array here,length: "+String(self.myConnectionsArray.count))
                }
            } 
            
            self.loaderCount -= 1
            if self.loaderCount == 0 {
                self.updateData()
            }
        }
    }
    
    func updateData() {
        self.refreshControl.endRefreshing()
        self.view.hideToastActivity()
        if self.myConnectionsArray.count > 0 {
            totalArray.append(ArrayModel(type:"titleCell",data:"My Connections:"))
            for myConnection in myConnectionsArray {
                totalArray.append(ArrayModel(type:"myConnectionsCell",data:myConnection))
            }
        }
        if self.myDevicesArray.count > 0 {
            totalArray.append(ArrayModel(type:"titleCell",data:"My Devices:"))
            for myDevice in myDevicesArray {
                totalArray.append(ArrayModel(type:"myDevicesCell",data:myDevice))
            }
        }
        
        if totalArray.count == 0 { //if no devices and connections
            self.tableView.tableHeaderView = nil
            self.noDevicesHolder.isHidden = false
        } else {
            self.tableView.tableHeaderView = searchController.searchBar
            self.noDevicesHolder.isHidden = true
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController != nil && searchController.isActive {
            return self.totalSearchResults.count
        }
        return self.totalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var item:ArrayModel
        if searchController.isActive {
            item = totalSearchResults[indexPath.item]
        } else {
            item = totalArray[indexPath.item]
        }
        let key = item.type
        if key == "titleCell" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for:indexPath)
            cell.textLabel?.text = (item.data as! String)
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else if key == "myDevicesCell" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myDevicesCell", for:indexPath) as! MyDevicesCell
            
            let myDevice:MyDevice = item.data as! MyDevice
            
            DownloadAndReadImage().renderLocalImage(imageName: myDevice.brandLogoName, imageView: cell.brandImage, folderName: Values.amazonBrandsFolder)
            
            cell.deviceCategory.text = myDevice.listName
            cell.deviceNickName.text = myDevice.deviceTag
            
            var s = ""
            if myDevice.category == "" {
                s = myDevice.brandName
            } else {
                s = myDevice.category
            }
            if myDevice.modelNo != "" {
                s += " | "+myDevice.modelNo
            }
            cell.deviceModel.text = s
            
            if !myDevice.vidBrandVerified {
                cell.verifiedIcon.isHidden = true
            } else {
                cell.verifiedIcon.isHidden = false
            }
            
            if myDevice.warrantyStatus != "" {
                cell.warrantyStatus.text = "Warranty Status: "+myDevice.warrantyStatus
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else if key == "myConnectionsCell" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myConnectionsCell", for: indexPath) as! MyConnectionsCell
            let myConnection:MyConnection = item.data as! MyConnection
            cell.deviceCategory.text = myConnection.listName
            cell.deviceModel.text = "VC No: "+myConnection.VCNo
            cell.deviceNickName.text = "Switch-Off Date: "+myConnection.switchOffDate
            cell.verifiedIcon.isHidden = true
            
            DownloadAndReadImage().renderLocalImage(imageName: myConnection.brandLogoName, imageView: cell.brandImage, folderName: Values.amazonBrandsFolder)
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        self.tableView.deselectRow(at: indexPath, animated: false)
//        OriUtil.oriLog("item selected at ",indexPath.item)
        var item:ArrayModel
        if searchController.isActive {
            item = totalSearchResults[indexPath.item]
        } else {
            item = totalArray[indexPath.item]
        }

        let myDevice:MyDevice = item.data as! MyDevice
        if myDevice.isOnboarded == "true" {
            if self.page == .reqService {
                if serviceType == "Repair" {
                    let storyboard = UIStoryboard(name: "ReqServiceStoryboard", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "repairScreen") as! RepairVC
                    vc.myDevice = myDevice
                    vc.serviceType = serviceType
                    self.show(vc, sender: self)
                } else {
                    let storyboard = UIStoryboard(name: "ReqServiceStoryboard", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "maintScreen") as! MaintenanceVC
                    vc.myDevice = myDevice
                    vc.serviceType = serviceType
                    self.show(vc, sender: self)
                }
            }

        } else if myDevice.brandPhone != "" {
            let msg = "We're working hard to bring "+myDevice.brandName+" services to you directly on Ori. For now, press call or call "+myDevice.brandName+" on "+myDevice.brandPhone+" to book a service request."
            let alertView = UIAlertController(title: "Call "+myDevice.brandName, message: msg, preferredStyle: UIAlertControllerStyle.alert)
            alertView.addAction(UIAlertAction(title: "Call", style: .default, handler: { (alertAction) -> Void in
                OriUtil.callNumber(number: myDevice.brandPhone)
            }))
            alertView.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alertView.presentInOwnWindow()
            
        } else {
            let msg = "We're working hard to bring "+myDevice.brandName+" services to you directly on Ori. For now, please contact "+myDevice.brandName+" through their website or helpline number to book a service request."
            let alertView = UIAlertController(title: "Call "+myDevice.brandName, message: msg, preferredStyle: UIAlertControllerStyle.alert)
            alertView.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alertView.presentInOwnWindow()
        }
    }
    
    
    
    func updateSearchResults(for searchController: UISearchController) {
        self.filterContentForSearchText(searchText: searchController.searchBar.text!)
        self.tableView.reloadData()
    }
    
    func filterContentForSearchText(searchText: String) {
        // Filter the array using the filter method
        self.totalSearchResults = []
        let st = searchText.lowercased()
        if self.myConnectionsArray.count != 0 { //connections exist
            var foundConnection = false
            for a in self.myConnectionsArray {
                if a.category.lowercased().contains(st) ||
                    a.listName.lowercased().contains(st) ||
                    a.subCategory.lowercased().contains(st) ||
                    a.brandSearchArray.lowercased().contains(st) ||
                    a.categorySearchArray.lowercased().contains(st) ||
                    a.brandName.lowercased().contains(st) ||
                    a.VCNo.lowercased().contains(st) ||
                    a.switchOffDate.lowercased().contains(st) ||
                    a.deviceTag.lowercased().contains(st) {
                    if !foundConnection {
                        self.totalSearchResults.append(ArrayModel(type:"titleCell",data:"My Connections:"))
                        foundConnection = true
                    }
                    self.totalSearchResults.append(ArrayModel(type:"myConnectionsCell",data:a))
                }
            }
        }
        
        if self.myDevicesArray.count != 0 {
            var foundDevice = false
            for a in self.myDevicesArray {
                if a.category.lowercased().contains(st) ||
                    a.listName.lowercased().contains(st) ||
                    a.subCategory.lowercased().contains(st) ||
                    a.brandSearchArray.lowercased().contains(st) ||
                    a.categorySearchArray.lowercased().contains(st) ||
                    a.brandName.lowercased().contains(st) ||
                    a.modelNo.lowercased().contains(st) ||
                    a.deviceTag.lowercased().contains(st) {
                    if !foundDevice {
                        self.totalSearchResults.append(ArrayModel(type:"titleCell",data:"My Devices:"))
                        foundDevice = true
                    }
                    self.totalSearchResults.append(ArrayModel(type:"myDevicesCell",data:a))
                }
            }
        }
        
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
    }
    

}
