//
//  DeviceDetailsVC.swift
//  ori
//
//  Created by Harsh Ganatra on 19/12/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import UIKit

class DeviceDetailsVC: UIViewController, UITabBarControllerDelegate, ReturnProtocol {
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    weak var delegate: ReturnProtocol?
    var myDevice:MyDevice!
    
    private lazy var viewController1: DeviceDetailsFragmentVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "MyDevicesStoryboard", bundle: nil)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "deviceDetailsFrag") as! DeviceDetailsFragmentVC
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    private lazy var viewController2: ServiceHistoryVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "MyDevicesStoryboard", bundle: nil)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "serviceHistory") as! ServiceHistoryVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewController2.fetchData(deviceId: myDevice._id)
        viewController1.myDevice = self.myDevice
        viewController1.delegate = self
        viewController1.registerTouch()
        viewController1.updateData()
        
        remove(asChildViewController: viewController2)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.editButton.isEnabled = false
        self.editButton.isEnabled = true
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        willMove(toParentViewController: viewController)
        
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        containerView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = self.containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }

    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var detailsHighlight: UIView!
    @IBAction func detailsButton(_ sender: Any) {
        self.detailsButton.setTitleColor(#colorLiteral(red: 0, green: 0.5463507771, blue: 0.7886484265, alpha: 1), for: .normal)
        self.detailsHighlight.isHidden = false
        self.serviceHistoryButton.setTitleColor(UIColor.lightGray, for: .normal)
        self.serviceHighlight.isHidden = true
        
        remove(asChildViewController: viewController2)
        add(asChildViewController: viewController1)
    }
    
    @IBOutlet weak var serviceHistoryButton: UIButton!
    @IBOutlet weak var serviceHighlight: UIView!
    @IBAction func serviceHistoryButton(_ sender: Any) {
        self.serviceHistoryButton.setTitleColor(#colorLiteral(red: 0, green: 0.5463507771, blue: 0.7886484265, alpha: 1), for: .normal)
        self.serviceHighlight.isHidden = false
        self.detailsButton.setTitleColor(UIColor.lightGray, for: .normal)
        self.detailsHighlight.isHidden = true
        
        remove(asChildViewController: viewController1)
        add(asChildViewController: viewController2)
    }

    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBAction func editButton(_ sender: Any) {
        self.performSegue(withIdentifier: "editDevice", sender: self)
    }
    
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    @IBAction func deleteButton(_ sender: Any) {
        let msg = "Are you sure you want to delete this device?"
        let alertVC = UIAlertController(title: "Delete Device", message: msg, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        alertVC.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
            self.view.makeToastActivity(.center)
            let json:[String:Any] = [
                "_id": self.myDevice._id,
                "state": false
            ]
            //call API
            OriNetworking.makeRequest(requestType: .post, url: ApiUrls.postUpdateDeviceUrl, parameters: json, view: self.view) {(status, error, response) in
                self.view.hideToastActivity()
                if status {
                    if response["status"].bool! {
                        if self.delegate != nil {
                            self.delegate?.getReturnValue(value: ArrayModel(type: "removeDevice", data: self.myDevice))
                        }
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        OriUtil.showToast(view: self.view, msg: "Could not delete device, please try again later")
                    }
                }
            }
        }))
        alertVC.presentInOwnWindow(animated: true, completion: nil)
    }
    
    func getReturnValue(value: ArrayModel) {
        if value.type == "myDevice" {
            self.myDevice = value.data as! MyDevice
            viewController1.myDevice = self.myDevice
            viewController1.updateData()
            if delegate != nil {
                delegate?.getReturnValue(value: ArrayModel(type: "myDevice", data: self.myDevice))
            }
        }
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.identifier == "editDevice" {
            let vc = segue.destination as! EditDeviceVC
            vc.delegate = self
            vc.myDevice = self.myDevice
        }
     }
    
    
}
