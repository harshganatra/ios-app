//
//  MyDevicesVC.swift
//  ori
//
//  Created by Harsh Ganatra on 07/12/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class MyDevicesVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, ReturnProtocol  {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDevicesHolder: UIView!
    var myDevicesArray: [MyDevice] = []
    var myConnectionsArray: [MyConnection] = []
    var totalArray: [ArrayModel] = []
    var totalSearchResults: [ArrayModel] = []
    var loaderCount = 0
    var searchController:UISearchController!
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.barStyle = UIBarStyle.default
            controller.searchBar.barTintColor = UIColor.white
            //            controller.searchBar.backgroundColor = UIColor.clear
            controller.searchBar.placeholder = "Search for device.."
            self.tableView.tableHeaderView = controller.searchBar
            return controller
        })()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        tableView.register(UINib(nibName:"MyDevicesCardLayout",bundle:nil), forCellReuseIdentifier: "myDevicesCell")
        tableView.register(UINib(nibName:"MyConnectionsCardLayout",bundle:nil), forCellReuseIdentifier: "myConnectionsCell")
        self.tableView.tableHeaderView = searchController.searchBar
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.backgroundColor = #colorLiteral(red: 0.9844431281, green: 0.9844661355, blue: 0.9844536185, alpha: 1)
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        self.refreshData()
    }
    
    @IBAction func addDevice(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "AddDeviceStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddDeviceScreen") as UIViewController
        self.show(vc, sender: self)
    }
    
    @IBAction func addConnection(_ sender: Any) {
        OriUtil.showToast(view: self.view, msg: "Coming soon!")
    }
    
    @objc func refreshData(_ sender: Any! = nil) {
        if LoginHelper.isLoggedIn() {
            self.view.makeToastActivity(.center)
            loaderCount += 1; self.fetchDevices()
//            loaderCount += 1; self.fetchConnections()
        } else {
            self.updateData()
        }
//        self.updateData()
    }
    
    func fetchDevices() {
        //call API
        OriNetworking.makeRequest(requestType: .get, url: ApiUrls.getAllDevicesByIdUrl, parameters: nil, auth: .send_auth, view: self.view) {(status, error, response) in
            if status {
                self.myDevicesArray = []

                if response["status"].bool! {
                    let data = response["data"]
                    for (_,subJson):(String, JSON) in data {
                        self.myDevicesArray.append(OriUtil.parseSingleDeviceJSON(json: subJson))
                    } // for loop ends here
                    //                        OriUtil.oriLog("Got devices array here,length: "+String(self.myDevicesArray.count))
                }
            }

            self.loaderCount -= 1
            if self.loaderCount == 0 {
                self.updateData()
            }
        }
    }
    
    func fetchConnections() {
        //call API
        OriNetworking.makeRequest(requestType: .get, url: ApiUrls.getConnections, parameters: nil, auth: .send_auth, view: self.view) {(status, error, response) in
            if status {
                self.myConnectionsArray = []

                if response["status"].bool! {
                    let data = response["data"]
                    for (_,subJson):(String, JSON) in data {
                        self.myConnectionsArray.append(OriUtil.parseSingleConnectionJSON(json: subJson))
                    } // for loop ends here
                    //                        OriUtil.oriLog("Got connections array here,length: "+String(self.myConnectionsArray.count))
                }
            }
            self.loaderCount -= 1
            if self.loaderCount == 0 {
                self.updateData()
            }
        }
    }
    
    func updateData() {
//        OriUtil.oriLog("Update data called")
        self.refreshControl.endRefreshing()
        self.view.hideToastActivity()
        
        self.totalArray = []
        
        if self.myConnectionsArray.count > 0 {
            totalArray.append(ArrayModel(type:"titleCell",data:"My Connections:"))
            for myConnection in myConnectionsArray {
                totalArray.append(ArrayModel(type:"myConnectionsCell",data:myConnection))
            }
        }
        if self.myDevicesArray.count > 0 {
            totalArray.append(ArrayModel(type:"titleCell",data:"My Devices:"))
            for myDevice in myDevicesArray {
                totalArray.append(ArrayModel(type:"myDevicesCell",data:myDevice))
            }
        }
        
        if totalArray.count == 0 { //if no devices and connections
            self.tableView.tableHeaderView = nil
            self.noDevicesHolder.isHidden = false
        } else {
            self.tableView.tableHeaderView = searchController.searchBar
            self.noDevicesHolder.isHidden = true
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive {
            return self.totalSearchResults.count
        }
        return self.totalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var item:ArrayModel
        if searchController.isActive {
            item = totalSearchResults[indexPath.item]
        } else {
            item = totalArray[indexPath.item]
        }
        let key = item.type
        if key == "titleCell" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for:indexPath)
            cell.textLabel?.text = (item.data as! String)
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else if key == "myDevicesCell" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myDevicesCell", for:indexPath) as! MyDevicesCell
            
            let myDevice:MyDevice = item.data as! MyDevice
            
            DownloadAndReadImage().renderLocalImage(imageName: myDevice.brandLogoName, imageView: cell.brandImage, folderName: Values.amazonBrandsFolder)
            
            cell.deviceCategory.text = myDevice.listName
            cell.deviceNickName.text = myDevice.deviceTag
            
            var s = ""
            if myDevice.category == "" {
                s = myDevice.brandName
            } else {
                s = myDevice.category
            }
            if myDevice.modelNo != "" {
                s += " | "+myDevice.modelNo
            }
            cell.deviceModel.text = s
            
            if !myDevice.vidBrandVerified {
                cell.verifiedIcon.isHidden = true
            } else {
                cell.verifiedIcon.isHidden = false
            }
            
            if myDevice.warrantyStatus != "" {
                cell.warrantyStatus.text = "Warranty Status: "+myDevice.warrantyStatus
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        } else if key == "myConnectionsCell" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myConnectionsCell", for: indexPath) as! MyConnectionsCell
            let myConnection:MyConnection = item.data as! MyConnection
            cell.deviceCategory.text = myConnection.listName
            cell.deviceModel.text = "VC No: "+myConnection.VCNo
            cell.deviceNickName.text = "Switch-Off Date: "+myConnection.switchOffDate
            cell.verifiedIcon.isHidden = true
            
            DownloadAndReadImage().renderLocalImage(imageName: myConnection.brandLogoName, imageView: cell.brandImage, folderName: Values.amazonBrandsFolder)
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        self.tableView.deselectRow(at: indexPath, animated: false)
        var item:ArrayModel
        if searchController.isActive {
            item = totalSearchResults[indexPath.item]
        } else {
            item = totalArray[indexPath.item]
        }
        let key = item.type
        
        if key == "myDevicesCell" {
            let myDevice:MyDevice = item.data as! MyDevice
            
            self.performSegue(withIdentifier: "toDeviceDetails", sender: myDevice)
            //            OriUtil.oriLog("selected device ",myDevice.brandName,myDevice.listName)
        } else if key == "myConnectionsCell" {
            let myConnection:MyConnection = item.data as! MyConnection
            self.performSegue(withIdentifier: "toConnectionDetails", sender: myConnection)
            //            OriUtil.oriLog("selected connection ",myConnection.brandName,myConnection.listName)
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        self.filterContentForSearchText(searchText: searchController.searchBar.text!)
        self.tableView.reloadData()
    }
    
    func filterContentForSearchText(searchText: String) {
        // Filter the array using the filter method
        self.totalSearchResults = []
        let st = searchText.lowercased()
        if self.myConnectionsArray.count != 0 { //connections exist
            var foundConnection = false
            for a in self.myConnectionsArray {
                if a.category.lowercased().contains(st) ||
                    a.listName.lowercased().contains(st) ||
                    a.subCategory.lowercased().contains(st) ||
                    a.brandSearchArray.lowercased().contains(st) ||
                    a.categorySearchArray.lowercased().contains(st) ||
                    a.brandName.lowercased().contains(st) ||
                    a.VCNo.lowercased().contains(st) ||
                    a.switchOffDate.lowercased().contains(st) ||
                    a.deviceTag.lowercased().contains(st) {
                    if !foundConnection {
                        self.totalSearchResults.append(ArrayModel(type:"titleCell",data:"My Connections:"))
                        foundConnection = true
                    }
                    self.totalSearchResults.append(ArrayModel(type:"myConnectionsCell",data:a))
                }
            }
        }
        
        if self.myDevicesArray.count != 0 {
            var foundDevice = false
            for a in self.myDevicesArray {
                if a.category.lowercased().contains(st) ||
                    a.listName.lowercased().contains(st) ||
                    a.subCategory.lowercased().contains(st) ||
                    a.brandSearchArray.lowercased().contains(st) ||
                    a.categorySearchArray.lowercased().contains(st) ||
                    a.brandName.lowercased().contains(st) ||
                    a.modelNo.lowercased().contains(st) ||
                    a.deviceTag.lowercased().contains(st) {
                    if !foundDevice {
                        self.totalSearchResults.append(ArrayModel(type:"titleCell",data:"My Devices:"))
                        foundDevice = true
                    }
                    self.totalSearchResults.append(ArrayModel(type:"myDevicesCell",data:a))
                }
            }
        }
        
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "toDeviceDetails") {
            let deviceDetailsVC = segue.destination as! DeviceDetailsVC
            let myDevice = sender as! MyDevice
            deviceDetailsVC.delegate = self
            deviceDetailsVC.myDevice = myDevice
        } else if (segue.identifier == "toConnectionDetails") {
            let deviceDetailsVC = segue.destination as! ConnectionDetailsVC
            let myDevice = sender as! MyConnection
            deviceDetailsVC.delegate = self
            deviceDetailsVC.myConnection = myDevice
        }
        
    }
    
    func getReturnValue(value: ArrayModel) {
        if value.type == "myDevice" {
            let returnedDevice = value.data as! MyDevice
            for i in (0..<myDevicesArray.count) {
                if myDevicesArray[i]._id == returnedDevice._id {
                    myDevicesArray[i] = returnedDevice
                    self.updateData()
                    break
                }
            }
        } else if value.type == "removeDevice" {
            let returnedDevice = value.data as! MyDevice
            for i in (0..<myDevicesArray.count) {
                if myDevicesArray[i]._id == returnedDevice._id {
                    myDevicesArray.remove(at: i)
                    self.updateData()
                    break
                }
            }
        }
    }
    
}

class MyDevicesCell: UITableViewCell {
    
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var deviceCategory: UILabel!
    @IBOutlet weak var deviceModel: UILabel!
    @IBOutlet weak var deviceNickName: UILabel!
    @IBOutlet weak var verifiedIcon: UIImageView!
    @IBOutlet weak var warrantyStatus: UILabel!
}

class MyConnectionsCell: UITableViewCell {
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var deviceCategory: UILabel!
    @IBOutlet weak var deviceModel: UILabel!
    @IBOutlet weak var deviceNickName: UILabel!
    @IBOutlet weak var verifiedIcon: UIImageView!
    @IBOutlet weak var warrantyStatus: UILabel!
}


