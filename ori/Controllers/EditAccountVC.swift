//
//  EditAccountVC.swift
//  ori
//
//  Created by Harsh Ganatra on 19/02/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import Toast_Swift

class EditAccountVC: UIViewController, ReturnProtocol {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mail: UIImageView!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var phone: UIImageView!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var genderText: UITextField!
    @IBOutlet weak var dobText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var user:OriUser = OriUser()
    let textDelegate = TextFieldDelegate()
    var datePicker:UIDatePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mail.tintColor = #colorLiteral(red: 0, green: 0.5463507771, blue: 0.7886484265, alpha: 1)
        profile.tintColor = #colorLiteral(red: 0, green: 0.5463507771, blue: 0.7886484265, alpha: 1)
        phone.tintColor = #colorLiteral(red: 0, green: 0.5463507771, blue: 0.7886484265, alpha: 1)

        user = LoginHelper.getCurrentUser().1
        
        firstName.text = user.firstName
        lastName.text = user.lastName
        genderText.text = user.gender
        mobileText.text = "+91 "+user.phoneNumber
        emailText.text = user.email
        
        firstName.tag = 1
        firstName.delegate = textDelegate
        lastName.tag = 2
        lastName.delegate = textDelegate
        emailText.tag = 4
        emailText.delegate = textDelegate
        
        let button = UIBarButtonItem(title: "Clear", style: UIBarButtonItemStyle.done, target: self, action: #selector(clearPicker))
        button.tintColor = #colorLiteral(red: 0.4401171207, green: 0.4401280284, blue: 0.4401221871, alpha: 1)
        let button3 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closePicker))
        button3.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        let numberToolbar: UIToolbar = UIToolbar()
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items=[
            button,
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button3
        ]
        numberToolbar.sizeToFit()
        self.dobText.inputAccessoryView = numberToolbar //do it for every relevant textfield if there are more than one
        if user.ISOdateOfBirth != "" {
            let date = OriUtil.dateFromISO(user.ISOdateOfBirth)
            datePicker.setDate(date, animated: false)
            dobText.text = OriUtil.getDateStringFromISO(user.ISOdateOfBirth)
        } else {
            datePicker.setDate(OriUtil.dateFromISO(OriUtil.getISO8601StringForDate("01-01-1990")), animated: false)
        }
        datePicker.timeZone = TimeZone.init(abbreviation: "IST")
        datePicker.maximumDate = Date()
        //        datePicker.backgroundColor = #colorLiteral(red: 0.9844431281, green: 0.9844661355, blue: 0.9844536185, alpha: 1)
        datePicker.datePickerMode = UIDatePickerMode.date
        dobText.inputView = datePicker
    }
    
    @objc func clearPicker () {
        self.dobText.resignFirstResponder()
        self.dobText.text = ""
    }
    
    @objc func closePicker () {
        self.dobText.resignFirstResponder()
        self.dobText.text = OriUtil.dateToString(date: self.datePicker.date)
        //        OriUtil.oriLog("date got is ", self.datePicker.date.debugDescription)
    }

    @IBAction func pickGender(_ sender: Any) {
//        self.genderButton.isEnabled = false
        let story = UIStoryboard(name: "PickerVC", bundle: nil)
        let pickerVC = story.instantiateViewController(withIdentifier: "pickerVC") as! PickerVC
        pickerVC.delegate = self
        pickerVC.key = "item"
        var genderList:[ArrayModel] = []
        genderList.append(ArrayModel(type: "gender", data: "Male"))
        genderList.append(ArrayModel(type: "gender", data: "Female"))
        pickerVC.totalArray = genderList
        pickerVC.totalSearchResults = genderList
        
        self.present(pickerVC, animated: true, completion: nil)
    }
    
    func getReturnValue(value: ArrayModel) {
        if value.type == "gender" {
            self.genderButton.isEnabled = true
            self.genderText.text = value.data as? String
        }
    }
    
    
    @IBAction func editMobile(_ sender: Any) {
        OriUtil.showToast(view: self.view, msg: "Mobile number cannot be edited")
    }
    
    @IBAction func saveButton(_ sender: Any) {
        if !Validation.hasText(textField: self.firstName) {
            OriUtil.showToast(view: self.view, msg: "Please enter first name")
        } else if !Validation.hasText(textField: self.lastName) {
            OriUtil.showToast(view: self.view, msg: "Please enter last name")
        } else if !Validation.isValidEmail(textField: self.emailText) {
            OriUtil.showToast(view: self.view, msg: "Please enter a valid email id")
        } else {
            self.view.makeToastActivity(.center)
            self.saveButton.isEnabled = false
            
            var json:[String:Any]  = [
                "firstName": self.firstName.text!,
                "lastName": self.lastName.text!,
                "email": self.emailText.text!,
                "platform": "iOS",
                "gcmRegistrationToken": "",
            ]
            user.firstName = self.firstName.text!
            user.lastName = self.lastName.text!
            user.email = self.emailText.text!
            
            if genderText.text != "" {
                user.gender = self.genderText.text!
                json["gender"] = self.genderText.text!
            }
            
            if dobText.text != "" {
                user.ISOdateOfBirth = OriUtil.getISO8601StringForDate(self.datePicker.date)
                json["dateOfBirth"] = user.ISOdateOfBirth
            }
            
            //call API
            OriNetworking.makeRequest(requestType: .post, url: ApiUrls.updateCustomerUrl, parameters: json, view: self.view) {(status, error, response) in
                self.view.hideToastActivity()
                if status {
                    if response["status"].bool! {
                        let defaults = UserDefaults.standard
                        defaults.set(self.user.firstName, forKey: Values.FIRST_NAME_KEY)
                        defaults.set(self.user.lastName, forKey: Values.LAST_NAME_KEY)
                        defaults.set(self.user.email, forKey: Values.EMAIL_KEY)
                        defaults.set(self.user.phoneNumber, forKey: Values.MOBILE_KEY)
                        defaults.set(self.user.gender, forKey: Values.GENDER_KEY)
                        defaults.set(self.user.ISOdateOfBirth, forKey: Values.ISO_DOB_KEY)
                                                
                        //TODO: Crashlyticcs update info
                        //TODO: Clevertap update info
                        
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        OriUtil.checkErrorFromJSON(view: self.view, json: response)
                    }
                }
                self.saveButton.isEnabled = true
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        self.subscribeToKeyboardNotifications()
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.unsubscribeToKeyboardNotifications()
    }
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(_ :)),
                                               name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: .UIKeyboardWillHide, object: nil)
    }
    func unsubscribeToKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyBoardWillShow(_ notification:Notification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = getKeyboardHeight(notification)
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(_ notification:Notification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }
}
