//
//  AddDevice2VC.swift
//  ori
//
//  Created by Harsh Ganatra on 04/01/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import SDWebImage
import MobileCoreServices

class AddDevice2VC: UIViewController, UIDocumentPickerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentInteractionControllerDelegate, UITextFieldDelegate {
    //UINavigationControllerDelegate is for camera picker
    //UIDocumentInteractionControllerDelegate is to view files
    
    weak var delegate: ReturnProtocol?
    var myDevice:MyDevice = MyDevice()
    let textDelegate = TextFieldDelegate()
    let datePicker = UIDatePicker()
    let imageHelper = DownloadAndReadImage()
    
    //Add device 2 outlets
    @IBOutlet weak var nickNameText: UITextField!
    @IBOutlet weak var retailerNameText: UITextField!
    @IBOutlet weak var datePickerText: UITextField!
    @IBOutlet weak var warrantyText: UITextField!
    @IBOutlet weak var addDevicePhotoHolder: UIStackView!
    @IBOutlet weak var devicePhoto: UIImageView!
    @IBOutlet weak var warrantyHolder: UIStackView!
    @IBOutlet weak var warranty: UIImageView!
    @IBOutlet weak var invoiceHolder: UIStackView!
    @IBOutlet weak var invoice: UIImageView!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
    @objc public enum PhotoType:Int {
        
        case devicePhoto = 1
        case warranty = 2
        case invoice = 3
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nickNameText.delegate = textDelegate
        nickNameText.tag = 0
        retailerNameText.delegate = textDelegate
        retailerNameText.tag = 1
        
        let button = UIBarButtonItem(title: "Don't Know", style: UIBarButtonItemStyle.done, target: self, action: #selector(clearPicker))
        button.tintColor = #colorLiteral(red: 0.4401171207, green: 0.4401280284, blue: 0.4401221871, alpha: 1)
        let button3 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closePicker))
        button3.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        let numberToolbar: UIToolbar = UIToolbar()
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items=[
            button,
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button3
        ]
        numberToolbar.sizeToFit()
        self.datePickerText.inputAccessoryView = numberToolbar //do it for every relevant textfield if there are more than one
        datePicker.setDate(Date(), animated: false)
        datePicker.timeZone = TimeZone.init(abbreviation: "IST")
        datePicker.maximumDate = Date()
        datePicker.minimumDate = Date(timeIntervalSince1970: 0)
//        datePicker.backgroundColor = #colorLiteral(red: 0.9844431281, green: 0.9844661355, blue: 0.9844536185, alpha: 1)
        datePicker.datePickerMode = UIDatePickerMode.date
        datePickerText.inputView = datePicker
        
        let numberToolbar2: UIToolbar = UIToolbar()
        numberToolbar2.barStyle = UIBarStyle.default
        let button2 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(closeWarrantyText))
        button2.tintColor = #colorLiteral(red: 0, green: 0.4237698913, blue: 0.6720848083, alpha: 1)
        numberToolbar2.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            button2
        ]
        numberToolbar2.sizeToFit()
        self.warrantyText.inputAccessoryView = numberToolbar2 //do it for every relevant textfield if there are more than one
        self.warrantyText.delegate = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(chooseDeviceImage))
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressDeviceImage))
        addDevicePhotoHolder.isUserInteractionEnabled = true
        addDevicePhotoHolder.addGestureRecognizer(tapGestureRecognizer)
        addDevicePhotoHolder.addGestureRecognizer(longPressGesture)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(chooseWarranty))
        let longPressGesture2 = UILongPressGestureRecognizer(target: self, action: #selector(longPressWarranty))
        warrantyHolder.isUserInteractionEnabled = true
        warrantyHolder.addGestureRecognizer(tapGestureRecognizer2)
        warrantyHolder.addGestureRecognizer(longPressGesture2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(chooseInvoice))
        let longPressGesture3 = UILongPressGestureRecognizer(target: self, action: #selector(longPressInvoice))
        invoiceHolder.isUserInteractionEnabled = true
        invoiceHolder.addGestureRecognizer(tapGestureRecognizer3)
        invoiceHolder.addGestureRecognizer(longPressGesture3)
    }
    
    @objc func clearPicker () {
        self.datePickerText.resignFirstResponder()
        self.datePickerText.text = ""
    }
    
    @objc func closePicker () {
        self.datePickerText.resignFirstResponder()
        self.datePickerText.text = OriUtil.dateToString(date: self.datePicker.date)
//        OriUtil.oriLog("date got is ", self.datePicker.date.debugDescription)
    }
    
    @objc func closeWarrantyText () {
        self.warrantyText.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        nickNameText.text = myDevice.deviceTag
        retailerNameText.text = myDevice.retailerName

        if myDevice.dateOfPurchase != "" {
            let date = OriUtil.dateFromISO(myDevice.dateOfPurchase)
            datePicker.setDate(date, animated: false)
            datePickerText.text = OriUtil.dateToString(date: datePicker.date)
        }
        
        self.warrantyText.text = myDevice.warrantyPeriod
        
        //setting images if user went back to AddDevice and is coming back
        if !imageHelper.setImageIfExists(imageView: devicePhoto, imageName: myDevice.deviceImage) {
            myDevice.deviceImage = "" //could not find device image, resetting field
        }
        if !imageHelper.setImageIfExists(imageView: invoice, imageName: myDevice.invoiceFile) {
            myDevice.invoiceFile = ""
        }
        if !imageHelper.setImageIfExists(imageView: warranty, imageName: myDevice.warrantyFile) {
            myDevice.warrantyFile = ""
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.addDetails()
        if self.isMovingFromParentViewController {
            if delegate != nil { //pass mydevice back to AddDeviceVC so that details are saved and can be loaded back
                delegate?.getReturnValue(value: ArrayModel(type: "addDevice2", data: myDevice as Any))
            }
        }
    }
   
    func addDetails() {
        myDevice.deviceTag = self.nickNameText.text!
        myDevice.retailerName = self.retailerNameText.text!
        if datePickerText.text != "" {
            myDevice.dateOfPurchase = OriUtil.getISO8601StringForDate(self.datePicker.date)
        } else {
            myDevice.dateOfPurchase = ""
        }
        myDevice.warrantyPeriod = self.warrantyText.text!
    }

    @IBAction func nextButton(_ sender: Any) {
        self.addDetails()
        let login = LoginHelper.getCurrentUser()
        if !login.0 {
            let storyboard: UIStoryboard = UIStoryboard(name: "LoginStoryboard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginScreen") as UIViewController
            (vc as! LoginVC).toPage = "addDevice"
            self.show(vc, sender: self)
        } else {
            self.view.makeToastActivity(.center)
            self.nextButton.isEnabled = false
            let myDevice = self.myDevice
            var json:[String:Any]  = [
                "userId": login.1.userID,
                "brandId": myDevice.brandId,
                "categorySlug": myDevice.categorySlug,
                "category": myDevice.category,
                "subCategory": myDevice.subCategory,
                "deviceTag": myDevice.deviceTag,
                "retailer": myDevice.retailerName,
                "model": myDevice.modelNo,
                "serialNumber": myDevice.serialNumber,
                "createdAt": OriUtil.getISO8601StringForCurrentDate(),
                "modifiedAt": OriUtil.getISO8601StringForCurrentDate(),
                "deviceImg": myDevice.deviceImage,
                "invoiceFile": myDevice.invoiceFile,
                "warrantyFile": myDevice.warrantyFile
            ]
            if !(warrantyText.text == "") {
                json["warrantyPeriod"] = Int(warrantyText.text!)
            }
            if myDevice.modelCode != "" {
                json["model_code"] = myDevice.modelCode
            }
            if myDevice.dateOfPurchase != "" {
                json["dateOfPurchase"] = myDevice.dateOfPurchase
            }
            
            //call API
            OriNetworking.makeRequest(requestType: .put, url: ApiUrls.putAddDeviceUrl, parameters: json, view: self.view) {(status, error, response) in
                self.view.hideToastActivity()
                if status {
                    if response["status"].bool! {
                        if myDevice.deviceImage != "" {
                            self.imageHelper.uploadFileToAWS(imageName: myDevice.deviceImage)
                        }
                        if myDevice.invoiceFile != "" {
                            self.imageHelper.uploadFileToAWS(imageName: myDevice.invoiceFile)
                        }
                        if myDevice.warrantyFile != "" {
                            self.imageHelper.uploadFileToAWS(imageName: myDevice.warrantyFile)
                        }
                        
                        self.myDevice._id = response["data"].stringValue
                        self.performSegue(withIdentifier: "toSuccess", sender: nil)
                    } else {
                        OriUtil.checkErrorFromJSON(view: self.view, json: response)
                    }
                } 
                self.nextButton.isEnabled = true
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == warrantyText {
            var newText = textField.text! as NSString
            newText = newText.replacingCharacters(in: range, with: string) as NSString
            return newText.length <= 2
        }
        return true
    }
    
    
    @objc func chooseDeviceImage() {
        if myDevice.deviceImage != "" {
            self.openFile(imageName: myDevice.deviceImage)
        } else {
            showPicker(photoType: .devicePhoto)
        }
    }
    
    @objc func longPressDeviceImage(sender:UILongPressGestureRecognizer) {
        if sender.state == .began && myDevice.deviceImage != "" {
            showMenu(photoType: .devicePhoto)
        }
    }
    
    @objc func deleteDeviceImage() {
        self.devicePhoto.image = #imageLiteral(resourceName: "ic_add_device_image")
        imageHelper.deleteLocalFile(fileName: myDevice.deviceImage)
        self.myDevice.deviceImage = ""
    }
    
    @objc func replaceDeviceImage() {
        showPicker(photoType: .devicePhoto)
    }
    
    @objc func chooseWarranty() {
        if myDevice.warrantyFile != "" {
            self.openFile(imageName: myDevice.warrantyFile)
        } else {
            showPicker(photoType: .warranty)
        }
    }
    
    @objc func longPressWarranty(sender:UILongPressGestureRecognizer) {
        if sender.state == .began && myDevice.warrantyFile != "" {
            showMenu(photoType: .warranty)
        }
    }
    
    @objc func deleteWarranty() {
        self.warranty.image = #imageLiteral(resourceName: "ic_add_warranty")
        imageHelper.deleteLocalFile(fileName: myDevice.warrantyFile)
        self.myDevice.warrantyFile = ""
    }
    
    @objc func replaceWarranty() {
        showPicker(photoType: .warranty)
    }
    
    @objc func chooseInvoice() {
        if myDevice.invoiceFile != "" {
            self.openFile(imageName: myDevice.invoiceFile)
        } else {
            showPicker(photoType: .invoice)
        }
    }
    
    @objc func longPressInvoice(sender:UILongPressGestureRecognizer) {
        if sender.state == .began && myDevice.invoiceFile != "" {
            showMenu(photoType: .invoice)
        }
    }
    
    @objc func deleteInvoice() {
        self.invoice.image = #imageLiteral(resourceName: "ic_add_invoice")
        imageHelper.deleteLocalFile(fileName: myDevice.invoiceFile)
        self.myDevice.invoiceFile = ""
    }
    
    @objc func replaceInvoice() {
        showPicker(photoType: .invoice)
    }
    
    func showMenu(photoType:PhotoType) {
        OriUtil.oriLog("long pressed ",photoType)
        becomeFirstResponder()
        let menu = UIMenuController.shared
        var item, item2: UIMenuItem
        if photoType == .devicePhoto {
            item = UIMenuItem(title: "Replace", action: #selector(AddDevice2VC.replaceDeviceImage))
            item2 = UIMenuItem(title: "Delete", action: #selector(AddDevice2VC.deleteDeviceImage))
            menu.menuItems = [item, item2]
            let bounds = devicePhoto.bounds
            let rect = CGRect(x: bounds.midX, y:  bounds.midY/2.0, width: 0, height: 0)
            menu.setTargetRect(rect, in: addDevicePhotoHolder)
        } else if photoType == .invoice {
            item = UIMenuItem(title: "Replace", action: #selector(AddDevice2VC.replaceInvoice))
            item2 = UIMenuItem(title: "Delete", action: #selector(AddDevice2VC.deleteInvoice))
            menu.menuItems = [item, item2]
            let bounds = invoiceHolder.bounds
            let rect = CGRect(x: bounds.midX, y:  bounds.midY/2.0, width: 0, height: 0)

            menu.setTargetRect(rect, in: invoiceHolder)
        } else if photoType == .warranty {
            item = UIMenuItem(title: "Replace", action: #selector(AddDevice2VC.replaceWarranty))
            item2 = UIMenuItem(title: "Delete", action: #selector(AddDevice2VC.deleteWarranty))
            menu.menuItems = [item, item2]
            let bounds = warrantyHolder.bounds
            let rect = CGRect(x: bounds.midX, y:  bounds.midY/2.0, width: 0, height: 0)
            menu.setTargetRect(rect, in: warrantyHolder)
        }

        menu.arrowDirection = .down
        
        menu.setMenuVisible(true, animated: true)
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    func showPicker(photoType:PhotoType) {
        let pickerController = UIAlertController()
        
        let photoAction = UIAlertAction(title:"Take Photo", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let cameraPicker = UIImagePickerController()
                cameraPicker.sourceType = UIImagePickerControllerSourceType.camera
                cameraPicker.cameraCaptureMode = .photo
                cameraPicker.allowsEditing=false
                cameraPicker.delegate=self
                cameraPicker.view.tag = photoType.rawValue
                self.present(cameraPicker, animated: true, completion: nil)
            } else {
                OriUtil.showToast(view: self.view, msg: "Camera not available")
            }
        }
        var repair = #imageLiteral(resourceName: "take_photo")
        repair = repair.scaled(to: CGSize(width: 35, height: 35))
        photoAction.setValue(repair, forKey: "image")
        
        let galleryAction = UIAlertAction(title:"Upload from Gallery", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            imagePicker.view.tag = photoType.rawValue
            self.present(imagePicker, animated: true, completion: nil)
        }
        var image = #imageLiteral(resourceName: "choose_from_gallery")
        image = image.scaled(to: CGSize(width: 35, height: 35))
        galleryAction.setValue(image, forKey: "image")
        
        let pdfAction = UIAlertAction(title:"Upload a PDF", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            importMenu.allowsMultipleSelection = false
            importMenu.delegate = self
            importMenu.view.tag = photoType.rawValue
            importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion: nil)
        }
        var image2 = #imageLiteral(resourceName: "choose_pdf")
        image2 = image2.scaled(to: CGSize(width: 35, height: 35))
        pdfAction.setValue(image2, forKey: "image")
        
        let cancel = UIAlertAction(title:"Close", style: UIAlertActionStyle.cancel) { action in
            pickerController.dismiss(animated: true, completion: nil)
        }
        
        pickerController.addAction(photoAction)
        pickerController.addAction(galleryAction)
        if photoType != .devicePhoto { // no PDF for device photo
            pickerController.addAction(pdfAction)
        }
        pickerController.addAction(cancel)
        
        if photoType == .devicePhoto {
            pickerController.title = "Upload Device Image:"
            pickerController.popoverPresentationController?.sourceView = self.addDevicePhotoHolder // works for both iPhone & iPad
        } else if photoType == .invoice {
            pickerController.title = "Upload Invoice:"
            pickerController.popoverPresentationController?.sourceView = self.invoiceHolder // works for both iPhone & iPad
        } else if photoType == .warranty {
            pickerController.title = "Upload Warranty:"
            pickerController.popoverPresentationController?.sourceView = self.warrantyHolder // works for both iPhone & iPad
        }
        
        pickerController.view.tintColor = #colorLiteral(red: 0.4401171207, green: 0.4401280284, blue: 0.4401221871, alpha: 1)
        
        self.present(pickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true, completion: nil)
        
        if picker.view.tag == PhotoType.devicePhoto.rawValue {
            OriUtil.oriLog("Picked device image")
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                deleteDeviceImage()
                devicePhoto.image = image
                myDevice.deviceImage = getImageName(type: .devicePhoto)
                imageHelper.saveImageToLocalStorage(image: image, fileNameWithExtension: myDevice.deviceImage)
            }
        } else if picker.view.tag == PhotoType.warranty.rawValue {
            OriUtil.oriLog("Picked warranty image")
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                deleteWarranty()
                warranty.image = image
                myDevice.warrantyFile = getImageName(type: .warranty)
                imageHelper.saveImageToLocalStorage(image: image, fileNameWithExtension: myDevice.warrantyFile)
            }
        } else if picker.view.tag == PhotoType.invoice.rawValue {
            OriUtil.oriLog("Picked invoice image")
            if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                deleteInvoice()
                invoice.image = image
                myDevice.invoiceFile = getImageName(type: .invoice)
                imageHelper.saveImageToLocalStorage(image: image, fileNameWithExtension: myDevice.invoiceFile)
            }
        }
    }

    
    func getImageName(type:PhotoType, ext:String = ".png") -> String {
        if type == .devicePhoto {
            return "ori_devimg_"+OriUtil.getDateTimeStamp()+ext
        } else if type == .invoice {
            return "ori_billimg_"+OriUtil.getDateTimeStamp()+ext
        } else if type == .warranty {
            return "ori_warrimg_"+OriUtil.getDateTimeStamp()+ext
        } else {
            OriUtil.oriLog("invalid photo type")
            return ""
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let url = urls[0] as URL
        OriUtil.oriLog("The Url is : \(url)")
        OriUtil.oriLog("last component of url" , url.lastPathComponent)
        
        if controller.view.tag == PhotoType.invoice.rawValue {
            myDevice.invoiceFile = getImageName(type: .invoice, ext: ".pdf")
            imageHelper.copyUrlToLocal(url: url, fileName: myDevice.invoiceFile)
            invoice.image = #imageLiteral(resourceName: "pdf")
        } else if controller.view.tag == PhotoType.warranty.rawValue {
            myDevice.warrantyFile = getImageName(type: .warranty, ext: ".pdf")
            imageHelper.copyUrlToLocal(url: url, fileName: myDevice.warrantyFile)
            warranty.image = #imageLiteral(resourceName: "pdf")
        }
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func openFile(imageName:String) {
        let x = imageHelper.fileExistsInLocal(imageName: imageName)
        if x.0 {
            let documentInteractionController = UIDocumentInteractionController(url: x.1!)
            documentInteractionController.delegate = self
            documentInteractionController.presentPreview(animated: true)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "toSuccess") {
            let vc = segue.destination as! SuccessVC
            vc.myDevice = self.myDevice
        }
    }

}

//class CameraPicker: UIImagePickerController {
//     var shouldAutorotate: Bool = false
//}

