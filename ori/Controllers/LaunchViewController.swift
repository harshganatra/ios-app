//
//  LaunchViewController.swift
//  ori
//
//  Created by Harsh Ganatra on 06/10/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import UIKit
import SwiftyJSON

class LaunchViewController: UIViewController {
    
    @IBOutlet weak var activityController: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        activityController.startAnimating()
        
        let defaults = UserDefaults.standard
        var brandUrl:String = ApiUrls.getBrandUrl
        let version = defaults.string(forKey: Values.brand_version_no)
        if version != nil {
            //            OriUtil.oriLog ("saved verion number: \(version!)")
            brandUrl += ("?version="+version!)
            //            brandUrl += ("?version="+"0") //used to reset brand db in debug
            
        }
        
        //call brands API
        OriNetworking.makeRequest(requestType: .get, url: brandUrl, parameters: nil, auth: .no_auth) {(status, error, response) in
            if status {
                //                OriUtil.oriLog("brand: ",response)
                self.brandDBupdate(brandsJSON: response)
            } else {
                //no network
                self.performSegue(withIdentifier: "noInternet", sender: nil)
            }
        }
    }
    
    func brandDBupdate( brandsJSON:JSON) {
        
        if brandsJSON["status"].bool! {
            
            //write brand version number to user defaults
            let brandVersion = brandsJSON["version"].intValue
            OriUtil.oriLog("brandVersion is: \(brandVersion)")
            
            let defaults = UserDefaults.standard
            defaults.set(String(describing: brandVersion), forKey: Values.brand_version_no)
            
            try! BrandDataHelper.createTable()
            
            let data = brandsJSON["data"]
            
            for (_,subJson):(String, JSON) in data {
                var isOnboarded, hasCategory, categoryJSON, amcAvailable, androidBrandArray, categoriesList, trackAvailable, trackJSON:String
                if subJson["isOnboarded"].bool! {
                    isOnboarded = "true"
                } else {
                    isOnboarded = "false"
                }
                if subJson["categories"].exists() , let category = subJson["categories"].rawString() {
                    categoriesList = category
                } else {
                    categoriesList = ""
                }
                
                if subJson["androidBrandArray"].exists() , let category = subJson["androidBrandArray"].rawString() {
                    androidBrandArray = category
                } else {
                    androidBrandArray = ""
                }
                
                if subJson["amcCategories"].exists() {
                    let amcCategories = subJson["amcCategories"].array
                    if let count = amcCategories?.count, count>0 {
                        amcAvailable = "1"
                    } else {amcAvailable = "0" }
                } else {amcAvailable = "0"}
                
                if subJson["category"].exists(), let category = subJson["category"].rawString() {
                    hasCategory = "1"
                    categoryJSON = category
                } else {
                    hasCategory = "0"
                    categoryJSON = ""
                }
                
                if let trackingAvailable = subJson["trackingAvailable"].bool, trackingAvailable && subJson["trackingForm"].exists(){
                    trackAvailable = "1"
                    trackJSON = subJson["trackingForm"].rawString()!
                } else {
                    trackAvailable = "0"
                    trackJSON = ""
                }
                do {
                    let _ = try BrandDataHelper.insert(
                        item:Brand(brandId : subJson["brandId"].intValue,
                                   brandLogoName : subJson["logoImg"].stringValue,
                                   isOnboarded : isOnboarded,
                                   serviceNumber : subJson["serviceNumber"].stringValue,
                                   brandName : subJson["title"].stringValue,
                                   hasCategory : hasCategory,
                                   brandCategory: "",
                                   brandSubCategory: "",
                                   trackingAvailable : trackAvailable,
                                   amcAvailable : amcAvailable,
                                   androidBrandArray : androidBrandArray,
                                   categoriesList : categoriesList,
                                   trackingJSONArray : trackJSON,
                                   categoryJSON : categoryJSON))
//                    OriUtil.oriLog(id)
                } catch _{}
            }
            
        } else { //brands data up to date
//            OriUtil.oriLog(brandsJSON["data"])
        }
        
        let defaults = UserDefaults.standard
        var catURL:String = ApiUrls.getCategoriesUrl
        let catVersion:String! = defaults.string(forKey: Values.categories_version_no)
        if catVersion != nil {
//            OriUtil.oriLog ("cat saved verion number: \(catVersion!)")
            catURL += ("?version=\(catVersion!)")
//            catURL += ("?version="+"0") //used to reset cat db in debug

        }
        
        //call categories API
        OriNetworking.makeRequest(requestType: .get, url: catURL, parameters: nil, auth: .no_auth) {(status, error, response) in
            if status {
                self.categoryDBupdate(catJSON: response)
            } else {
                //no internet
                self.performSegue(withIdentifier: "noInternet", sender: nil)
            }
        }
        
    }
    
    func categoryDBupdate( catJSON:JSON) {
        
        if catJSON["status"].bool! {
            
            //write brand version number to user defaults
            let catVersion = catJSON["version"].intValue
//            OriUtil.oriLog("brandVersion is: \(catVersion)")
            let defaults = UserDefaults.standard
            defaults.set(String(describing: catVersion), forKey: Values.categories_version_no)
            
            try! CategoryDataHelper.createTable()
            
            let data = catJSON["data"]
            
            for (_,subJson):(String, JSON) in data {
                var category, brandIds, amcAvailable, amcBrandIds, serviceList, searchArray:String
                
                if subJson["tags"].exists() , let categoryarray = subJson["tags"].array {
                    category = categoryarray[0].stringValue
                } else {
                    category = ""
                }
                
                if subJson["brandIds"].exists() , let brandIdArray = subJson["brandIds"].rawString() {
                    brandIds = brandIdArray
                } else {
                    brandIds = ""
                }
                
                if subJson["amcBrandArray"].exists() , let amcArray = subJson["amcBrandArray"].rawString() {
                    amcBrandIds = amcArray
                    amcAvailable = "1"
                } else {
                    amcBrandIds = ""
                    amcAvailable = "0"
                }
                
                if subJson["serviceIds"].exists() , let array = subJson["serviceIds"].rawString() {
                    serviceList = array
                } else {
                    serviceList = ""
                }
                
                if subJson["searchArray"].exists() , let array = subJson["searchArray"].rawString() {
                    searchArray = array
                } else {
                    searchArray = ""
                }
                
                
                do {
                    let _ = try CategoryDataHelper.insert(
                        item:OriCategory(listName : subJson["listName"].stringValue,
                                         shortName : subJson["shortName"].stringValue,
                                         category : category,
                                         categorySlug : subJson["slug"].stringValue,
                                         categoryID : "",
                                         respectiveBrandIDs : brandIds,
                                         imageName: subJson["mainIcon"].stringValue,
                                         searchArray: searchArray,
                                         serviceList : serviceList,
                                         AMCBrandIDS : amcBrandIds,
                                         amcAvailable : amcAvailable ))
                    //                    OriUtil.oriLog("id is \(bosId)")
                } catch _{}
                
            }
            
        } else { //categories data up to date
//            OriUtil.oriLog(catJSON["data"])
        }
        
        self.goToHome()
    }
    
    func goToHome() {
//        print("going to home")
        let storyboard: UIStoryboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as UIViewController
        
        UIApplication.shared.keyWindow!.replaceRootViewControllerWith(vc, animated: true, completion: nil)
    }
    //        let navigationController = self.navigationController
    //        navigationController?.setViewControllers([vc], animated: true)
    //        self.dismiss(animated: true, completion: nil)
    //        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
    //        self.show(vc, sender: self)
//        OriUtil.oriLog("find ac")
//        let c = try! BrandDataHelper.find(id:2)
//        if let co = c {
//            OriUtil.oriLog(co)
//        }
//        let c:OriCategory = (try! CategoryDataHelper.find(slug:"air_conditioner"))!
//        OriUtil.oriLog(c)
    
//    //Declare enum
//    enum AnimationType{
//        case ANIMATE_RIGHT
//        case ANIMATE_LEFT
//        case ANIMATE_UP
//        case ANIMATE_DOWN
//    }
//    // Create Function...
//
//    func showViewControllerWith(newViewController:UIViewController, usingAnimation animationType:AnimationType)
//    {
//
//        let currentViewController = UIApplication.shared.delegate?.window??.rootViewController
//        let width = currentViewController?.view.frame.size.width;
//        let height = currentViewController?.view.frame.size.height;
//
//        var previousFrame:CGRect?
//        var nextFrame:CGRect?
//
//        switch animationType
//        {
//        case .ANIMATE_LEFT:
//            previousFrame = CGRect(x:width!-1, y:0.0, width:width!, height:height!)
//            nextFrame = CGRect(-width!, 0.0, width!, height!);
//        case .ANIMATE_RIGHT:
//            previousFrame = CGRect(-width!+1, 0.0, width!, height!);
//            nextFrame = CGRect(width!, 0.0, width!, height!);
//        case .ANIMATE_UP:
//            previousFrame = CGRect(0.0, height!-1, width!, height!);
//            nextFrame = CGRect(0.0, -height!+1, width!, height!);
//        case .ANIMATE_DOWN:
//            previousFrame = CGRect(0.0, -height!+1, width!, height!);
//            nextFrame = CGRect(0.0, height!-1, width!, height!);
//        }
//
//        newViewController.view.frame = previousFrame!
//        UIApplication.shared.delegate?.window??.addSubview(newViewController.view)
//        UIView.animate(withDuration: 0.33,
//                                   animations: { () -> Void in
//                                    newViewController.view.frame = (currentViewController?.view.frame)!
//                                    currentViewController?.view.frame = nextFrame!
//
//        })
//        { (fihish:Bool) -> Void in
//            UIApplication.shared.delegate?.window??.rootViewController = newViewController
//        }
//    }
    
}

