//
//  CardDetailsVC.swift
//  ori
//
//  Created by Harsh Ganatra on 09/03/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit

class CardDetailsVC: UIViewController {

    weak var delegate:ReturnProtocol?
    var myCard:MyCard = MyCard()
    
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var cardTitle: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var complaintNo: UILabel!
    @IBOutlet weak var cardNo: UILabel!
    @IBOutlet weak var schedule: UILabel!
    @IBOutlet weak var tech_name: UILabel!
    @IBOutlet weak var tech_phone: UILabel!
    @IBOutlet weak var complaintStatus: UILabel!
    @IBOutlet weak var complaintLastUpdated: UILabel!
    @IBOutlet weak var phoneHolder: UIStackView!
    
    @IBOutlet weak var techDetailsLayout: UIView!
    @IBOutlet weak var historyLayout: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DownloadAndReadImage().renderLocalImage(imageName: myCard.brandLogoName, imageView: self.brandImage, folderName: Values.amazonBrandsFolder)
        self.cardTitle.text = myCard.title
        self.subTitle.text = myCard.subTitle
        self.complaintNo.text = myCard.complaintNumber
        self.cardNo.text = myCard.cardNumber
        self.schedule.text = myCard.schedule
        self.complaintNo.text = myCard.complaintNumber

        if myCard.engineerName != "NA" && myCard.engineerName != "" {
            techDetailsLayout.isHidden = false
            tech_name.text = myCard.engineerName
            
            if myCard.engineerPhone != "" {
                phoneHolder.isHidden = false
                tech_phone.text = myCard.engineerPhone
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(callEngineer))
                phoneHolder.isUserInteractionEnabled = true
                phoneHolder.addGestureRecognizer(tapGestureRecognizer)
            } else {
                phoneHolder.isHidden = true
            }
        } else {
            techDetailsLayout.isHidden = true
        }
        
        self.historyLayout.isHidden = true
        
        self.navigationItem.rightBarButtonItem = nil
    }

    @objc func callEngineer() {
        let phone = myCard.engineerPhone
        let msg = "Do you want to call your technician on "+phone+" now?"
        let alertVC = UIAlertController(title: "Call", message: msg, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        alertVC.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
            OriUtil.callNumber(number: phone)
        }))
        alertVC.presentInOwnWindow(animated: true, completion: nil)
    }
    
    @IBAction func cancelReq(_ sender: Any) {
        let status = myCard.status.lowercased()
        if status == "finished" {
            OriUtil.showToast(view: self.view, msg: "Booking has already been completed")
        } else if status == "cancelled" {
            OriUtil.showToast(view: self.view, msg: "Booking has already been cancelled")
        } else if status == "booked" {
            let msg = "Once this service is cancelled, it cannot be undone."
            let alertVC = UIAlertController(title: "Cancel Service?", message: msg, preferredStyle: .alert)
            alertVC.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alertVC.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (_) in
                
                self.view.makeToastActivity(.center)

                let json:[String:Any] = [
                    "cardNumber": self.myCard.cardNumber,
                    "status": "Cancelled",
                    "complaintStatus": "Cancelled",
                    "complaintStatusDetails": "Cancelled by client"
                ]
                //call API
                OriNetworking.makeRequest(requestType: .post, url: ApiUrls.updateCardUrl, parameters: json, view: self.view) {(status, error, response) in
                    self.view.hideToastActivity()
                    if status {
                        if response["status"].bool! {
                            if self.delegate != nil {
                                self.delegate?.getReturnValue(value: ArrayModel(type: "removeCard", data: self.myCard))
                            }
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            OriUtil.showErrorToast(view: self.view)
                        }
                    }
                }
            }))
            alertVC.presentInOwnWindow(animated: true, completion: nil)
        } else {
            let phone = myCard.brandPhone
            let msg = "Your service has already been confirmed by "+myCard.brandName+". Please call the brand directly on "+phone+" to cancel the service request."
            let alertVC = UIAlertController(title: "Cancel booked service with brand?", message: msg, preferredStyle: .alert)
            alertVC.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alertVC.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                OriUtil.callNumber(number: phone)
            }))
            alertVC.presentInOwnWindow(animated: true, completion: nil)
        }
    }
    
    @IBAction func escalate(_ sender: Any) {
        let status = myCard.status.lowercased()
        if status == "finished" || status == "cancelled" {
            OriUtil.showToast(view: self.view, msg: "Cannot escalate this booking")
        } else if myCard.isEscalated {
            OriUtil.showToast(view: self.view, msg: "Booking has been already escalated recently, please try again after a while")
        } else {
            //TODO show textbox to escalate card
            
        }
    }
    
    @IBOutlet weak var refresh: UIBarButtonItem!
    @IBAction func refresh(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
