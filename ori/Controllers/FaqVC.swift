//
//  FaqVC.swift
//  ori
//
//  Created by Harsh Ganatra on 22/02/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit

class FaqVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var selectedQuestion:Int = -1
    var faqQuestions:[String] = ["What is ORI?" ,
                                 "Why use ORI?" ,
                                 "Do I have to pay for booking a service through ORI?" ,
                                 "Who repairs your appliances when you book with ORI?" ,
                                 "Why get it repaired from the brand’s authorized service" ,
                                 "What if my appliance is under warranty? What if it’s not?" ,
                                 "How do I book a service?" ,
                                 "What is ‘My Bookings’?" ,
                                 "How do I track an existing service request made via ORI?" ,
                                 "How do I track an existing service request made via a call center?" ,
                                 "What is ‘Escalate Request’?" ,
                                 "Where can I see all my devices?" ,
                                 "How do I add a new device?" ,
                                 "How many devices can I save?" ,
                                 "Where do I see my saved addresses?" ,
                                 "Do I need an ORI account to use the app?" ,
                                 "Did we miss anything?"]
    
    var faqAnswers:[String] = ["ORI is the fastest platform to book after-sales brand-authorised services for appliances and gadgets.\t" ,
                               "Repair, Maintenance, Installation, Device Registration and Warranty Activation – you name it and we’ll help you book a request for it in under a minute." ,
                               "Booking a service through ORI is completely free of cost. No hidden terms and conditions!" ,
                               "The technicians are from the same brand you bought your appliances and devices from." ,
                               "Besides the fact that the brand technicians know your appliances better than anyone else, you can avail brand warranty and original spare parts with 100% assurance against tampering." ,
                               "You can book services using the app for your appliances both in-warranty as well as out-of-warranty. If it’s within the warranty period, then you pay nothing (based on what is covered under warranty), while standard rates are applicable for out-of-warranty services." ,
                               "On your home-screen, click on the bottom right button ‘Request Service’. Choose from the three options – Repair, Maintenance, Installation.\n\n1. Repair\n Choose from the list of devices already added to your account. You can also add a new device.\n Proceed to provide the state of your warranty. Enter a little about your problem.\n Select from the list of addresses already added to your account. You can also add a new address.\n Set date and time. Press ‘Confirm’ to successfully book a repair request.\n\n 2. Maintenance/Installation\n Choose from the list of devices already added to your account. You can also add a new device.\n Select from the list of addresses already added to your account. You can also add a new address.\n Set date and time. Press ‘Confirm’ to successfully book a maintenance or an installation request." ,
        "‘My Bookings’ section lists all your service requests. It is divided into two sections – In Progress and Completed. Under ‘In Progress’, you can track your pending service requests. Once ‘Confirmed’, you will be able to directly access information about the service technician who will be visiting you." ,
        "Swipe on any screen to reveal the left hand side menu. Select ‘My Bookings’. Tap on the relevant service request card under ‘In Progress’ to track its progress." ,
        "For services requested via a call center, click on the ‘Track Booking’ button on the Home page. Fill in the details about your device, brand and complaint ID to instantly access all the important details of your booking." ,
        "Every brand commits to a certain number of days in which your service request will be completed (Turnaround time). In a case where a brand cannot meet the Turnaround time, you have the option to escalate the service request (i.e. voice your displeasure). The ‘Escalate Request’ button allows you to raise your concern with the highest authority within the brand. Please note that the ‘Escalate Request’ button is only activated if the brand does not finish the service within its committed Turnaround time." ,
        "Tap on ‘My Devices’ on your home-screen to see all the devices you have saved with us." ,
        "In two easy steps! Start by clicking on the blue plus sign in the top right section your home-screen.\n\n Fill in the necessary device details like category and brand.\n\n Press Next. Provide the bill and warranty information. Securely store your invoices with us on the cloud.\n\n Press ‘Add and Proceed’ to successfully add devices to your account." ,
        "As many as you\'d like to! Save all your devices for total bliss." ,
        "Swipe on any screen to reveal the left hand side menu. Tap on your name at the top of the panel and click on ‘View Saved Addresses’ to view all the addresses saved to your account. You can also add an address from here." ,
        "Yes. Think of the ORI account as the keys to your house.  But it’s really easy - we only need your name, phone number and email address! We’ll send you an OTP to confirm your phone number and then you’re all set to unlock a world of after-sales benefits for all your appliances and gadgets." ,
        "Call us! Swipe on any screen to reveal the left hand side menu and hit the ‘Call’ button under the ‘Help and Support’ section. You can also enter your feedback directly from the app by swiping right and selecting ‘Feedback’. You can even drop us an e-mail at ori@oriserve.com."]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "FaqQuestionLayout", bundle: nil) , forCellReuseIdentifier: "FaqCell")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.faqQuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FaqCell", for: indexPath) as! FaqCell
        cell.answerHolder.superview?.sendSubview(toBack: cell.answerHolder)
        if indexPath.item == selectedQuestion {
            cell.answerHolder.isHidden = false
        } else {
            cell.answerHolder.isHidden = true
        }
        cell.question.text = faqQuestions[indexPath.item]
        cell.answer.text = faqAnswers[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedQuestion == indexPath.item {
            selectedQuestion = -1
        } else {
            self.selectedQuestion = indexPath.item
        }
        self.tableView.reloadData()
        
        let visibleCell = tableView.visibleCells[tableView.visibleCells.count-1] as! FaqCell
//        OriUtil.oriLog("last cell ",visibleCell.question.text!)
        let cell = tableView.cellForRow(at: indexPath) as! FaqCell
        if selectedQuestion != -1 && visibleCell == cell {
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

class FaqCell : UITableViewCell {
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var answer: UILabel!
    @IBOutlet weak var answerHolder: UIView!
    
}


