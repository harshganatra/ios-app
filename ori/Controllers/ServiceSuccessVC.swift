//
//  ServiceSuccessVC.swift
//  ori
//
//  Created by Harsh Ganatra on 10/01/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit

class ServiceSuccessVC: UIViewController {

    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var successText: UILabel!
    @IBOutlet weak var cardDetailsHolder: UIView!
    @IBOutlet weak var complaintNumber: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    
    var amcSuccess = false
    var myCard = MyCard()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DownloadAndReadImage().renderLocalImage(imageName: myCard.brandLogoName, imageView: self.brandImage, folderName: Values.amazonBrandsFolder)
        
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.clear
        imageView.frame = CGRect(x:0,y:0,width:2*(self.navigationController?.navigationBar.bounds.height)!,height:(self.navigationController?.navigationBar.bounds.height)!)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButton(sender:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        imageView.tag = 13
        self.navigationController?.navigationBar.addSubview(imageView)
        
        let name = LoginHelper.getCurrentUser().1.firstName
        if amcSuccess {
            successText.text = name+", your request for the AMC purchase of your "+myCard.listName+" has been registered. "+"We'll contact you shortly to assist with payment and queries."

        } else { // Card success
            successText.text = name+", you have successfully requested service for your "+myCard.listName+". "+myCard.brandName+" will confirm your booking and get back to you soon!"
        }
        
        if myCard.complaintNumber != "" {
            self.cardDetailsHolder.isHidden = false
            self.complaintNumber.text = myCard.complaintNumber
            self.cardNumber.text = myCard.cardNumber
        }
        
        let defaults = UserDefaults.standard
        if !defaults.bool(forKey: Values.card_popup_shown) {
            let msg = "We'll notify you every time your service status is updated\n\nYour "+myCard.listName+" details will also be updated by "+myCard.brandName+" at the end of service. Hold on for that"
            let alertView = UIAlertController(title: "STRESS-FREE TRACKING", message: msg, preferredStyle: UIAlertControllerStyle.alert)
            alertView.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alertAction) -> Void in
                defaults.set(true, forKey: Values.card_popup_shown)
            }))
            alertView.presentInOwnWindow()
        }
    }
    
    @objc func backButton(sender: UIButton!) {
        self.leavePage()
    }

    @IBAction func goToHome(_ sender: Any) {
        self.leavePage()
    }
    
    @IBAction func trackBooking(_ sender: Any) {
        if let navController = self.navigationController {
            for controller in navController.viewControllers as Array {
                if controller.isKind(of: HomeController.self) {
                    let homeVC = controller as! HomeController
                    homeVC.goToPage = .mycards
                    homeVC.animatePages = false
                }
            }
        }
        self.leavePage(false)
    }
    
    func leavePage(_ animated:Bool = true) {
        for view in (navigationController?.navigationBar.subviews)!{ //remove the view
            if view.tag == 13 {
                view.removeFromSuperview()
            }
        }
        self.navigationController?.popToRootViewController(animated: animated)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
