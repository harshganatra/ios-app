//
//  AMCDetailsVC.swift
//  ori
//
//  Created by Harsh Ganatra on 09/03/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit

class AMCDetailsVC: UIViewController {

    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var cardTitle: UILabel!
    @IBOutlet weak var cardSubtitle: UILabel!
    @IBOutlet weak var amcNo: UILabel!
    @IBOutlet weak var cardNo: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var rupeeSymbol: UIImageView!
    
    weak var delegate:ReturnProtocol?
    var myCard:MyCard = MyCard()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DownloadAndReadImage().renderLocalImage(imageName: myCard.brandLogoName, imageView: self.brandImage, folderName: Values.amazonBrandsFolder)
        self.cardTitle.text = myCard.title
        self.cardSubtitle.text = myCard.subTitle
        self.amcNo.text = myCard.complaintNumber
        self.cardNo.text = myCard.cardNumber
        self.status.text = myCard.complaintStatusDetails
        self.price.text = myCard.amcPlan.amcPrice
        
        rupeeSymbol.image = rupeeSymbol.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        rupeeSymbol.tintColor = #colorLiteral(red: 1, green: 0, blue: 0.3037448227, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
