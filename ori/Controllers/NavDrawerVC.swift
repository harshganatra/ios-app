//
//  NavDrawerVC.swift
//  ori
//
//  Created by Harsh Ganatra on 12/10/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import UIKit

class NavDrawerVC: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var userName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if LoginHelper.isLoggedIn() {
            loginButton.isHidden = true
            logoutButton.isHidden = false
            userName.text = LoginHelper.getCurrentUser().1.name
        } else {
            logoutButton.isHidden = true
            loginButton.isHidden = false
            userName.text = ""
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "closeDrawer") {
            
            let navigation: UINavigationController = segue.destination as! UINavigationController
            
            let vc = navigation.viewControllers[0] as! HomeController
            let openPage = sender as! HomeController.toPage
            vc.animatePages = false
            vc.goToPage = openPage
            
        }
    }
    
    // call this function to close the drawer and keep the original VC
    // (perform a segue instead of this to initialize new HOMEVC)
    func closeDrawer(_ openPage: HomeController.toPage) {
        let vc = self.revealViewController().frontViewController as! UINavigationController
        (vc.viewControllers[0] as! HomeController).openPage(openPage)
        self.revealViewController().pushFrontViewController(vc, animated: true)
    }
    
    @IBAction func loginFunc(_ sender: Any) {
        self.closeDrawer(HomeController.toPage.login)
    }
    
    @IBAction func logoutFunc(_ sender: Any) {
        let vc = self.revealViewController().frontViewController as! UINavigationController
        (vc.viewControllers[0] as! HomeController).openPage(.logout)
        self.revealViewController().pushFrontViewController(vc, animated: true)
//        self.closeDrawer(HomeController.toPage.logout)
    }
    
    @IBAction func rateButton(_ sender: Any) {
        
        self.closeDrawer(HomeController.toPage.rate)
    }
    
    @IBAction func shareButton(_ sender: Any) {
        
        let textToShare = "Hi, download the fastest way to avail complete care for all your appliances and gadgets! Get the ORI app for Android and iOS at https://goo.gl/rlu6rt today!"

        let objectsToShare = [textToShare] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
        
        self.closeDrawer(HomeController.toPage.share)
    }
    
    @IBAction func feedbackButton(_ sender: Any) {
        self.closeDrawer(HomeController.toPage.feedback)
    }
    
    @IBAction func helpButton(_ sender: Any) {
        self.closeDrawer(HomeController.toPage.help)
    }
    
    @IBAction func trackButton(_ sender: Any) {
        self.closeDrawer(HomeController.toPage.mycards)
    }
    
    @IBAction func myDevicesButton(_ sender: Any) {
        self.closeDrawer(HomeController.toPage.mydevices)
    }
    
    @IBAction func homeButton(_ sender: Any) {
        self.closeDrawer(HomeController.toPage.none)
    }
    
    @IBAction func accountButton(_ sender: Any) {
        if LoginHelper.isLoggedIn() { //button shouldnt perform any action when logged out
            self.closeDrawer(HomeController.toPage.account)
        }
    }

}
