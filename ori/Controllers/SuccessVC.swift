//
//  SuccessVC.swift
//  ori
//
//  Created by Harsh Ganatra on 09/01/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit

class SuccessVC: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    var myDevice = MyDevice()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.clear
        imageView.frame = CGRect(x:0,y:0,width:2*(self.navigationController?.navigationBar.bounds.height)!,height:(self.navigationController?.navigationBar.bounds.height)!)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backButton(sender:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        imageView.tag = 13
        self.navigationController?.navigationBar.addSubview(imageView)
        
        let name = (myDevice.category == ("") ? myDevice.listName : myDevice.category);
        label.text = "Congratulations! Your "+(myDevice.brandName)+" "+name+" is now in safe hands."
    }
    
    @objc func backButton(sender: UIButton!) {
        self.leavePage()
    }
    
    
    @IBAction func addDevice(_ sender: Any) {
        self.leavePage(animated:false, goTo: .adddevice)
    }
    
    func leavePage(animated:Bool = true, goTo:HomeController.toPage = .none, myDevice:MyDevice = MyDevice()) {
        if goTo != .none {
            if let navController = self.navigationController {
                for controller in navController.viewControllers as Array {
                    if controller.isKind(of: HomeController.self) {
                        let homeVC = controller as! HomeController
                        homeVC.goToPage = goTo
                        homeVC.animatePages = animated
                        homeVC.myDevice = myDevice
                    }
                }
            }
        }
        
        for view in (navigationController?.navigationBar.subviews)!{ //remove the view
            if view.tag == 13 {
                view.removeFromSuperview()
            }
        }
        self.navigationController?.popToRootViewController(animated: animated)
    }
    
    
    @IBOutlet weak var reqService: UIBarButtonItem!
    @IBAction func reqService(_ sender: Any) {
        
        let pickerController = UIAlertController()
        pickerController.title = "Request Service:"
        
        let repairAction = UIAlertAction(title:"Repair", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            self.leavePage(animated:false, goTo: .repair, myDevice: self.myDevice)
        }
        var repair = UIImage(named: "repair")
        repair = repair?.scaled(to: CGSize(width: 35, height: 35))
        repairAction.setValue(repair, forKey: "image")
        
        let maintAction = UIAlertAction(title:"Maintenance", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            self.leavePage(animated:false, goTo: .maintenance, myDevice: self.myDevice)
        }
        var image = UIImage(named: "maintenance")
        image = image?.scaled(to: CGSize(width: 35, height: 35))
        maintAction.setValue(image, forKey: "image")
        
        let installAction = UIAlertAction(title:"Installation", style: UIAlertActionStyle.default) { action in
            pickerController.dismiss(animated: true, completion: nil)
            self.leavePage(animated:false, goTo: .installation, myDevice: self.myDevice)
        }
        var image2 = UIImage(named: "installation")
        image2 = image2?.scaled(to: CGSize(width: 35, height: 35))
        installAction.setValue(image2, forKey: "image")
        
        let cancel = UIAlertAction(title:"Close", style: UIAlertActionStyle.cancel) { action in
            pickerController.dismiss(animated: true, completion: nil)
        }
        
        pickerController.addAction(repairAction)
        pickerController.addAction(maintAction)
        pickerController.addAction(installAction)
        pickerController.addAction(cancel)
        
        pickerController.popoverPresentationController?.barButtonItem = self.reqService // works for both iPhone & iPad
        pickerController.view.tintColor = #colorLiteral(red: 0.4401171207, green: 0.4401280284, blue: 0.4401221871, alpha: 1)
        
        self.present(pickerController, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
