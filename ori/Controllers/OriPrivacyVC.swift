//
//  OriPrivacyVC.swift
//  ori
//
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit
import WebKit

class OriPrivacyVC: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.makeToastActivity(.center)
        
        webView.navigationDelegate = self
        
        let str = "<html><body style=\"text-align:justify;color:#5d5d5d;font-size:48px;background-color:#FAFAFA; \">"+"<p>Ori is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website including in particular information which identifies you personally and other information that is collected from visitors and users of the Services, then you can be assured that it will only be used in accordance with this privacy statement and our terms and conditions.</p> <p>Please read this privacy policy carefully so that you understand how we will treat your information. By using any of our Services, you confirm that you have read, understood and agree to this privacy policy. If you do not agree to this policy, please do not use any of the Services. If you have any queries, please email us at ori@oriserve.com</p> <h2>WHO ARE WE?</h2> <p>Ori is a smart home management system to take care of all your consumer appliances post purchase. Ori lets you to request installation, demo, in-warranty services, free services, AMC, Extended warranty & Out of warranty service directly from brand authorized service centers</p> <h2>WHAT DO WE COLLECT?</h2> <p>We collect Personal Data from you when you voluntarily provide such information, such as when you contact us with inquiries, respond to one of our surveys, register for access to the Services or use certain Services, which typically includes your:</p> <ul> <li>Name</li> <li>Contact information including email address</li> <li>Demographic information such as postcode, address, preferences and interests</li> <li>Your address book, etc</li> <li>Your device details</li> <li>Your device purchase details</li> <li>Other information relevant to customer surveys and/or offers</li> </ul> <p>What we do with the information we gather?</p> <p>We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:</p> <ul> <li>To implement and monitor any Ori bookings which you might make</li> <li>To share your Personal Data with Ori professionals in order to carry out your bookings</li> <li>To provide you with information, products or services that you request from us or which we feel may interest you</li> <li>To notify you about changes to our services.</li> <li>For marketing purposes</li> </ul> <h2>Links to other websites</h2> <ul> <li>Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. You should exercise caution and look at the privacy statement applicable to the website in question.</li> <li>We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.</li> <li>You may request details of personal information which we hold about you under the Data Protection Act 1998. A small fee will be payable. If you would like a copy of the information held on you please write to ori@oriserve.com</li> <li>If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible, at the above address. We will promptly correct any information found to be incorrect.</li> "+"</body></html>"
        
        webView.loadHTMLString(str, baseURL: nil)
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if webView.isLoading {
            return
        }
        self.view.hideToastActivity()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
