//
//  NoInternetVC.swift
//  ori
//
//  Created by Harsh Ganatra on 26/02/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import UIKit

class NoInternetVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func tryAgain(_ sender: Any) {
        if OriNetworking.isConnected(view: self.view) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func settings(_ sender: Any) {
//        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
//            return
//        }
//
//        if UIApplication.shared.canOpenURL(settingsUrl) {
//            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                OriUtil.oriLog("Settings opened: \(success)") // Prints true
//            })
//        }
        UIApplication.shared.open(URL(string:"App-Prefs:root=General")!, options: [:], completionHandler: nil)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
