//
//  OriNetworking.swift
//  ori
//
//  Created by Harsh Ganatra on 09/10/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class OriNetworking {
    
    public enum requestType: String {
//        case options = "OPTIONS"
        case get     = "GET"
//        case head    = "HEAD"
        case post    = "POST"
        case put     = "PUT"
//        case patch   = "PATCH"
        case delete  = "DELETE"
//        case trace   = "TRACE"
//        case connect = "CONNECT"
    }
    
    public enum authType: Int {
        case send_auth = 0
        case default_auth     = 1
        case no_auth    = 2
    }
    
    typealias CompletionHandler = (_:Bool, _:Error?, _:JSON) -> Void
    
    public static func isConnected(view:UIView? = nil) -> Bool {
        if let isConnected = NetworkReachabilityManager()?.isReachable, !isConnected {
            //cannot connect to internet
            if view != nil {
//                OriUtil.showToast(view: view!, msg: "No internet connection")
                OriUtil.noInternetToast(view: view!)
            }
            OriUtil.oriLog("Error.: No Internet Connection")
            return false
        } else {
            return true
        }
    }
    
    public static func makeRequest(requestType:requestType , url:String, parameters:Parameters? = nil,
                                   auth: authType = .send_auth, view:UIView? = nil, completionHandler: @escaping CompletionHandler){
        
        if !isConnected(view: view) {
            // no internet
            completionHandler(false, nil, JSON.null)
            return
        }
        
        var headers:Dictionary<String,String>
        switch auth {
        case .no_auth:
            headers = ["Authorization": "Bearer "+""]
        case .default_auth:
            headers = ["Authorization": "Bearer "+Values.default_auth_token]
        case .send_auth:
            let isLoggedIn = LoginHelper.getCurrentUser()
            if isLoggedIn.0 {
                let user = isLoggedIn.1
                headers = ["Authorization": "Bearer "+user.authToken]
            } else {
                headers = ["Authorization": "Bearer "+""]
            }
        }
        OriUtil.oriLog("calling url \(url)")
        
        switch requestType {
        case .get:
            request(url, method: .get, headers: headers).responseJSON { response in
                let result = parseResponse(response: response, view: view)
                completionHandler(result.status, result.error, result.response)
            }
        case .put:
            OriUtil.oriLog(parameters as Any)
            request(url, method: .put, parameters:parameters, encoding:JSONEncoding.default, headers: headers).responseJSON { response in
                let result = parseResponse(response: response, view: view)
                completionHandler(result.status, result.error, result.response)
            }
        case .post:
            OriUtil.oriLog(parameters as Any)
            request(url, method: .post, parameters:parameters, encoding:JSONEncoding.default, headers: headers).responseJSON { response in
                let result = parseResponse(response: response, view: view)
                completionHandler(result.status, result.error, result.response)
            }
        case .delete:
            OriUtil.oriLog(parameters as Any)
            request(url, method: .delete, parameters:parameters, encoding:JSONEncoding.default, headers: headers).responseJSON { response in
                let result = parseResponse(response: response, view: view)
                completionHandler(result.status, result.error, result.response)
            }
//        default:
//            OriUtil.oriLog("Request type not supported")
        }
    
    }

    public static func parseResponse(response:DataResponse<Any>, view:UIView? = nil) -> (status:Bool, error:Error?, response:JSON){
//        OriUtil.oriLog(response)
        switch response.result {
        case .success(let value):
            let json = JSON(value)
//            OriUtil.oriLog("JSON: json")
            return (true, nil, json)
        case .failure(let error):
            if view != nil {
//                OriUtil.showToast(view: view!, msg: "No internet connection")
                OriUtil.noInternetToast(view: view!)

            }
            OriUtil.oriLog("Error.: \(error)")
            return (false, error, JSON.null)
        }
    }
    
    
}

