//
//  Values.swift
//  ori
//
//  Created by Harsh Ganatra on 09/10/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation
class Values {
    
    /***For Share Prefs*****/
    public static let LAUNCHED = "launched"
    public static let VERSION = "version_code"
    public static let TOKEN_KEY = "token"
    public static let USERID_KEY = "userid"
    public static let EMAIL_KEY = "email"
    public static let FIRST_NAME_KEY = "fname"
    public static let LAST_NAME_KEY = "lname"
    public static let GENDER_KEY = "gender"
    public static let ISO_DOB_KEY = "dob"
    public static let MOBILE_KEY = "mobile"
    public static let card_popup_shown = "card_notification_hint_dialog"
    
    public static let repair = "Repair"
    public static let maint = "Maintenance"
    public static let installation = "Installation"
    
    public static let address_key = "address"
    public static let myConnection = "myConnection"
    
    //amazon URLs
    public static let amazonURL = "https://s3-ap-southeast-1.amazonaws.com/oriapp/"; //main folder with uploaded images
    public static let amazonCategoriesFolder = "usw/"; //category images
    public static let amazonBrandsFolder = "brand_logos/"; //brand images
    
    public static let brand_version_no = "BrandVersion"
    public static let categories_version_no = "CategoriesVersion"
    
    public static let defaultDate = "0001-01-01T00:00:00Z"
    
    //auth token for a new clean id generated from backend (account phone number 1111111111).
    // Used in cases where the data needs to be fetched from the server when the user is not logged in
    public static let default_auth_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJ1c2VycyIsImp0aSI6IjU4ZDI1MTliZGJjYmJhMmQ1NDMzYjUyZiIsImlzcyI6Im9yaXNlcnZlLmNvbSIsInN1YiI6IjU4ZDI1MTliZGJjYmJhMmQ1NDMzYjUzMCJ9.SnhqRG4ReFOWqiCEiEMubwfbHdohN9ZjtdsRs9Exo7o"

}


