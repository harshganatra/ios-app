//
//  Validation.swift
//  ori
//
//  Created by Harsh Ganatra on 12/12/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation

class Validation {
    private static let EMAIL_REGEX:String = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    private static let PHONE_REGEX:String = "[789]\\d{9}"
    
    static func isValidPhoneNumber(textField: UITextField, required: Bool = false) -> Bool {
        let text = textField.text
        if let str=text {
            if str == "" && required {
                return false
            }
            let r = str.startIndex..<str.endIndex
            let r2 = str.range(of: PHONE_REGEX, options: .regularExpression)
            if r2 == r {
                return true
            }
        }
        return false
    }
    
    static func isValidEmail(textField: UITextField, required: Bool = false) -> Bool {
        let text = textField.text
        if var str=text {
            str = str.trimmingCharacters(in: .whitespacesAndNewlines)
            if str == "" && required {
                return false
            }
            let r = str.startIndex..<str.endIndex
            let r2 = str.range(of: EMAIL_REGEX, options: .regularExpression)
            if r2 == r {
                return true
            }
        }
        return false
    }
    
    static func hasText(textField: UITextField, required: Bool = false) -> Bool {
        let text = textField.text
        if var str=text {
            str = str.trimmingCharacters(in: .whitespacesAndNewlines)
            if str == "" && required {
                return false
            }
            return true
        }
        return false
    }
}
