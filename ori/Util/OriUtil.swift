//
//  OriUtil.swift
//  ori
//
//  Created by Harsh Ganatra on 11/10/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation
import SwiftyJSON
import Toast_Swift

class OriUtil {
    public static func oriLog(_ msg:Any...) {
//        print(msg)
    }
    
    static let ISOFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    static let dateFormat = "dd-MM-yyyy"
    static let updatedFormat = "dd-MM-yyyy  hh:mm\u{00A0aa}"
    static let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
    
    static func getDateStringFromISO(_ ISOdate:String) -> String {
        if ISOdate == "" {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = dateFormat
        
        return dateFormatter.string(from: dateFromISO(ISOdate))
    }
    
    static func getISO8601StringForCurrentDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = ISOFormat
        
        return dateFormatter.string(from: Date())
    }
    
    static func getDateTimeStamp() -> String {
        let format = "yyyyMMdd_HHmmss"

        let dateFormatter = DateFormatter()
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: Date())
    }
    
    static func getISO8601StringForDate(_ date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = ISOFormat
        
        return dateFormatter.string(from: date)
    }
    
    static func getISO8601StringForDate(_ date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = ISOFormat
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.locale = enUSPosixLocale
        dateFormatter2.dateFormat = dateFormat
        
        return dateFormatter.string(from: dateFormatter2.date(from: date)!)
    }
    
    static func updatedAtString(_ ISODate:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = updatedFormat
        
        return dateFormatter.string(from: dateFromISO(ISODate))
    }
    
    static func dateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: date)
    }
    
    static func dateFromISO(_ ISODate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = ISOFormat
        
        let length:Int = "yyyy-MM-ddTHH:mm:ss".count
        let indexUpto = ISODate.index(ISODate.startIndex, offsetBy: length)
        let date:String = ISODate[..<indexUpto]+"Z"
        
        return dateFormatter.date(from: date)!
    }
    
    // view passed is always self.view
    static func showToast(view:UIView, msg:String) {
        cancelToast(view: view)
        view.makeToast(msg)
    }
    
    // view passed is always self.view
    static func noInternetToast(view:UIView) {
        cancelToast(view: view)
        var style = ToastStyle()
        style.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 0.8)
        
        let point = CGPoint(x: view.bounds.size.width / 2.0,y: view.bounds.size.height / 5.0)

        view.makeToast("No internet connection", point: point, title: nil, image: nil, style: style, completion: nil)
        
    }
    
    static func showErrorToast(view:UIView) {
        cancelToast(view: view)
        view.makeToast("Something went wrong. Kindly retry.")
    }
    
    static func cancelToast(view:UIView) {
        view.hideToast()
    }
    
    static func checkErrorFromJSON(view:UIView, json:JSON) {
        if json["data"].exists() && json["data"].string != nil {
            showToast(view: view, msg: json["data"].stringValue)
        } else {
            showErrorToast(view: view)
        }
    }
    
    static func returnSHA(data: String, key: String) -> String {
        let cKey = key.cString(using: String.Encoding.utf8)
        let cData = data.cString(using: String.Encoding.utf8)
        var result = [CUnsignedChar](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA512), cKey!, strlen(cKey!), cData!, strlen(cData!), &result)
        let hmacData:NSData = NSData(bytes: result, length: (Int(CC_SHA512_DIGEST_LENGTH)))
        //        var hmacBase64 = hmacData.base64EncodedStringWithOptions(NSData.Base64EncodingOptions.Encoding76CharacterLineLength)
        let hmacBase64 = hmacData.base64EncodedString()
        return String(hmacBase64)
    }
    
    static func setButton(selected:Bool , button:UIButton!) {
        if selected {
            button.layer.borderWidth = 1.0
            button.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0.3037448227, alpha: 1)
            button.backgroundColor = UIColor.white
        } else {
            button.layer.borderWidth = 0.0
            button.backgroundColor = #colorLiteral(red: 0.9593952298, green: 0.9594177604, blue: 0.959405601, alpha: 1)
        }
    }
    
    static func saveLocalAddress(_ address:Address) {
        let defaults = UserDefaults.standard
        defaults.set(address._id, forKey: "ADD_ID")
        defaults.set(address.addressLine1, forKey: "ADD_ADDRESS1")
        defaults.set(address.addressLine2, forKey: "ADD_ADDRESS2")
        defaults.set(address.city, forKey: "ADD_CITY")
        defaults.set(address.landmark, forKey: "ADD_LANDMARK")
        defaults.set(address.state, forKey: "ADD_STATE")
        defaults.set(address.zipcode, forKey: "ADD_ZIP")
        defaults.set(address.nickName, forKey: "ADD_NICKNAME")
        defaults.set(address.isDefault, forKey: "ADD_DEFAULT")
    }
    
    static func getLocalAddress() -> Address {
        let defaults = UserDefaults.standard
        let address_id = defaults.string(forKey: "ADD_ID")
        if address_id != nil && address_id != "" {
            var address = Address()
            address._id = address_id!
            address.addressLine1 = defaults.string(forKey: "ADD_ADDRESS1")!
            address.addressLine2 = defaults.string(forKey: "ADD_ADDRESS2")!
            address.city = defaults.string(forKey: "ADD_CITY")!
            address.landmark = defaults.string(forKey: "ADD_LANDMARK")!
            address.state = defaults.string(forKey: "ADD_STATE")!
            address.zipcode = defaults.string(forKey: "ADD_ZIP")!
            address.nickName = defaults.string(forKey: "ADD_NICKNAME")!
            address.isDefault = defaults.bool(forKey: "ADD_DEFAULT")
            return address
        }
        return Address()
    }
    
    static func deleteLocalAddress() {
        saveLocalAddress(Address()) //setting blank address
    }
    
    static func parseSingleDeviceJSON(json:JSON) -> MyDevice {
        var myDevice:MyDevice = MyDevice()
        myDevice.categorySlug = json["categorySlug"].stringValue
        myDevice.brandId = json["brandId"].intValue
        myDevice._id = json["_id"].stringValue
        myDevice.userId = json["userId"].stringValue
        myDevice.createdAt = json["createdAt"].stringValue
        myDevice.modifiedAt = json["modifiedAt"].stringValue
        if json["warrantyStatus"].exists(){ myDevice.warrantyStatus = json["warrantyStatus"].stringValue }
        if json["warrantyPeriod"].exists(){ myDevice.warrantyPeriod = json["warrantyPeriod"].stringValue }
        if json["model"].exists(){ myDevice.modelNo = json["model"].stringValue }
        if json["serialNumber"].exists(){ myDevice.serialNumber = json["serialNumber"].stringValue }
        if json["dateOfPurchase"].exists(){ myDevice.dateOfPurchase = json["dateOfPurchase"].stringValue }
        if json["deviceTag"].exists(){
            myDevice.deviceTag = json["deviceTag"].stringValue
            myDevice.deviceTag = myDevice.deviceTag.replacingOccurrences(of: "#", with: "")
        }
        if json["deviceImg"].exists(){
            myDevice.deviceImage = json["deviceImg"].stringValue
            if myDevice.deviceImage == "NOIMG" || myDevice.deviceImage == " " {
                myDevice.deviceImage = ""
            }
        }
        if json["invoiceFile"].exists(){ myDevice.invoiceFile = json["invoiceFile"].stringValue
            if myDevice.invoiceFile == "NOIMG" || myDevice.invoiceFile == " " {
                myDevice.invoiceFile = ""
            }
        }
        if json["warrantyFile"].exists(){ myDevice.warrantyFile = json["warrantyFile"].stringValue
            if myDevice.warrantyFile == "NOIMG" || myDevice.warrantyFile == " " { //space is put to delete file on server
                myDevice.warrantyFile = ""
            }
        }
        if json["category"].exists(){ myDevice.category = json["category"].stringValue }
        if json["brandVerified"].exists(){ myDevice.vidBrandVerified = json["brandVerified"].boolValue }
        if json["retailer"].exists(){ myDevice.retailerName = json["retailer"].stringValue }
        
        let c:OriCategory = (try! CategoryDataHelper.find(slug:myDevice.categorySlug))!
        myDevice.listName = c.listName
        myDevice.defaultDeviceImage = c.imageName
        myDevice.applianceOrConnection = c.category
        myDevice.categorySearchArray = c.searchArray
        if c.amcAvailable == "1" {
            var brandIds = c.AMCBrandIDS
            brandIds = brandIds.replacingOccurrences(of: "[", with: "")
            brandIds = brandIds.replacingOccurrences(of: "]", with: "")
            brandIds = brandIds.replacingOccurrences(of: " ", with: "")
            let numbers = brandIds.split(separator: ",")
            if numbers.contains(String.SubSequence( String(myDevice.brandId))) {
                myDevice.amcAvailable = true
            }
        }
        
        let b = (try! BrandDataHelper.find(id:myDevice.brandId))!
        myDevice.brandName = b.brandName
        myDevice.isOnboarded = b.isOnboarded
        myDevice.brandPhone = b.serviceNumber
        myDevice.brandLogoName = b.brandLogoName
        myDevice.brandSearchArray = b.androidBrandArray
        
        return myDevice
    }
    
    static func parseSingleConnectionJSON(json:JSON) -> MyConnection {
        var myConnection:MyConnection = MyConnection()
        myConnection.categorySlug = json["categorySlug"].stringValue
        myConnection.brandId = json["brandId"].intValue
        myConnection._id = json["_id"].stringValue
        
        let c:OriCategory = (try! CategoryDataHelper.find(slug:myConnection.categorySlug))!
        myConnection.listName = c.listName
        myConnection.defaultDeviceImage = c.imageName
        myConnection.applianceOrConnection = c.category
        myConnection.categorySearchArray = c.searchArray
        
        let b = (try! BrandDataHelper.find(id:myConnection.brandId))!
        myConnection.brandName = b.brandName
        myConnection.brandPhone = b.serviceNumber
        myConnection.brandLogoName = b.brandLogoName
        myConnection.brandSearchArray = b.androidBrandArray
        
        if json["RMN"].exists(){ myConnection.RMN = json["RMN"].stringValue }
        if json["vcNumber"].exists(){ myConnection.VCNo = json["vcNumber"].stringValue }
        if json["dateOfPurchase"].exists(){ myConnection.dateOfPurchase = json["dateOfPurchase"].stringValue }
        if json["retailer"].exists(){ myConnection.retailer = json["retailer"].stringValue }
        if json["invoiceFile"].exists(){ myConnection.invoiceFile = json["invoiceFile"].stringValue }
        if json["deviceTag"].exists(){
            myConnection.deviceTag = json["deviceTag"].stringValue
            myConnection.deviceTag = myConnection.deviceTag.replacingOccurrences(of: "#", with: "")
        }
        if json["schemeCode"].exists(){ myConnection.schemeCode = json["schemeCode"].stringValue }
        if json["zoneCode"].exists(){ myConnection.zoneCode = json["serialNumber"].stringValue }
        if json["monthlySubscriptionAmount"].exists(){ myConnection.monthlySubscriptionAmount = json["deviceImg"].stringValue }
        if json["packPeriod"].exists(){ myConnection.packPeriod = json["packPeriod"].stringValue }
        if json["SMSID"].exists(){ myConnection.SMSID = json["SMSID"].stringValue }
        if json["balance"].exists(){ myConnection.balance = json["balance"].stringValue }
        if json["userName"].exists(){ myConnection.userName = json["userName"].stringValue }
        if json["category"].exists(){ myConnection.category = json["category"].stringValue }
        if json["emailID"].exists(){ myConnection.emailId = json["emailID"].stringValue }
        if json["status"].exists(){ myConnection.status = json["status"].stringValue }
        if json["subCategory"].exists(){ myConnection.subCategory = json["subCategory"].stringValue }
        if json["verifiedFlag"].exists(){ myConnection.verified = json["verifiedFlag"].boolValue }
        if json["switchOffDate"].exists(){
            let date = json["switchOffDate"].stringValue
            if date != Values.defaultDate {
                myConnection.switchOffDate = getDateStringFromISO(date)
            }
        }
        if json["lastRechargeDate"].exists(){
            let date = json["lastRechargeDate"].stringValue
            if date != Values.defaultDate {
                myConnection.lastRechargeDate = getDateStringFromISO(date)
            }
        }
        if json["basePack"].exists(){
            myConnection.basePackName = json["basePack"]["name"].stringValue
            myConnection.basePackPrice = json["basePack"]["price"].stringValue
        }
        
        let array = json["addOnPacks"].arrayValue
        var addOnPacks:[DishPack] = []
        for a in array {
            addOnPacks.append(DishPack(packName:a["name"].stringValue, price:a["name"].stringValue))
        }
        myConnection.addOnPacks = addOnPacks
        
        let array2 = json["otherCharges"].arrayValue
        var otherCharges:[DishPack] = []
        for a in array2 {
            otherCharges.append(DishPack(packName:a["name"].stringValue, price:a["name"].stringValue))
        }
        myConnection.otherCharges = otherCharges
        
        let array3 = json["childVCs"].arrayValue
        var childVCs:[DishChildVC] = []
        for a in array3 {
            var childVC = DishChildVC()
            childVC.VCNo = a["vcNumber"].stringValue
            if a["SMSID"].exists(){ childVC.SMSID = a["SMSID"].stringValue }
            if a["balance"].exists(){ childVC.balance = a["balance"].stringValue }
            if a["RMN"].exists(){ childVC.RMN = a["RMN"].stringValue }
            if a["emailID"].exists(){ childVC.emailId = a["emailID"].stringValue }
            if a["basePack"].exists(){
                childVC.basePackName = a["basePack"]["name"].stringValue
                childVC.basePackPrice = a["basePack"]["price"].stringValue
            }
            let array = a["addOnPacks"].arrayValue
            var addOnPacks2:[DishPack] = []
            for a2 in array {
                addOnPacks2.append(DishPack(packName:a2["name"].stringValue, price:a2["name"].stringValue))
            }
            childVC.addOnPacks = addOnPacks2
            
            childVCs.append(childVC)
        }
        myConnection.childVCs = childVCs
        
        return myConnection
    }
    
    static func parseSingleCardJSON(json:JSON) -> MyCard {
        var myCard:MyCard = MyCard()
        myCard.categorySlug = json["categorySlug"].stringValue
        myCard.brandId = json["brandId"].intValue

        let c:OriCategory = (try! CategoryDataHelper.find(slug:myCard.categorySlug))!
        myCard.listName = c.listName
        myCard.shortName = c.shortName
        
        let b = (try! BrandDataHelper.find(id:myCard.brandId))!
        myCard.brandName = b.brandName
        myCard.brandPhone = b.serviceNumber
        myCard.brandLogoName = b.brandLogoName
        
        if json["complaintNumber"].exists(){ myCard.complaintNumber = json["complaintNumber"].stringValue }
        if json["status"].exists(){ myCard.status = json["status"].stringValue }
        if json["uwsTask"].exists(){
            if json["uwsTask"]["service"].exists(){ myCard.service = json["uwsTask"]["service"].stringValue }
            if json["uwsTask"]["problem"].exists(){ myCard.problem = json["uwsTask"]["problem"].stringValue }
            if json["uwsTask"]["warrantyInfo"].exists(){ myCard.warrantyInfo = json["uwsTask"]["warrantyInfo"].stringValue }
            if json["uwsTask"]["deviceId"].exists(){ myCard.deviceId = json["uwsTask"]["deviceId"].stringValue }
        }
        if json["uwsSchedule"].exists(){
            let date = json["uwsSchedule"]["date"].stringValue
            let slot = json["uwsSchedule"]["timeSlot"].stringValue
//            OriUtil.oriLog("Card from server ",myCard.brandName,myCard.listName)
//            OriUtil.oriLog("Date from server ",date, "slot from server ",slot)
            if date != "" {
                myCard.schedule = getDateStringFromISO(date) + " , " + slot
            }
        }
        if json["type"].exists(){ myCard.type = json["type"].stringValue }
        if json["deviceTag"].exists(){ myCard.deviceTag = json["deviceTag"].stringValue }
        
        var title = myCard.listName
        if myCard.deviceTag != "" {
            title += (" ("+myCard.deviceTag+")")
        }
        myCard.title = title
        
        var subTitle = ""
        if myCard.type.caseInsensitiveCompare("UWS") == ComparisonResult.orderedSame && myCard.service != "" {
            subTitle = "Type: "+myCard.service
        } else if myCard.type.caseInsensitiveCompare("UWT") == ComparisonResult.orderedSame{
            subTitle = "Tracked via Ori"
        } else if myCard.type.caseInsensitiveCompare("UWA") == ComparisonResult.orderedSame {
            subTitle = "Annual Maintenance Contract"
        }
        myCard.subTitle = subTitle
        if json["cardNumber"].exists(){ myCard.cardNumber = json["cardNumber"].stringValue }
        if json["createdAt"].exists(){ myCard.createdAt = json["createdAt"].stringValue }
        if json["bookmarked"].exists(){ myCard.bookmarked = json["bookmarked"].boolValue }
        if json["showFeedbackFlag"].exists(){ myCard.showFeedbackFlag = json["showFeedbackFlag"].boolValue }
        if json["engineerName"].exists(){ myCard.engineerName = json["engineerName"].stringValue }
        if json["engineerPhone"].exists(){ myCard.engineerPhone = json["engineerPhone"].stringValue }
        if json["complaintStatusDetails"].exists(){ myCard.engineerName = json["complaintStatusDetails"].stringValue }
        
        if json["amcDetails"].exists(){
            var plan:AMCPlan = parseAMCJSON(json: json["amcDetails"])
            if json["collectPayment"].exists(){ plan.collectPayment = json["collectPayment"].boolValue }
            myCard.amcPlan = plan
        }
        
        return myCard
    }
    
    static func parseAMCJSON(json:JSON) -> AMCPlan {
        var plan = AMCPlan()
        if json["amcDuration"].exists(){ plan.duration = json["amcDuration"].stringValue }
        if json["amcPrice"].exists(){ plan.amcPrice = json["amcPrice"].stringValue }
        if json["amcTitle"].exists(){ plan.title = json["amcTitle"].stringValue }
        if json["amcInclusions"].exists(){ plan.inclusions = json["amcInclusions"].stringValue }
        if json["basicPrice"].exists(){ plan.basicPrice = json["basicPrice"].stringValue }
        if json["discount"].exists(){ plan.discount = json["discount"].stringValue }
        if json["tax"].exists(){ plan.tax = json["tax"].stringValue }
        if json["netAmount"].exists(){ plan.netPrice = json["netAmount"].stringValue }
        if json["originalPrice"].exists(){ plan.originalPrice = json["originalPrice"].stringValue }
        
        return plan
    }
    
    //call a phone number
    static func callNumber(number:String) {
        
        let phoneFallback = "telprompt://\(number)"
        let fallbackURl = URL(string:phoneFallback)!
        
        let phone = "tel://\(number)"
        let url = URL(string:phone)!
        
        let shared = UIApplication.shared
        
        if(shared.canOpenURL(fallbackURl)){
            if #available(iOS 10.0, *) {
                shared.open(fallbackURl, options: [:], completionHandler: nil)
            } else {
                shared.openURL(fallbackURl)
            }
        }else if (shared.canOpenURL(url)){
            if #available(iOS 10.0, *) {
                shared.open(url, options: [:], completionHandler: nil)
            } else {
                shared.openURL(url)
            }
        }else{
            OriUtil.oriLog("unable to open url for call")
        }
        
    }
    
    static func openApp(url:URL) -> Bool {
        let shared = UIApplication.shared
        if (shared.canOpenURL(url)){
            if #available(iOS 10.0, *) {
                shared.open(url, options: [:], completionHandler: nil)
            } else {
                shared.openURL(url)
            }
            return true
        }else{
            OriUtil.oriLog("unable to open url to app")
        }
        return false
    }
}




