//
//  DownloadAndReadImage.swift
//  ori
//
//  Created by Harsh Ganatra on 14/12/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation
import AWSCore
import AWSS3
import SDWebImage

class DownloadAndReadImage {
    let bucket_name = "oriapp"
    
    /*
     * folderName is the amazon folder name -- constants from Values file (amazonCategoriesFolder / amazonBrandsFolder)
     *
     */
    func renderLocalImage(imageName:String, imageView:UIImageView, folderName:String) {
        let name = imageName.trimmingCharacters(in: .whitespacesAndNewlines)
        if name == "" || name == "NOIMG" {
            return
        }
        let fileManager = FileManager.default
        
        let imagePath = (getDocumentsDirectory().appendingPathComponent(imageName))
        if fileManager.fileExists( atPath: imagePath.path ) {
//            OriUtil.oriLog("aws image exists, setting from local ",imageName)
            if imageName.contains(".pdf") {
                imageView.image = #imageLiteral(resourceName: "pdf")
            } else {
                imageView.sd_setImage(with: imagePath, completed: nil)
            }
            //            imageView.image = UIImage(contentsOfFile: imagePath.absoluteString)
        } else { //downloading file from AWS
            let  transferUtility = AWSS3TransferUtility.default()
            
//            let tasks = transferUtility.getDownloadTasks()
            
            
            //set loading on imageview
            OriUtil.oriLog("aws downloading image ",imageName)
            imageView.image = nil
            
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            indicator.center = imageView.center
            indicator.hidesWhenStopped = true
            imageView.addSubview(indicator)
            indicator.startAnimating()
            
            let completionHandler:AWSS3TransferUtilityDownloadCompletionHandlerBlock = { (task, URL , Data , error) -> Void in
                DispatchQueue.main.async(execute: {
                    // Do something e.g. Alert a user for transfer completion.
                    // On failed uploads, `error` contains the error object.
//                    OriUtil.oriLog("aws completion handler called "+imageName)
//                    imageView.sd_removeActivityIndicator()
                    indicator.stopAnimating()
                    indicator.removeFromSuperview()

                    if let _ = error {
                        //handle error during AWS download
                        imageView.image = #imageLiteral(resourceName: "error")

//                        if fileManager.fileExists( atPath: imagePath.absoluteString ) {
//                            do {
//                                try fileManager.removeItem(at: imagePath)
//                            } catch {
//                                OriUtil.oriLog("could  not delete file")
//                            }
//                        }
                    } else { //download completed
//                        imageView.image = UIImage(contentsOfFile: imagePath.path)

                        if imageName.contains(".pdf") {
                            imageView.image = #imageLiteral(resourceName: "pdf")
                        } else {
                            imageView.sd_setImage(with: imagePath, completed: nil)
                        }
                    }

                })
            }

            
            transferUtility.download(
                to: imagePath,
                bucket: bucket_name,
                key: folderName+imageName,
                expression: nil,
                completionHandler: completionHandler )

            //transfer manager
//            let transferManager = AWSS3TransferManager.default()
//
//            let downloadingFileURL = imagePath
//
//            let downloadRequest = AWSS3TransferManagerDownloadRequest()!
//
//            downloadRequest.bucket = bucket_name
//            downloadRequest.key = folderName+imageName
//            downloadRequest.downloadingFileURL = downloadingFileURL
//
//            transferManager.download(downloadRequest).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
//
//                if let error = task.error as NSError? {
//                    if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code) {
//                        switch code {
//                        case .cancelled, .paused:
//                            break
//                        default:
//                            OriUtil.oriLog("Error downloading: \(String(describing: downloadRequest.key)) Error: \(error)")
//                        }
//                    } else {
//                        OriUtil.oriLog("Error downloading: \(String(describing: downloadRequest.key)) Error: \(error)")
//                    }
//                    return nil
//                }
//
//                OriUtil.oriLog("aws Download complete for: \(String(describing: downloadRequest.key))")
////                let downloadOutput = task.result
//
//                imageView.image = UIImage(contentsOfFile: downloadingFileURL.path)
//                return nil
//            })

        } // if block ends here
    }
    
    func getDocumentsDirectory() -> URL {
//        let fileManager = FileManager.default
        //        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        //        return paths[0]
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String)
//        OriUtil.oriLog("aws path while aws download ",paths)
        return URL(fileURLWithPath: paths)
        
    }
    
    @discardableResult func setImageIfExists(imageView: UIImageView, imageName:String) -> Bool {
        if imageName != "" {
            let x = fileExistsInLocal(imageName: imageName)
            if x.0 {
                if imageName.contains(".pdf") {
                    imageView.image = #imageLiteral(resourceName: "pdf")
                } else {
                    imageView.sd_setImage(with: x.1!, completed: nil)
                }
                return true
            }
        }
        return false
    }
    
    func fileExistsInLocal(imageName:String) -> (Bool,URL?) {
        if imageName != "" {
            let fileManager = FileManager.default
            let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)
            if fileManager.fileExists( atPath: imagePath.path ) {
                return (true,imagePath)
            }
        }
        return (false,nil)
    }
    
    func saveImageToLocalStorage(image: UIImage, fileNameWithExtension: String){
        let imagePath = (getDocumentsDirectory().appendingPathComponent(fileNameWithExtension))
        
//        guard let imageData = UIImageJPEGRepresentation(image, 1) else {
//            // handle failed conversion
//            print("jpg error")
//            return
//        }
        
        guard let imageData = UIImagePNGRepresentation(image) else {
            // handle failed conversion
//            print("png error")
            return
        }
        
        do {
            try imageData.write(to: imagePath)
        } catch {
            OriUtil.oriLog("Cannot save image",error)
        }
    }
    
    func deleteLocalFile(fileName:String) {
        let x = fileExistsInLocal(imageName: fileName)
        if x.0 {
            let fileManager = FileManager.default
            do {
                try fileManager.removeItem(at: x.1!)
            } catch {
                OriUtil.oriLog("Could not delete local file",fileName)
            }
        }
    }
    
    func copyUrlToLocal(url:URL, fileName:String) {
        let filePath = getDocumentsDirectory().appendingPathComponent(fileName)
        
        let fileManager = FileManager.default
        do {
            try fileManager.copyItem(at: url, to: filePath)
        } catch {
            OriUtil.oriLog("Could not copy local file",fileName)
        }
        
    }
    
    func uploadFileToAWS(imageName:String) {
        let x = fileExistsInLocal(imageName: imageName)
        var contentType:String = "image"
        if x.0 {
            do {
                let data = try Data(contentsOf: x.1!)
            
                if imageName.contains(".pdf") {
                    contentType = "application/pdf"
                }
                let expression = AWSS3TransferUtilityUploadExpression()
                expression.progressBlock = {(task, progress) in
                    DispatchQueue.main.async(execute: {
                        // Do something e.g. Update a progress bar.
                    })
                }
                
                var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
                completionHandler = { (task, error) -> Void in
                    DispatchQueue.main.async(execute: {
                        // Do something e.g. Alert a user for transfer completion.
                        // On failed uploads, `error` contains the error object.
                    })
                }
                
                let transferUtility = AWSS3TransferUtility.default()
                
                transferUtility.uploadData(data,
                                           bucket: bucket_name,
                                           key: imageName,
                                           contentType: contentType,
                                           expression: expression,
                                           completionHandler: completionHandler)
            } catch {
                OriUtil.oriLog("Could not parse url to data")
            }
        } else{
            OriUtil.oriLog("Could not find local URL")
        }
        
        
    }
    
    func deleteAWSFile(fileName:String) {
//        let s3 = AWSS3.default()
//        let deleteObjectRequest = AWSS3DeleteObjectRequest()
//        deleteObjectRequest?.bucket = self.bucket_name
//        deleteObjectRequest?.key = fileName
//        s3.deleteObject(deleteObjectRequest!).continueWith { (task:AWSTask) -> AnyObject? in
//            if let error = task.error {
//                OriUtil.oriLog("Error occurred: \(error)")
//                return nil
//            }
//            OriUtil.oriLog("Deleted successfully.")
//            return nil
//        }
    }
}





