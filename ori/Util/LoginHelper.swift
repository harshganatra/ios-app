//
//  LoginHelper.swift
//  ori
//
//  Created by Harsh Ganatra on 10/11/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation

class LoginHelper {
 
    static func isLoggedIn() -> Bool {
        let pref = UserDefaults.standard
        let userID = pref.string(forKey: Values.USERID_KEY) as String?
        if let check=userID, check != "" {
            return true
        }
        return false
    }
    
    static func getCurrentUser() -> (Bool, OriUser) {
        var user:OriUser = OriUser()
        let pref = UserDefaults.standard
        let userID = pref.string(forKey: Values.USERID_KEY) as String?
        if let check=userID, check != "" { //user id exists
            user.userID = userID!
            user.authToken = pref.string(forKey: Values.TOKEN_KEY)! as String
            user.firstName = pref.string(forKey: Values.FIRST_NAME_KEY)! as String
            user.lastName = pref.string(forKey: Values.LAST_NAME_KEY)! as String
            user.phoneNumber = pref.string(forKey: Values.MOBILE_KEY)! as String
            user.email = pref.string(forKey: Values.EMAIL_KEY)! as String
            user.gender = pref.string(forKey: Values.GENDER_KEY)! as String
            user.ISOdateOfBirth = pref.string(forKey: Values.ISO_DOB_KEY)! as String
            user.name = user.firstName+" "+user.lastName
            
            return (true,user)
        }
        return (false,user)
    }
}

