//
//  PhoneFieldDelegate.swift
//  ori
//
//  Created by Harsh Ganatra on 06/11/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation
import UIKit

class NumericFieldDelegate: NSObject, UITextFieldDelegate {
    var limit:Int = 10 //default is 10 for phone number
    
    init(digits:Int = 10) {
        self.limit = digits
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.count == 0 { // to allow backspace key to work
            return true
        }
        
        var newText = textField.text! as NSString
        var isDigit: Bool = false
        newText = newText.replacingCharacters(in: range, with: string) as NSString
        
        if let _ = Int(string) {
            isDigit = true
        }
        
        return isDigit && newText.length <= limit
    }
}
