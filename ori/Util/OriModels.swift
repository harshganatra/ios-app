//
//  Brand.swift
//  ori
//
//  Created by Harsh Ganatra on 10/10/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Brand {
    var brandId:Int
    var brandLogoName, isOnboarded, serviceNumber, brandName, hasCategory, brandCategory, brandSubCategory,
    trackingAvailable, amcAvailable, androidBrandArray, categoriesList:String
    var trackingJSONArray, categoryJSON:String
}

struct OriCategory {
    var listName, shortName, category, categorySlug, categoryID, respectiveBrandIDs, imageName, searchArray, serviceList, AMCBrandIDS, amcAvailable:String
}

public struct ArrayModel {
    init(type:String, data:Any) {
        self.type = type
        self.data = data
    }
    var type:String
    var data:Any
}

// MARK: Return Protocol
public protocol ReturnProtocol: class {
    func getReturnValue(value: ArrayModel)
}

struct SliderContent {
    init() {
        self.title = ""
        self.url  = ""
        self.imageURL = ""
        self.type = ""
    }
    var title, url, imageURL, type: String
}

struct ScrollContent {
    init() {
        self.title = ""
        self.subtitle = ""
        self.url  = ""
        self.imageURL = ""
        self.type = ""
        self.cardNumber = ""
        self.myDevice = MyDevice()
        self.myConnection = MyConnection()
        self.myCard = MyCard()
    }
    var title, subtitle, url, imageURL, type, cardNumber: String
    var myDevice: MyDevice
    var myConnection:MyConnection
    var myCard: MyCard
}

struct OriUser {
    init() {
        self.name = ""
        self.authToken = ""
        self.gender = ""
        self.firstName = ""
        self.lastName = ""
        self.phoneNumber = ""
        self.userID = ""
        self.ISOdateOfBirth = ""
        self.email = ""
    }
    var name :String
    var phoneNumber:String
    var authToken:String
    var userID:String
    var firstName, lastName, gender, ISOdateOfBirth:String
    var email:String
}

struct Address{
    init() {
        self._id = ""
        self.addressLine1 = ""
        self.landmark = ""
        self.zipcode = ""
        self.city = ""
        self.addressLine2 = ""
        self.state = ""
        self.nickName = ""
        self.isDefault = false
    }
    
    func matches(_ address:Address) -> Bool {
        return address._id == self._id
    }
    var _id :String
    var addressLine1:String
    var addressLine2:String
    var state:String
    var zipcode, city, landmark, nickName:String
    var isDefault:Bool
}

struct MyDevice {
    init() {
        self.isOnboarded = "false"
        self.brandId = -1
        self.vidBrandVerified = false; self.amcAvailable = false
        self._id = ""; self.applianceOrConnection = ""; self.userId = ""; self.categorySlug = ""; self.createdAt = ""
        self.modifiedAt = ""; self.modelNo = ""; self.serialNumber = ""; self.dateOfPurchase = ""; self.deviceTag = ""
        self.deviceImage = ""; self.invoiceFile = ""; self.warrantyFile = ""
        self.defaultDeviceImage = ""; self.category = ""
        self.brandName = ""; self.brandLogoName = ""; self.brandPhone = ""; self.retailerName = ""
        self.categorySearchArray = ""; self.brandSearchArray = ""
        self.listName = ""; self.subCategory = ""; self.modelCode = ""
        self.warrantyStatus = ""; self.warrantyPeriod = ""
    }
    
    var _id, applianceOrConnection, userId, categorySlug, createdAt :String
    var modifiedAt, modelNo, serialNumber, dateOfPurchase, deviceTag :String
    var deviceImage, invoiceFile, warrantyFile, defaultDeviceImage, category :String
    var brandId :Int
    var brandName, brandLogoName, brandPhone, retailerName :String
    var categorySearchArray, brandSearchArray :String
    var listName, subCategory, modelCode :String
    var isOnboarded :String
    var vidBrandVerified, amcAvailable :Bool //device is verified by videocon, can be any brand under videocon
    var warrantyStatus, warrantyPeriod :String
}

struct MyConnection {
    init() {
        self._id = ""; self.VCNo = ""; self.status = ""; self.userName = ""; self.SMSID = ""; self.RMN = ""; self.balance = ""; self.basePackName = ""; self.basePackPrice=""
        self.categorySlug = ""; self.category = ""; self.subCategory = ""; self.model = ""; self.dateOfPurchase = ""; self.retailer = ""; self.invoiceFile = ""; self.deviceTag = ""
        self.brandName = ""; self.brandLogoName = ""; self.brandPhone = ""; self.brandOnboarded = ""; self.listName = ""; self.defaultDeviceImage = "" ;
        self.emailId = ""; self.switchOffDate = ""; self.applianceOrConnection = ""; self.categorySearchArray = ""; self.brandSearchArray = ""
        self.packPeriod = ""; self.zoneCode = ""; self.schemeCode = ""; self.monthlySubscriptionAmount = ""; self.minRechargeAmount = ""
        self.statusID = ""; self.lastRechargeDate = ""
        
        self.brandId = -1
        self.verified = false
        self.childVCs = []
        self.addOnPacks = []; self.otherCharges = []
    }
    
    var _id, VCNo, status, userName, SMSID, RMN, balance, basePackName, basePackPrice,
    categorySlug, category, subCategory, model, dateOfPurchase, retailer, invoiceFile, deviceTag :String
    var brandName, brandLogoName, brandPhone, brandOnboarded, listName, defaultDeviceImage,
    emailId, switchOffDate, applianceOrConnection, categorySearchArray, brandSearchArray :String
    var packPeriod, zoneCode, schemeCode, monthlySubscriptionAmount, minRechargeAmount,
    statusID, lastRechargeDate :String
    var brandId :Int
    var verified:Bool
    var childVCs:[DishChildVC]
    var addOnPacks, otherCharges:[DishPack]
}

struct DishPack {
    init() {
        self.pack_name = ""
        self.type = ""
        self.price = ""
        self.id = ""
    }
    init(packName:String, price:String) {
        self.pack_name = packName
        self.type = ""
        self.price = price
        self.id = ""
    }
    var pack_name, type, price, id:String
}

struct DishChildVC {
    init() {
        self.VCNo = ""
        self.SMSID = ""
        self.RMN = ""
        self.emailId = ""
        self.balance = ""
        self.basePackName = ""
        self.basePackPrice = ""
        self.addOnPacks = []
    }
    var VCNo, SMSID, RMN, emailId, balance, basePackName, basePackPrice:String
    var addOnPacks: [DishPack]
}

struct MyCard {
    init() {
        self.service = ""; self.problem = ""; self.warrantyInfo = ""; self.deviceId = ""
        self.brandName = ""; self.brandPhone = ""; self.complaintNumber = ""; self.shortName = ""
        self.brandLogoName = ""
        self.schedule = ""
        self.type = ""; self.categorySlug = ""; self.cardNumber = ""
        self.createdAt = ""; self.status = ""; self.listName = ""; self.deviceTag = ""
        self.engineerName = ""; self.engineerPhone = ""; self.complaintStatusDetails = ""; self.paymentOrderId = ""; self.paymentStatus = ""
        self.title = ""; self.subTitle = ""
        self.history = CardStatus()
        self.fullHistory = []
        self.showFeedbackFlag = false; self.bookmarked = false; self.isEscalated = false
        self.brandId = -1
        self.amcPlan = AMCPlan()
    }
    var service, problem, warrantyInfo, deviceId, brandName, brandPhone, complaintNumber, shortName:String
    var brandLogoName, schedule, type, categorySlug, cardNumber:String
    var createdAt, status, listName, deviceTag:String
    var engineerName, engineerPhone, complaintStatusDetails, paymentOrderId, paymentStatus:String
    var history:CardStatus
    var fullHistory:[CardStatus]
    var showFeedbackFlag, bookmarked, isEscalated:Bool
    var brandId:Int;
    var amcPlan:AMCPlan
    var title, subTitle:String
}

struct CardStatus {
    init() {
        self.status = ""
        self.statusDetails = ""
        self.lastUpdated = ""
    }
    var status, statusDetails, lastUpdated:String
}

struct AMCPlan {
    init() {
        self.duration = ""
        self.amcPrice = ""
        self.title = ""
        self.inclusions = ""
        self.netPrice = ""
        self.discount = ""
        self.basicPrice = ""
        self.tax = ""
        self.originalPrice = ""
        self.collectPayment = false
    }
    var duration, amcPrice, title, inclusions, netPrice, discount, basicPrice, tax, originalPrice:String
    var collectPayment:Bool
}

struct ListWithCode {
    var problemText, problemCode:String
    init(problemText:String, problemCode:String) {
        self.problemText = problemText
        self.problemCode = problemCode
    }
}



