//
//  SQLiteDataStore.swift
//  ori
//
//  Created by Harsh Ganatra on 10/10/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation
import SQLite

class SQLiteDataStore {
    static let sharedInstance = SQLiteDataStore()
    let BBDB: Connection?
    
    private init() {
        
        var path = "oridb.sqlite"
        
        let dirs: [NSString] = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory,
                                                                   FileManager.SearchPathDomainMask.allDomainsMask, true) as [NSString]
        
        if dirs.count > 0 {
            let dir = dirs[0]
            path = dir.appendingPathComponent("oridb.sqlite");
        }
        
        do {
            BBDB = try Connection(path)
        } catch _ {
            BBDB = nil
        }
    }
    
    func createTables() throws{
        do {
            try CategoryDataHelper.createTable()
            try BrandDataHelper.createTable()
        } catch {
            throw DataAccessError.Datastore_Connection_Error
        }
    }
}

class BrandDataHelper {
    static let TABLE_NAME = "Brands"
    
    static let table = Table(TABLE_NAME)
    static let Id = Expression<Int>("Id")
    static let brandId = Expression<Int>("brandId")
    static let brandName = Expression<String>("brandName")
    static let imageName = Expression<String>("imageName")
    static let isOnboarded = Expression<String>("isOnboarded")
    static let hasCategory = Expression<String>("hasCategory")
    static let categoryJSON = Expression<String>("categoryJSON")
    static let trackingAvailable = Expression<String>("trackingAvailable")
    static let trackingFormJSON = Expression<String>("trackingFormJSON")
    static let categoriesList = Expression<String>("categoriesList")
    static let serviceNumber = Expression<String>("serviceNumber")
    static let androidBrandArray = Expression<String>("androidBrandArray")
    static let amcAvailable = Expression<String>("amcAvailable")
    
    typealias B = Brand
    
    static func createTable() throws {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        try DB.run(table.drop(ifExists: true))
        
        do {
            let _ = try DB.run( table.create(ifNotExists: true) {t in
                t.column(Id, primaryKey: .autoincrement)
                t.column(brandId)
                t.column(brandName )
                t.column(imageName )
                t.column(isOnboarded )
                t.column(hasCategory )
                t.column(categoryJSON )
                t.column(trackingAvailable )
                t.column(trackingFormJSON )
                t.column(categoriesList )
                t.column(serviceNumber )
                t.column(androidBrandArray )
                t.column(amcAvailable )
            })
            
        } catch _ {
            // Error throw if table already exists
        }
        
    }
    
    static func insert(item: B) throws -> Int64 {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        let insert = table.insert(brandId <- item.brandId,
                                  brandName <- item.brandName,
                                  imageName <- item.brandLogoName,
                                  isOnboarded <- item.isOnboarded,
                                  hasCategory <- item.hasCategory,
                                  categoryJSON <- item.categoryJSON,
                                  trackingAvailable <- item.trackingAvailable,
                                  trackingFormJSON <- item.trackingJSONArray,
                                  categoriesList <- item.categoriesList,
                                  serviceNumber <- item.serviceNumber,
                                  amcAvailable <- item.amcAvailable,
                                  androidBrandArray <- item.androidBrandArray)
        //            do {
        let rowId = try DB.run(insert)
        guard rowId > 0 else {
            throw DataAccessError.Insert_Error
        }
        return rowId
        //            } catch _ {
        //                throw DataAccessError.Insert_Error
        //            }
        
    }
    
    static func deleteRows () throws -> Void {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        try DB.run(table.delete())
        //            do {
        //                let tmp = try DB.run(table.delete())
        //                guard tmp == 1 else {
        //                    throw DataAccessError.Delete_Error
        //                }
        //            } catch _ {
        //                throw DataAccessError.Delete_Error
        //            }
        
    }
    
    static func find(id: Int) throws -> B? {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        let query = table.filter(brandId == id)
        let items = try! DB.prepare(query)
        for item in  items {
            return Brand(brandId : item[brandId],
                         brandLogoName : item[imageName],
                         isOnboarded : item[isOnboarded],
                         serviceNumber : item[serviceNumber],
                         brandName : item[brandName],
                         hasCategory : item[hasCategory],
                         brandCategory: "",
                         brandSubCategory: "",
                         trackingAvailable : item[trackingAvailable],
                         amcAvailable : item[amcAvailable],
                         androidBrandArray : item[androidBrandArray],
                         categoriesList : item[categoriesList],
                         trackingJSONArray : item[trackingFormJSON],
                         categoryJSON : item[categoryJSON])
        }
        OriUtil.oriLog("brand not found")
        return nil
        
    }
    
    static func findAll() throws -> [B]? {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        var retArray = [B]()
        let items = try! DB.prepare(table)
        for item in items {
            retArray.append(Brand(brandId : item[brandId],
                                  brandLogoName : item[imageName],
                                  isOnboarded : item[isOnboarded],
                                  serviceNumber : item[serviceNumber],
                                  brandName : item[brandName],
                                  hasCategory : item[hasCategory],
                                  brandCategory: "",
                                  brandSubCategory: "",
                                  trackingAvailable : item[trackingAvailable],
                                  amcAvailable : item[amcAvailable],
                                  androidBrandArray : item[androidBrandArray],
                                  categoriesList : item[categoriesList],
                                  trackingJSONArray : item[trackingFormJSON],
                                  categoryJSON : item[categoryJSON]))
        }
        
        return retArray
        
    }
}

class CategoryDataHelper {
    static let TABLE_NAME = "Categories"
    
    static let table = Table(TABLE_NAME)
    static let Id = Expression<Int>("Id")
    static let categorySlug = Expression<String>("categorySlug")
    static let listName = Expression<String>("listName")
    static let shortName = Expression<String>("shortName")
    static let searchArray = Expression<String>("searchArray")
    static let brandIds = Expression<String>("brandIds")
    static let serviceIds = Expression<String>("serviceIds")
    static let category = Expression<String>("category")
    static let imageName = Expression<String>("imageName")
    static let amcAvailable = Expression<String>("amcAvailable")
    static let amcBrandIDList = Expression<String>("amcBrandIDList")
    
    typealias C = OriCategory
    
    static func createTable() throws {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        try DB.run(table.drop(ifExists: true))
        do {
            let _ = try DB.run( table.create(ifNotExists: true) {t in
                t.column(Id, primaryKey: .autoincrement)
                t.column(categorySlug )
                t.column(listName )
                t.column(shortName )
                t.column(searchArray )
                t.column(brandIds )
                t.column(serviceIds )
                t.column(category )
                t.column(imageName )
                t.column(amcAvailable )
                t.column(amcBrandIDList )
            })
            
        } catch _ {
            // Error throw if table already exists
        }
        //         try! deleteRows()
        
    }
    
    static func insert(item: C) throws -> Int64 {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        let insert = table.insert(categorySlug <- item.categorySlug,
                                  listName <- item.listName,
                                  shortName <- item.shortName,
                                  searchArray <- item.searchArray,
                                  brandIds <- item.respectiveBrandIDs,
                                  serviceIds <- item.serviceList,
                                  category <- item.category,
                                  imageName <- item.imageName,
                                  amcAvailable <- item.amcAvailable,
                                  amcBrandIDList <- item.AMCBrandIDS)
//        do {
//            let rowId = try DB.run(insert)
//            guard rowId > 0 else {
//
//                throw DataAccessError.Insert_Error
//            }
//            OriUtil.oriLog("no error \(rowId)")
//            return rowId
//        } catch _ {
//            OriUtil.oriLog(" error ")
//            throw DataAccessError.Insert_Error
//        }
        
        do {
            let rowId = try DB.run(insert)
            return rowId
        } catch let Result.error(message, code, _) where code == SQLITE_CONSTRAINT {
            OriUtil.oriLog("constraint failed: \(message), ")
        } catch let error {
            OriUtil.oriLog("insertion failed: \(error)")
        }
        return 0
    }
    
    static func deleteRows () throws -> Void {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        
        try DB.run(table.delete())
        //        do {
        //            let tmp = try DB.run(table.delete())
        //            guard tmp == 1 else {
        //                OriUtil.oriLog("table delete tmp: \(tmp) means no rows to delete" )
        //                return
        ////                throw DataAccessError.Delete_Error
        //            }
        //        } catch _ {
        //            throw DataAccessError.Delete_Error
        //        }
        
    }
    
    static func find(slug: String) throws -> C? {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        let query = table.filter(categorySlug == slug)
        let items = try! DB.prepare(query)
        for item in  items {
            return OriCategory(listName : item[listName],
                               shortName : item[shortName],
                               category : item[category],
                               categorySlug : item[categorySlug],
                               categoryID : "",
                               respectiveBrandIDs : item[brandIds],
                               imageName: item[imageName],
                               searchArray: item[searchArray],
                               serviceList : item[serviceIds],
                               AMCBrandIDS : item[amcBrandIDList],
                               amcAvailable : item[amcAvailable])
        }
        
        return nil
        
    }
    
    static func findAll() throws -> [C]? {
        guard let DB = SQLiteDataStore.sharedInstance.BBDB else {
            throw DataAccessError.Datastore_Connection_Error
        }
        var retArray = [C]()
        let items = try! DB.prepare(table)
        for item in items {
            retArray.append(OriCategory(listName : item[listName],
                                        shortName : item[shortName],
                                        category : item[category],
                                        categorySlug : item[categorySlug],
                                        categoryID : "",
                                        respectiveBrandIDs : item[brandIds],
                                        imageName: item[imageName],
                                        searchArray: item[searchArray],
                                        serviceList : item[serviceIds],
                                        AMCBrandIDS : item[amcBrandIDList],
                                        amcAvailable : item[amcAvailable]))
        }
        
        return retArray
        
    }
}


enum DataAccessError: Error {
    case Datastore_Connection_Error
    case Insert_Error
    case Delete_Error
    case Search_Error
    case Nil_In_Data
}
