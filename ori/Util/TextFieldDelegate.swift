//
//  TextFieldDelegate.swift
//  ori
//
//  Created by Harsh Ganatra on 05/01/18.
//  Copyright © 2018 Oriserve. All rights reserved.
//

import Foundation
import UIKit

class TextFieldDelegate: NSObject, UITextFieldDelegate {
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true;
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var view = textField.superview
        while view?.superview != nil {
            view = view?.superview
        }
        
        if let nextField = view?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            return true;
        }
        return false
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if string.count == 0 { // to allow backspace key to work
//            return true
//        }
//
//        var newText = textField.text! as NSString
//        var isDigit: Bool = false
//        newText = newText.replacingCharacters(in: range, with: string) as NSString
//
//        if let _ = Int(string) {
//            isDigit = true
//        }
//
//        return isDigit && newText.length <= 10
//    }
}
