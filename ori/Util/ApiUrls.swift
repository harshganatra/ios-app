//
//  ApiUrls.swift
//  ori
//
//  Created by Harsh Ganatra on 14/12/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation

public class ApiUrls {
    private static var apiurl = "https://api.oriserve.com/"
//    private static var apiurl = "http://192.168.43.139:10000/"
    
    public static var homeSliderURL = apiurl + "api/uws/content/homepage"
    public static var getBrandUrl = apiurl + "api/brands"
    public static var getCategoriesUrl = apiurl + "api/categories"
    public static var postSendOtpUrl = apiurl + "api/customer/verify"
    public static var postConfirmOTPUrl = apiurl + "api/customer/verify/confirm"
    public static var postLoginUrl = apiurl + "api/login"
    public static var putAddCustUrl = apiurl + "api/customer"
    public static var putAddAddressUrl = apiurl + "api/customer/address"
    public static var putAddDeviceUrl = apiurl + "api/uws/device"
    public static var putAddCardUrl = apiurl + "api/createcard"
    public static var getAddressByUserIdUrl = apiurl + "api/customer/addresses"
    public static var getAllCardsByUserIdUrl = apiurl + "api/cards"
    public static var getSingleCardByUserIdUrl = apiurl + "api/card"
    public static var getCardStatusUpdate = apiurl + "api/cardstatusupdate"
    public static var putFeedbackUrl = apiurl + "api/customer/feedback"
    public static var getAllDevicesByIdUrl = apiurl + "api/customer/devices"
    public static var postUpdateDeviceUrl = apiurl + "api/customer/device"
    public static var getContentUrl = apiurl + "api/uws/content"
    public static var getContentDetailsUrl = apiurl + "api/uws/content/details"
    public static var postContentFeedbackUrl = apiurl + "api/uws/content/feedback"
    public static var gethomePageUpdates = apiurl + "api/uws/updates/homepage"
    public static var getDeviceCount = apiurl + "api/customerdevicecount"
    public static var updateCustomerUrl = apiurl + "api/customer/update"
    public static var customerExistsUrl = apiurl + "api/customer/exists"
    public static var updateCardUrl = apiurl + "api/card/update"
    public static var escalateCardUrl = apiurl + "api/card/escalate"
    public static var trackCardUrl = apiurl + "api/track"
    public static var cardsByDevice = apiurl + "api/cardsbydevice"
    public static var appUpdate = apiurl + "api/appUpdate"
    public static var checkProblems = apiurl + "api/card/listproblems"
    public static var deleteAddressByIdUrl = apiurl + "api/customer/address/"
    public static var getAMCForm = apiurl + "api/amc/form"
    public static var getAMCPlans = apiurl + "api/amc/info"
    public static var addAMC = apiurl + "api/amc/order"
    public static var listModels = apiurl + "api/card/listmodels"
    public static var getDeviceByIdUrl = apiurl + "api/customer/device/"
    public static var payUBizHashCall = apiurl + "ori/payu_/app/request"
    public static var payUBizStatusCall = apiurl + "ori/payu_/verify"
    public static var getPaymentHistory = apiurl + "api/payu/getPaymentHistory"
    
    public static var dish_getEndeavourSubscriberDetails = apiurl + "api/dishtv/getSubscriberDetails"
    public static var dish_addConnection = apiurl + "api/dishtv/device"
    public static var dish_refreshConnection = apiurl + "api/dishtv/refreshConnection"
    public static var dish_refreshDishBox = apiurl + "api/dishtv/refreshDishBox"
    public static var dish_reqCallback = apiurl + "api/dishtv/reqCallback"
    public static var dish_getRechargeAmount = apiurl + "api/dishtv/getInstantRechargeAmount"
    public static var getConnections = apiurl + "api/customer/connections"
    public static var updateConnection = apiurl + "api/customer/connection"
}
