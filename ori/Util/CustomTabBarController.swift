//
//  CustomTabBarController.swift
//  ori
//
//  Created by Harsh Ganatra on 20/12/17.
//  Copyright © 2017 Oriserve. All rights reserved.
//

import Foundation
import UIKit

class CustomTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBar.appearance().tintColor = UIColor.black
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -15)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.selectedIndex = 1;
    }
    
    override var selectedViewController: UIViewController? {
        didSet {
            
            guard let viewControllers = viewControllers else {
                return
            }
            
            for viewController in viewControllers {
                
                if viewController == selectedViewController {
                    
                    let selected: [NSAttributedStringKey: AnyObject] =
                        [NSAttributedStringKey.font:fontForTimesRoman(withStyle: "bold", andFontsize: 12),
                         NSAttributedStringKey.foregroundColor: UIColor.black]
                    
                    viewController.tabBarItem.setTitleTextAttributes(selected, for: .normal)
                    
                } else {
                    
                    let normal: [NSAttributedStringKey: AnyObject] =
                        [NSAttributedStringKey.font: fontForTimesRoman(withStyle: "regular", andFontsize: 12),
                         NSAttributedStringKey.foregroundColor: UIColor.gray]
                    
                    viewController.tabBarItem.setTitleTextAttributes(normal, for: .normal)
                    
                }
            }
        }
    }
    
    
    func fontForTimesRoman(withStyle style: String, andFontsize size: CGFloat) -> UIFont {
        if (style == "bold") {
            return UIFont(name: "TimesNewRomanPS-BoldMT", size: size)!
        }
        else if(style == "italic"){
            return UIFont(name: "TimesNewRomanPS-ItalicMT", size: size)!
        }
        else{
            return UIFont(name: "TimesNewRomanPSMT", size: size)!
        }
        
    }
}
